﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.SignerInfo
struct SignerInfo_t4122348804;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_String2029220233.h"

// System.Void UnityEngine.Purchasing.Security.SignerInfo::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void SignerInfo__ctor_m2022425308 (SignerInfo_t4122348804 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Purchasing.Security.SignerInfo::get_Version()
extern "C"  int32_t SignerInfo_get_Version_m890152389 (SignerInfo_t4122348804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_Version(System.Int32)
extern "C"  void SignerInfo_set_Version_m3486820030 (SignerInfo_t4122348804 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.SignerInfo::get_IssuerSerialNumber()
extern "C"  String_t* SignerInfo_get_IssuerSerialNumber_m988083912 (SignerInfo_t4122348804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_IssuerSerialNumber(System.String)
extern "C"  void SignerInfo_set_IssuerSerialNumber_m3572187615 (SignerInfo_t4122348804 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Purchasing.Security.SignerInfo::get_EncryptedDigest()
extern "C"  ByteU5BU5D_t3397334013* SignerInfo_get_EncryptedDigest_m2178470575 (SignerInfo_t4122348804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.SignerInfo::set_EncryptedDigest(System.Byte[])
extern "C"  void SignerInfo_set_EncryptedDigest_m3655085066 (SignerInfo_t4122348804 * __this, ByteU5BU5D_t3397334013* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
