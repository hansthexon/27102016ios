﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Stores_UnityEngine_Purchasing_ScriptingUnityCallbac906080071.h"
#include "Stores_UnityEngine_Purchasing_AmazonAppStoreStoreE1518886395.h"
#include "Stores_UnityEngine_Purchasing_FakeAmazonExtensions2261777661.h"
#include "Stores_UnityEngine_Purchasing_FakeGooglePlayConfigu737012266.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl1301617341.h"
#include "Stores_UnityEngine_Purchasing_AppleStoreImpl_U3CMe2294210682.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleConfiguatio4052738437.h"
#include "Stores_UnityEngine_Purchasing_FakeAppleExtensions4039399289.h"
#include "Stores_UnityEngine_Purchasing_AndroidStore3203055206.h"
#include "Stores_UnityEngine_Purchasing_FakeStoreUIMode2321492887.h"
#include "Stores_UnityEngine_Purchasing_FakeStore3882981564.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_DialogType1733969544.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CRetriev2692708567.h"
#include "Stores_UnityEngine_Purchasing_FakeStore_U3CPurchas2000504963.h"
#include "Stores_UnityEngine_Purchasing_RawStoreProvider3477922056.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo4003664591.h"
#include "Stores_UnityEngine_Purchasing_StandardPurchasingMo1310600573.h"
#include "Stores_UnityEngine_Purchasing_StoreConfiguration2466794143.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore3684252124.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_DialogRe2092195449.h"
#include "Stores_UnityEngine_Purchasing_UIFakeStore_Lifecycl1057582876.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil166323129.h"
#include "Stores_UnityEngine_Purchasing_Extension_UnityUtil_3019257939.h"
#include "Stores_UnityEngine_Purchasing_WinRTStore36043095.h"
#include "Stores_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Stores_U3CPrivateImplementationDetailsU3E_U24Array1568637717.h"
#include "Vuforia_UnityExtensions_U3CModuleU3E3783534214.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst3719235061.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst1156375578.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst1337058172.h"
#include "Vuforia_UnityExtensions_Vuforia_DigitalEyewearAbst1300054541.h"
#include "Vuforia_UnityExtensions_Vuforia_Device1860504872.h"
#include "Vuforia_UnityExtensions_Vuforia_Device_Mode2855916820.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice1202635122.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_EyeID642957731.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearDevice_Eyew1521251591.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker189438242.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTracker2183873360.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackerAbstr2651534015.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin3766399464.h"
#include "Vuforia_UnityExtensions_Vuforia_DistortionRenderin2945034146.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearUser117253723.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearCal3632467967.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEyewearDev2977282393.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearDevi22891981.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraConfiguratio3904398347.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseCameraConfigurat38459502.h"
#include "Vuforia_UnityExtensions_Vuforia_BaseStereoViewerCa1102239676.h"
#include "Vuforia_UnityExtensions_Vuforia_StereoViewerCamera3365023487.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityContro1276557833.h"
#include "Vuforia_UnityExtensions_Vuforia_MixedRealityControll38013191.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTr3644694819.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra111727860.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParameters1247673784.h"
#include "Vuforia_UnityExtensions_Vuforia_CustomViewerParamet779886969.h"
#include "Vuforia_UnityExtensions_Vuforia_DeviceTrackingMana2097550852.h"
#include "Vuforia_UnityExtensions_Vuforia_FactorySetter648583075.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2025108506.h"
#include "Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbs3732945727.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibra1518014586.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalPlayMode3894463544.h"
#include "Vuforia_UnityExtensions_Vuforia_RotationalDeviceTra832065887.h"
#include "Vuforia_UnityExtensions_Vuforia_IOSCamRecoveringHel979448318.h"
#include "Vuforia_UnityExtensions_Vuforia_MeshUtils2110180948.h"
#include "Vuforia_UnityExtensions_Vuforia_ExternalStereoCame4187656756.h"
#include "Vuforia_UnityExtensions_Vuforia_NullHideExcessArea2290611987.h"
#include "Vuforia_UnityExtensions_Vuforia_StencilHideExcessA3377289090.h"
#include "Vuforia_UnityExtensions_Vuforia_LegacyHideExcessAr3475621185.h"
#include "Vuforia_UnityExtensions_Vuforia_DedicatedEyewearCam816511398.h"
#include "Vuforia_UnityExtensions_Vuforia_NullCameraConfigura133234522.h"
#include "Vuforia_UnityExtensions_Vuforia_MonoCameraConfigur3796201132.h"
#include "Vuforia_UnityExtensions_Vuforia_UnityCameraExtensi2392150382.h"
#include "Vuforia_UnityExtensions_Vuforia_View3542740111.h"
#include "Vuforia_UnityExtensions_Vuforia_ViewerParametersLi3152440868.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (ScriptingUnityCallback_t906080071), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[2] = 
{
	ScriptingUnityCallback_t906080071::get_offset_of_forwardTo_0(),
	ScriptingUnityCallback_t906080071::get_offset_of_util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (AmazonAppStoreStoreExtensions_t1518886395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[1] = 
{
	AmazonAppStoreStoreExtensions_t1518886395::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (FakeAmazonExtensions_t2261777661), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (FakeGooglePlayConfiguration_t737012266), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (AppleStoreImpl_t1301617341), -1, sizeof(AppleStoreImpl_t1301617341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1907[9] = 
{
	AppleStoreImpl_t1301617341::get_offset_of_m_DeferredCallback_2(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptError_3(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RefreshReceiptSuccess_4(),
	AppleStoreImpl_t1301617341::get_offset_of_m_RestoreCallback_5(),
	AppleStoreImpl_t1301617341::get_offset_of_m_Native_6(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_util_7(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_instance_8(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	AppleStoreImpl_t1301617341_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (U3CMessageCallbackU3Ec__AnonStorey0_t2294210682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_subject_0(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_payload_1(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_receipt_2(),
	U3CMessageCallbackU3Ec__AnonStorey0_t2294210682::get_offset_of_transactionId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (FakeAppleConfiguation_t4052738437), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (FakeAppleExtensions_t4039399289), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (AndroidStore_t3203055206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[4] = 
{
	AndroidStore_t3203055206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (FakeStoreUIMode_t2321492887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	FakeStoreUIMode_t2321492887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (FakeStore_t3882981564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[5] = 
{
	FakeStore_t3882981564::get_offset_of_m_Biller_0(),
	FakeStore_t3882981564::get_offset_of_m_PurchasedProducts_1(),
	FakeStore_t3882981564::get_offset_of_purchaseCalled_2(),
	FakeStore_t3882981564::get_offset_of_U3CunavailableProductIdU3Ek__BackingField_3(),
	FakeStore_t3882981564::get_offset_of_UIMode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (DialogType_t1733969544)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	DialogType_t1733969544::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[2] = 
{
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_products_0(),
	U3CRetrieveProductsU3Ec__AnonStorey0_t2692708567::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (U3CPurchaseU3Ec__AnonStorey1_t2000504963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[2] = 
{
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_product_0(),
	U3CPurchaseU3Ec__AnonStorey1_t2000504963::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (RawStoreProvider_t3477922056), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (StandardPurchasingModule_t4003664591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[7] = 
{
	StandardPurchasingModule_t4003664591::get_offset_of_m_AndroidPlatform_1(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_PlatformProvider_2(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_RuntimePlatform_3(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Util_4(),
	StandardPurchasingModule_t4003664591::get_offset_of_m_Logger_5(),
	StandardPurchasingModule_t4003664591::get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_6(),
	StandardPurchasingModule_t4003664591::get_offset_of_windowsStore_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (MicrosoftConfiguration_t1310600573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[2] = 
{
	MicrosoftConfiguration_t1310600573::get_offset_of_useMock_0(),
	MicrosoftConfiguration_t1310600573::get_offset_of_module_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (StoreConfiguration_t2466794143), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[1] = 
{
	StoreConfiguration_t2466794143::get_offset_of_U3CandroidStoreU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (UIFakeStore_t3684252124), -1, sizeof(UIFakeStore_t3684252124_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[7] = 
{
	UIFakeStore_t3684252124::get_offset_of_m_CurrentDialog_5(),
	UIFakeStore_t3684252124::get_offset_of_m_LastSelectedDropdownIndex_6(),
	UIFakeStore_t3684252124::get_offset_of_UIFakeStoreCanvasPrefab_7(),
	UIFakeStore_t3684252124::get_offset_of_m_Canvas_8(),
	UIFakeStore_t3684252124::get_offset_of_m_EventSystem_9(),
	UIFakeStore_t3684252124::get_offset_of_m_ParentGameObjectPath_10(),
	UIFakeStore_t3684252124_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (DialogRequest_t2092195449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[5] = 
{
	DialogRequest_t2092195449::get_offset_of_QueryText_0(),
	DialogRequest_t2092195449::get_offset_of_OkayButtonText_1(),
	DialogRequest_t2092195449::get_offset_of_CancelButtonText_2(),
	DialogRequest_t2092195449::get_offset_of_Options_3(),
	DialogRequest_t2092195449::get_offset_of_Callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (LifecycleNotifier_t1057582876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[1] = 
{
	LifecycleNotifier_t1057582876::get_offset_of_OnDestroyCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (UnityUtil_t166323129), -1, sizeof(UnityUtil_t166323129_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1930[4] = 
{
	UnityUtil_t166323129_StaticFields::get_offset_of_s_Callbacks_2(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_CallbacksPending_3(),
	UnityUtil_t166323129_StaticFields::get_offset_of_s_PcControlledPlatforms_4(),
	UnityUtil_t166323129::get_offset_of_foregroundListeners_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (U3CDelayedCoroutineU3Ec__Iterator0_t3019257939), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1931[6] = 
{
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_delay_0(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_coroutine_1(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24this_2(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24current_3(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24disposing_4(),
	U3CDelayedCoroutineU3Ec__Iterator0_t3019257939::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (WinRTStore_t36043095), -1, sizeof(WinRTStore_t36043095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1933[6] = 
{
	WinRTStore_t36043095::get_offset_of_win8_0(),
	WinRTStore_t36043095::get_offset_of_callback_1(),
	WinRTStore_t36043095::get_offset_of_util_2(),
	WinRTStore_t36043095::get_offset_of_logger_3(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	WinRTStore_t36043095_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (DigitalEyewearAbstractBehaviour_t3719235061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[24] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mCameraOffset_8(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mCentralAnchorPoint_9(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mParentAnchorPoint_10(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mDistortionRenderingMode_11(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mDistortionRenderingLayer_12(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mEyewearType_13(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mStereoFramework_14(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mViewerName_15(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mViewerManufacturer_16(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mUseCustomViewer_17(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mCustomViewer_18(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearAbstractBehaviour_t3719235061::get_offset_of_mDistortionRenderingBhvr_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (EyewearType_t1156375578)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1940[4] = 
{
	EyewearType_t1156375578::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (StereoFramework_t1337058172)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1941[4] = 
{
	StereoFramework_t1337058172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (SerializableViewerParameters_t1300054541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1942[11] = 
{
	SerializableViewerParameters_t1300054541::get_offset_of_Version_0(),
	SerializableViewerParameters_t1300054541::get_offset_of_Name_1(),
	SerializableViewerParameters_t1300054541::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t1300054541::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t1300054541::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t1300054541::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t1300054541::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t1300054541::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t1300054541::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t1300054541::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t1300054541::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (Device_t1860504872), -1, sizeof(Device_t1860504872_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1943[1] = 
{
	Device_t1860504872_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (Mode_t2855916820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1944[3] = 
{
	Mode_t2855916820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (EyewearDevice_t1202635122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (EyeID_t642957731)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[4] = 
{
	EyeID_t642957731::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (EyewearCalibrationReading_t1521251591)+ sizeof (Il2CppObject), sizeof(EyewearCalibrationReading_t1521251591_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1947[5] = 
{
	EyewearCalibrationReading_t1521251591::get_offset_of_pose_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_scale_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerX_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_centerY_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EyewearCalibrationReading_t1521251591::get_offset_of_unused_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (Tracker_t189438242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[1] = 
{
	Tracker_t189438242::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (DeviceTracker_t2183873360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (DeviceTrackerAbstractBehaviour_t2651534015), -1, sizeof(DeviceTrackerAbstractBehaviour_t2651534015_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1953[11] = 
{
	DeviceTrackerAbstractBehaviour_t2651534015_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_2(),
	DeviceTrackerAbstractBehaviour_t2651534015_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_3(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mAutoInitTracker_4(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mAutoStartTracker_5(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mPosePrediction_6(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mModelCorrectionMode_7(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mModelTransformEnabled_8(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mModelTransform_9(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mTrackerStarted_10(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mTrackerWasActiveBeforePause_11(),
	DeviceTrackerAbstractBehaviour_t2651534015::get_offset_of_mTrackerWasActiveBeforeDisabling_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (DistortionRenderingMode_t3766399464)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[4] = 
{
	DistortionRenderingMode_t3766399464::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (DistortionRenderingBehaviour_t2945034146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1955[12] = 
{
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t2945034146::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (PlayModeEyewearUserCalibratorImpl_t117253723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (PlayModeEyewearCalibrationProfileManagerImpl_t3632467967), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (PlayModeEyewearDevice_t2977282393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[3] = 
{
	PlayModeEyewearDevice_t2977282393::get_offset_of_mProfileManager_1(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mCalibrator_2(),
	PlayModeEyewearDevice_t2977282393::get_offset_of_mDummyPredictiveTracking_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (DedicatedEyewearDevice_t22891981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[2] = 
{
	DedicatedEyewearDevice_t22891981::get_offset_of_mProfileManager_1(),
	DedicatedEyewearDevice_t22891981::get_offset_of_mCalibrator_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (CameraConfigurationUtility_t3904398347), -1, sizeof(CameraConfigurationUtility_t3904398347_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1962[6] = 
{
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t3904398347_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (BaseCameraConfiguration_t38459502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[11] = 
{
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t38459502::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t38459502::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t38459502::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t38459502::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t38459502::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t38459502::get_offset_of_mVideoBgMgr_8(),
	BaseCameraConfiguration_t38459502::get_offset_of_mBackgroundPlaneBehaviour_9(),
	BaseCameraConfiguration_t38459502::get_offset_of_mCameraParameterChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (BaseStereoViewerCameraConfiguration_t1102239676), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[5] = 
{
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mPrimaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSecondaryCamera_12(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mSkewFrustum_13(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenWidth_14(),
	BaseStereoViewerCameraConfiguration_t1102239676::get_offset_of_mScreenHeight_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (StereoViewerCameraConfiguration_t3365023487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[7] = 
{
	0,
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftNearClipPlane_17(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedLeftFarClipPlane_18(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightNearClipPlane_19(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mLastAppliedRightFarClipPlane_20(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mCameraOffset_21(),
	StereoViewerCameraConfiguration_t3365023487::get_offset_of_mIsDistorted_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (MixedRealityController_t1276557833), -1, sizeof(MixedRealityController_t1276557833_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1968[14] = 
{
	MixedRealityController_t1276557833_StaticFields::get_offset_of_mInstance_0(),
	MixedRealityController_t1276557833::get_offset_of_mVuforiaBehaviour_1(),
	MixedRealityController_t1276557833::get_offset_of_mDigitalEyewearBehaviour_2(),
	MixedRealityController_t1276557833::get_offset_of_mVideoBackgroundManager_3(),
	MixedRealityController_t1276557833::get_offset_of_mViewerHasBeenSetExternally_4(),
	MixedRealityController_t1276557833::get_offset_of_mViewerParameters_5(),
	MixedRealityController_t1276557833::get_offset_of_mFrameWorkHasBeenSetExternally_6(),
	MixedRealityController_t1276557833::get_offset_of_mStereoFramework_7(),
	MixedRealityController_t1276557833::get_offset_of_mCentralAnchorPoint_8(),
	MixedRealityController_t1276557833::get_offset_of_mLeftCameraOfExternalSDK_9(),
	MixedRealityController_t1276557833::get_offset_of_mRightCameraOfExternalSDK_10(),
	MixedRealityController_t1276557833::get_offset_of_mObjectTrackerStopped_11(),
	MixedRealityController_t1276557833::get_offset_of_mMarkerTrackerStopped_12(),
	MixedRealityController_t1276557833::get_offset_of_mAutoStopCameraIfNotRequired_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (Mode_t38013191)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1969[7] = 
{
	Mode_t38013191::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (RotationalDeviceTracker_t3644694819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (MODEL_CORRECTION_MODE_t111727860)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1971[4] = 
{
	MODEL_CORRECTION_MODE_t111727860::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (ViewerParameters_t1247673784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[1] = 
{
	ViewerParameters_t1247673784::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (CustomViewerParameters_t779886969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[7] = 
{
	CustomViewerParameters_t779886969::get_offset_of_mVersion_1(),
	CustomViewerParameters_t779886969::get_offset_of_mName_2(),
	CustomViewerParameters_t779886969::get_offset_of_mManufacturer_3(),
	CustomViewerParameters_t779886969::get_offset_of_mButtonType_4(),
	CustomViewerParameters_t779886969::get_offset_of_mScreenToLensDistance_5(),
	CustomViewerParameters_t779886969::get_offset_of_mTrayAlignment_6(),
	CustomViewerParameters_t779886969::get_offset_of_mMagnet_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (DeviceTrackingManager_t2097550852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[4] = 
{
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t2097550852::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t2097550852::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t2097550852::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (FactorySetter_t648583075), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (EyewearCalibrationProfileManagerImpl_t2025108506), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (BackgroundPlaneAbstractBehaviour_t3732945727), -1, sizeof(BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1979[13] = 
{
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVideoBgConfigChanged_5(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mQBehaviour_7(),
	BackgroundPlaneAbstractBehaviour_t3732945727_StaticFields::get_offset_of_maxDisplacement_8(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_defaultNumDivisions_9(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mMesh_10(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mStereoDepth_11(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mBackgroundOffset_12(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mVuforiaBehaviour_13(),
	BackgroundPlaneAbstractBehaviour_t3732945727::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (EyewearUserCalibratorImpl_t1518014586), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t3894463544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1984[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t3894463544::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (RotationalDeviceTrackerImpl_t832065887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (IOSCamRecoveringHelper_t979448318), -1, sizeof(IOSCamRecoveringHelper_t979448318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1987[9] = 
{
	0,
	0,
	0,
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sHasJustResumed_3(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_4(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sCheckedFailedFrameCounter_5(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_6(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_7(),
	IOSCamRecoveringHelper_t979448318_StaticFields::get_offset_of_sRecoveryAttemptCounter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (MeshUtils_t2110180948), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (ExternalStereoCameraConfiguration_t4187656756), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[21] = 
{
	0,
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftNearClipPlane_17(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftFarClipPlane_18(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightNearClipPlane_19(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightFarClipPlane_20(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightVerticalVirtualFoV_23(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_24(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedLeftProjection_25(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mLastAppliedRightProjection_26(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftNearClipPlane_27(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftFarClipPlane_28(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightNearClipPlane_29(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightFarClipPlane_30(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftVerticalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewLeftHorizontalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightVerticalVirtualFoV_33(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mNewRightHorizontalVirtualFoV_34(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetLeftMatrix_35(),
	ExternalStereoCameraConfiguration_t4187656756::get_offset_of_mExternallySetRightMatrix_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (NullHideExcessAreaClipping_t2290611987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (StencilHideExcessAreaClipping_t3377289090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[13] = 
{
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mVuforiaBehaviour_8(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mPlanesActivated_9(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlane_10(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalPos_11(),
	StencilHideExcessAreaClipping_t3377289090::get_offset_of_mBgPlaneLocalScale_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (LegacyHideExcessAreaClipping_t3475621185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1992[21] = 
{
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mVuforiaBehaviour_13(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mDeactivatedHideBehaviours_15(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mPlanesActivated_16(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mLeftPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mRightPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mBottomPlaneCachedScale_19(),
	LegacyHideExcessAreaClipping_t3475621185::get_offset_of_mTopPlaneCachedScale_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (DedicatedEyewearCameraConfiguration_t816511398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1993[13] = 
{
	0,
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mPrimaryCamera_12(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mSecondaryCamera_13(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenWidth_14(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mScreenHeight_15(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNeedToCheckStereo_16(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedNearClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedFarClipPlane_18(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mLastAppliedVirtualFoV_19(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewNearClipPlane_20(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewFarClipPlane_21(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mNewVirtualFoV_22(),
	DedicatedEyewearCameraConfiguration_t816511398::get_offset_of_mEyewearDevice_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (NullCameraConfiguration_t133234522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[1] = 
{
	NullCameraConfiguration_t133234522::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (MonoCameraConfiguration_t3796201132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[7] = 
{
	0,
	MonoCameraConfiguration_t3796201132::get_offset_of_mPrimaryCamera_12(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortWidth_13(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mCameraViewPortHeight_14(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedNearClipPlane_15(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFarClipPlane_16(),
	MonoCameraConfiguration_t3796201132::get_offset_of_mLastAppliedFoV_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (UnityCameraExtensions_t2392150382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1996[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (View_t3542740111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1997[6] = 
{
	View_t3542740111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (ViewerParametersList_t3152440868), -1, sizeof(ViewerParametersList_t3152440868_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1999[2] = 
{
	ViewerParametersList_t3152440868::get_offset_of_mNativeVPL_0(),
	ViewerParametersList_t3152440868_StaticFields::get_offset_of_mListForAuthoringTools_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
