﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.FactoryFunc`1<System.Object>
struct FactoryFunc_1_t741989655;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void LitJson.FactoryFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void FactoryFunc_1__ctor_m1059335099_gshared (FactoryFunc_1_t741989655 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define FactoryFunc_1__ctor_m1059335099(__this, ___object0, ___method1, method) ((  void (*) (FactoryFunc_1_t741989655 *, Il2CppObject *, IntPtr_t, const MethodInfo*))FactoryFunc_1__ctor_m1059335099_gshared)(__this, ___object0, ___method1, method)
// T LitJson.FactoryFunc`1<System.Object>::Invoke()
extern "C"  Il2CppObject * FactoryFunc_1_Invoke_m2225877924_gshared (FactoryFunc_1_t741989655 * __this, const MethodInfo* method);
#define FactoryFunc_1_Invoke_m2225877924(__this, method) ((  Il2CppObject * (*) (FactoryFunc_1_t741989655 *, const MethodInfo*))FactoryFunc_1_Invoke_m2225877924_gshared)(__this, method)
// System.IAsyncResult LitJson.FactoryFunc`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FactoryFunc_1_BeginInvoke_m4292257436_gshared (FactoryFunc_1_t741989655 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define FactoryFunc_1_BeginInvoke_m4292257436(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (FactoryFunc_1_t741989655 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))FactoryFunc_1_BeginInvoke_m4292257436_gshared)(__this, ___callback0, ___object1, method)
// T LitJson.FactoryFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * FactoryFunc_1_EndInvoke_m3177408120_gshared (FactoryFunc_1_t741989655 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define FactoryFunc_1_EndInvoke_m3177408120(__this, ___result0, method) ((  Il2CppObject * (*) (FactoryFunc_1_t741989655 *, Il2CppObject *, const MethodInfo*))FactoryFunc_1_EndInvoke_m3177408120_gshared)(__this, ___result0, method)
