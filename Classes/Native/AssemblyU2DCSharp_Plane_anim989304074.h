﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Plane_anim
struct  Plane_anim_t989304074  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Plane_anim::planeButton
	GameObject_t1756533147 * ___planeButton_2;
	// UnityEngine.Animator Plane_anim::planeAnim
	Animator_t69676727 * ___planeAnim_3;

public:
	inline static int32_t get_offset_of_planeButton_2() { return static_cast<int32_t>(offsetof(Plane_anim_t989304074, ___planeButton_2)); }
	inline GameObject_t1756533147 * get_planeButton_2() const { return ___planeButton_2; }
	inline GameObject_t1756533147 ** get_address_of_planeButton_2() { return &___planeButton_2; }
	inline void set_planeButton_2(GameObject_t1756533147 * value)
	{
		___planeButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___planeButton_2, value);
	}

	inline static int32_t get_offset_of_planeAnim_3() { return static_cast<int32_t>(offsetof(Plane_anim_t989304074, ___planeAnim_3)); }
	inline Animator_t69676727 * get_planeAnim_3() const { return ___planeAnim_3; }
	inline Animator_t69676727 ** get_address_of_planeAnim_3() { return &___planeAnim_3; }
	inline void set_planeAnim_3(Animator_t69676727 * value)
	{
		___planeAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___planeAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
