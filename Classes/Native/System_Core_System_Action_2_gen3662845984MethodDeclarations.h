﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen129729484MethodDeclarations.h"

// System.Void System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3657728487(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3662845984 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4071786849_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>::Invoke(T1,T2)
#define Action_2_Invoke_m3067079544(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3662845984 *, Vector3_t2243707580 , JsonWriter_t1927598499 *, const MethodInfo*))Action_2_Invoke_m12962276_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1510557583(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3662845984 *, Vector3_t2243707580 , JsonWriter_t1927598499 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m1370092077_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<UnityEngine.Vector3,LitJson.JsonWriter>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1259289413(__this, ___result0, method) ((  void (*) (Action_2_t3662845984 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m2096582895_gshared)(__this, ___result0, method)
