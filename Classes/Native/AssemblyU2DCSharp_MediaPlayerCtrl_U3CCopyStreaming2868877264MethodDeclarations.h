﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1
struct U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::.ctor()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1__ctor_m2613178105 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3590945907 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3116110715 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::MoveNext()
extern "C"  bool U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_MoveNext_m1286643867 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::Dispose()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_Dispose_m3256119066 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<CopyStreamingAssetVideoAndLoad>c__Iterator1::Reset()
extern "C"  void U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_Reset_m3444348736 (U3CCopyStreamingAssetVideoAndLoadU3Ec__Iterator1_t2868877264 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
