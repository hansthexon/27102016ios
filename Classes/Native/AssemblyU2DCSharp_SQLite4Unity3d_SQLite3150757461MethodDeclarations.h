﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_Result1450270481.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ConfigOpt4001894229.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ColType2341528904.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ExtendedR1691412462.h"

// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Open(System.String,System.IntPtr&)
extern "C"  int32_t SQLite3_Open_m2418927504 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Open(System.String,System.IntPtr&,System.Int32,System.IntPtr)
extern "C"  int32_t SQLite3_Open_m295370215 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, IntPtr_t* ___db1, int32_t ___flags2, IntPtr_t ___zvfs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Open(System.Byte[],System.IntPtr&,System.Int32,System.IntPtr)
extern "C"  int32_t SQLite3_Open_m3889102194 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___filename0, IntPtr_t* ___db1, int32_t ___flags2, IntPtr_t ___zvfs3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Open16(System.String,System.IntPtr&)
extern "C"  int32_t SQLite3_Open16_m2635868059 (Il2CppObject * __this /* static, unused */, String_t* ___filename0, IntPtr_t* ___db1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::EnableLoadExtension(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_EnableLoadExtension_m964719143 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, int32_t ___onoff1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Close(System.IntPtr)
extern "C"  int32_t SQLite3_Close_m1036340778 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Initialize()
extern "C"  int32_t SQLite3_Initialize_m1090735632 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Shutdown()
extern "C"  int32_t SQLite3_Shutdown_m933277928 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Config(SQLite4Unity3d.SQLite3/ConfigOption)
extern "C"  int32_t SQLite3_Config_m3612444335 (Il2CppObject * __this /* static, unused */, int32_t ___option0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::SetDirectory(System.UInt32,System.String)
extern "C"  int32_t SQLite3_SetDirectory_m1504925041 (Il2CppObject * __this /* static, unused */, uint32_t ___directoryType0, String_t* ___directoryPath1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::BusyTimeout(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_BusyTimeout_m3465391879 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, int32_t ___milliseconds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::Changes(System.IntPtr)
extern "C"  int32_t SQLite3_Changes_m1047578995 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Prepare2(System.IntPtr,System.String,System.Int32,System.IntPtr&,System.IntPtr)
extern "C"  int32_t SQLite3_Prepare2_m1339478572 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, String_t* ___sql1, int32_t ___numBytes2, IntPtr_t* ___stmt3, IntPtr_t ___pzTail4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::Prepare2(System.IntPtr,System.String)
extern "C"  IntPtr_t SQLite3_Prepare2_m1754241278 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, String_t* ___query1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Step(System.IntPtr)
extern "C"  int32_t SQLite3_Step_m2897001430 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Reset(System.IntPtr)
extern "C"  int32_t SQLite3_Reset_m3892495633 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/Result SQLite4Unity3d.SQLite3::Finalize(System.IntPtr)
extern "C"  int32_t SQLite3_Finalize_m1083398254 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SQLite4Unity3d.SQLite3::LastInsertRowid(System.IntPtr)
extern "C"  int64_t SQLite3_LastInsertRowid_m191530479 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::Errmsg(System.IntPtr)
extern "C"  IntPtr_t SQLite3_Errmsg_m4213932121 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLite3::GetErrmsg(System.IntPtr)
extern "C"  String_t* SQLite3_GetErrmsg_m2984016983 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindParameterIndex(System.IntPtr,System.String)
extern "C"  int32_t SQLite3_BindParameterIndex_m3673864820 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindNull(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_BindNull_m2302027013 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindInt(System.IntPtr,System.Int32,System.Int32)
extern "C"  int32_t SQLite3_BindInt_m3650093282 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int32_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindInt64(System.IntPtr,System.Int32,System.Int64)
extern "C"  int32_t SQLite3_BindInt64_m4139043157 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, int64_t ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindDouble(System.IntPtr,System.Int32,System.Double)
extern "C"  int32_t SQLite3_BindDouble_m917469905 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, double ___val2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindText(System.IntPtr,System.Int32,System.String,System.Int32,System.IntPtr)
extern "C"  int32_t SQLite3_BindText_m3472253532 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, String_t* ___val2, int32_t ___n3, IntPtr_t ___free4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::BindBlob(System.IntPtr,System.Int32,System.Byte[],System.Int32,System.IntPtr)
extern "C"  int32_t SQLite3_BindBlob_m1905565561 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, ByteU5BU5D_t3397334013* ___val2, int32_t ___n3, IntPtr_t ___free4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::ColumnCount(System.IntPtr)
extern "C"  int32_t SQLite3_ColumnCount_m3918418219 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::ColumnName(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SQLite3_ColumnName_m3478136233 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::ColumnName16Internal(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SQLite3_ColumnName16Internal_m4267294289 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLite3::ColumnName16(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_ColumnName16_m4083644 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/ColType SQLite4Unity3d.SQLite3::ColumnType(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_ColumnType_m566938654 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::ColumnInt(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_ColumnInt_m4075567720 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 SQLite4Unity3d.SQLite3::ColumnInt64(System.IntPtr,System.Int32)
extern "C"  int64_t SQLite3_ColumnInt64_m2191260063 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double SQLite4Unity3d.SQLite3::ColumnDouble(System.IntPtr,System.Int32)
extern "C"  double SQLite3_ColumnDouble_m3033091719 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::ColumnText(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SQLite3_ColumnText_m281500291 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::ColumnText16(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SQLite3_ColumnText16_m768615716 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLite3::ColumnBlob(System.IntPtr,System.Int32)
extern "C"  IntPtr_t SQLite3_ColumnBlob_m628758177 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::ColumnBytes(System.IntPtr,System.Int32)
extern "C"  int32_t SQLite3_ColumnBytes_m304146018 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLite3::ColumnString(System.IntPtr,System.Int32)
extern "C"  String_t* SQLite3_ColumnString_m3456667847 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] SQLite4Unity3d.SQLite3::ColumnByteArray(System.IntPtr,System.Int32)
extern "C"  ByteU5BU5D_t3397334013* SQLite3_ColumnByteArray_m3033705312 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLite3/ExtendedResult SQLite4Unity3d.SQLite3::ExtendedErrCode(System.IntPtr)
extern "C"  int32_t SQLite3_ExtendedErrCode_m3557837058 (Il2CppObject * __this /* static, unused */, IntPtr_t ___db0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLite3::LibVersionNumber()
extern "C"  int32_t SQLite3_LibVersionNumber_m3078485688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
