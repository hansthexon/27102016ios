﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_Expression_1_g2729858435MethodDeclarations.h"

// System.Void System.Linq.Expressions.Expression`1<System.Func`2<RoomObject,System.Boolean>>::.ctor(System.Linq.Expressions.Expression,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>)
#define Expression_1__ctor_m3567297764(__this, ___body0, ___parameters1, method) ((  void (*) (Expression_1_t2968240103 *, Expression_t114864668 *, ReadOnlyCollection_1_t3201290647 *, const MethodInfo*))Expression_1__ctor_m2373241160_gshared)(__this, ___body0, ___parameters1, method)
