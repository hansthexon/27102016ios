﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_Comp4163790932MethodDeclarations.h"

// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>::.ctor()
#define CompileResult__ctor_m3405633809(__this, method) ((  void (*) (CompileResult_t162244657 *, const MethodInfo*))CompileResult__ctor_m3789497005_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>::get_CommandText()
#define CompileResult_get_CommandText_m641419843(__this, method) ((  String_t* (*) (CompileResult_t162244657 *, const MethodInfo*))CompileResult_get_CommandText_m2764066139_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>::set_CommandText(System.String)
#define CompileResult_set_CommandText_m3268937738(__this, ___value0, method) ((  void (*) (CompileResult_t162244657 *, String_t*, const MethodInfo*))CompileResult_set_CommandText_m1496493232_gshared)(__this, ___value0, method)
// System.Object SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>::get_Value()
#define CompileResult_get_Value_m429688868(__this, method) ((  Il2CppObject * (*) (CompileResult_t162244657 *, const MethodInfo*))CompileResult_get_Value_m2706335512_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>::set_Value(System.Object)
#define CompileResult_set_Value_m1856125849(__this, ___value0, method) ((  void (*) (CompileResult_t162244657 *, Il2CppObject *, const MethodInfo*))CompileResult_set_Value_m420182477_gshared)(__this, ___value0, method)
