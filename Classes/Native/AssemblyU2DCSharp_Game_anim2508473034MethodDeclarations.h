﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Game_anim
struct Game_anim_t2508473034;

#include "codegen/il2cpp-codegen.h"

// System.Void Game_anim::.ctor()
extern "C"  void Game_anim__ctor_m4041479021 (Game_anim_t2508473034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_anim::Start()
extern "C"  void Game_anim_Start_m1848421605 (Game_anim_t2508473034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_anim::Update()
extern "C"  void Game_anim_Update_m1597095400 (Game_anim_t2508473034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Game_anim::gameAnimate()
extern "C"  void Game_anim_gameAnimate_m3144435832 (Game_anim_t2508473034 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
