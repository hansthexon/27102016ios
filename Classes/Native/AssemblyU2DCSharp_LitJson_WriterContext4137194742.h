﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.WriterContext
struct  WriterContext_t4137194742  : public Il2CppObject
{
public:
	// System.Int32 LitJson.WriterContext::Count
	int32_t ___Count_0;
	// System.Int32 LitJson.WriterContext::Padding
	int32_t ___Padding_1;
	// System.Boolean LitJson.WriterContext::InArray
	bool ___InArray_2;
	// System.Boolean LitJson.WriterContext::InObject
	bool ___InObject_3;
	// System.Boolean LitJson.WriterContext::ExpectingValue
	bool ___ExpectingValue_4;

public:
	inline static int32_t get_offset_of_Count_0() { return static_cast<int32_t>(offsetof(WriterContext_t4137194742, ___Count_0)); }
	inline int32_t get_Count_0() const { return ___Count_0; }
	inline int32_t* get_address_of_Count_0() { return &___Count_0; }
	inline void set_Count_0(int32_t value)
	{
		___Count_0 = value;
	}

	inline static int32_t get_offset_of_Padding_1() { return static_cast<int32_t>(offsetof(WriterContext_t4137194742, ___Padding_1)); }
	inline int32_t get_Padding_1() const { return ___Padding_1; }
	inline int32_t* get_address_of_Padding_1() { return &___Padding_1; }
	inline void set_Padding_1(int32_t value)
	{
		___Padding_1 = value;
	}

	inline static int32_t get_offset_of_InArray_2() { return static_cast<int32_t>(offsetof(WriterContext_t4137194742, ___InArray_2)); }
	inline bool get_InArray_2() const { return ___InArray_2; }
	inline bool* get_address_of_InArray_2() { return &___InArray_2; }
	inline void set_InArray_2(bool value)
	{
		___InArray_2 = value;
	}

	inline static int32_t get_offset_of_InObject_3() { return static_cast<int32_t>(offsetof(WriterContext_t4137194742, ___InObject_3)); }
	inline bool get_InObject_3() const { return ___InObject_3; }
	inline bool* get_address_of_InObject_3() { return &___InObject_3; }
	inline void set_InObject_3(bool value)
	{
		___InObject_3 = value;
	}

	inline static int32_t get_offset_of_ExpectingValue_4() { return static_cast<int32_t>(offsetof(WriterContext_t4137194742, ___ExpectingValue_4)); }
	inline bool get_ExpectingValue_4() const { return ___ExpectingValue_4; }
	inline bool* get_address_of_ExpectingValue_4() { return &___ExpectingValue_4; }
	inline void set_ExpectingValue_4(bool value)
	{
		___ExpectingValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
