﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RoomObject
struct  RoomObject_t2982870316  : public Il2CppObject
{
public:
	// System.String RoomObject::<ObjId>k__BackingField
	String_t* ___U3CObjIdU3Ek__BackingField_0;
	// System.String RoomObject::<ObjPNG>k__BackingField
	String_t* ___U3CObjPNGU3Ek__BackingField_1;
	// System.String RoomObject::<Room>k__BackingField
	String_t* ___U3CRoomU3Ek__BackingField_2;
	// System.String RoomObject::<ReachScore>k__BackingField
	String_t* ___U3CReachScoreU3Ek__BackingField_3;
	// System.String RoomObject::<Earned>k__BackingField
	String_t* ___U3CEarnedU3Ek__BackingField_4;
	// System.String RoomObject::<PlayerInRoomPosXY>k__BackingField
	String_t* ___U3CPlayerInRoomPosXYU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CObjIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CObjIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CObjIdU3Ek__BackingField_0() const { return ___U3CObjIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CObjIdU3Ek__BackingField_0() { return &___U3CObjIdU3Ek__BackingField_0; }
	inline void set_U3CObjIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CObjIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CObjIdU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CObjPNGU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CObjPNGU3Ek__BackingField_1)); }
	inline String_t* get_U3CObjPNGU3Ek__BackingField_1() const { return ___U3CObjPNGU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CObjPNGU3Ek__BackingField_1() { return &___U3CObjPNGU3Ek__BackingField_1; }
	inline void set_U3CObjPNGU3Ek__BackingField_1(String_t* value)
	{
		___U3CObjPNGU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CObjPNGU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CRoomU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CRoomU3Ek__BackingField_2)); }
	inline String_t* get_U3CRoomU3Ek__BackingField_2() const { return ___U3CRoomU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRoomU3Ek__BackingField_2() { return &___U3CRoomU3Ek__BackingField_2; }
	inline void set_U3CRoomU3Ek__BackingField_2(String_t* value)
	{
		___U3CRoomU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CRoomU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CReachScoreU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CReachScoreU3Ek__BackingField_3)); }
	inline String_t* get_U3CReachScoreU3Ek__BackingField_3() const { return ___U3CReachScoreU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CReachScoreU3Ek__BackingField_3() { return &___U3CReachScoreU3Ek__BackingField_3; }
	inline void set_U3CReachScoreU3Ek__BackingField_3(String_t* value)
	{
		___U3CReachScoreU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CReachScoreU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CEarnedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CEarnedU3Ek__BackingField_4)); }
	inline String_t* get_U3CEarnedU3Ek__BackingField_4() const { return ___U3CEarnedU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CEarnedU3Ek__BackingField_4() { return &___U3CEarnedU3Ek__BackingField_4; }
	inline void set_U3CEarnedU3Ek__BackingField_4(String_t* value)
	{
		___U3CEarnedU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEarnedU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CPlayerInRoomPosXYU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RoomObject_t2982870316, ___U3CPlayerInRoomPosXYU3Ek__BackingField_5)); }
	inline String_t* get_U3CPlayerInRoomPosXYU3Ek__BackingField_5() const { return ___U3CPlayerInRoomPosXYU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CPlayerInRoomPosXYU3Ek__BackingField_5() { return &___U3CPlayerInRoomPosXYU3Ek__BackingField_5; }
	inline void set_U3CPlayerInRoomPosXYU3Ek__BackingField_5(String_t* value)
	{
		___U3CPlayerInRoomPosXYU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPlayerInRoomPosXYU3Ek__BackingField_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
