﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1902082073;
// System.Collections.Generic.IDictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct IDictionary_2_t2734145610;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct ICollection_1_t1800110070;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>[]
struct KeyValuePair_2U5BU5D_t4160868962;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>
struct IEnumerator_1_t4262898534;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct KeyCollection_t2923592664;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct ValueCollection_t3438122032;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1760119595.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor()
extern "C"  void Dictionary_2__ctor_m3634293356_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3634293356(__this, method) ((  void (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2__ctor_m3634293356_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3020596541_gshared (Dictionary_2_t440094893 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3020596541(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3020596541_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m2103437098_gshared (Dictionary_2_t440094893 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m2103437098(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2103437098_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m91720987_gshared (Dictionary_2_t440094893 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m91720987(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t440094893 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m91720987_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2444159867_gshared (Dictionary_2_t440094893 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2444159867(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2444159867_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1426693808_gshared (Dictionary_2_t440094893 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1426693808(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t440094893 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1426693808_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2431231145_gshared (Dictionary_2_t440094893 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2431231145(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t440094893 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m2431231145_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m679439298_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m679439298(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m679439298_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1351802442_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1351802442(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1351802442_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3615119078_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3615119078(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3615119078_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m640301868_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m640301868(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m640301868_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m791435229_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m791435229(__this, method) ((  bool (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m791435229_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m835899496_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m835899496(__this, method) ((  bool (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m835899496_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2758217012_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2758217012(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2758217012_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3726485741_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3726485741(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3726485741_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1132400946_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1132400946(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1132400946_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3900471114_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3900471114(__this, ___key0, method) ((  bool (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3900471114_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2840318681_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2840318681(__this, ___key0, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2840318681_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m842536380_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m842536380(__this, method) ((  bool (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m842536380_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3890776920_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3890776920(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3890776920_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2839103486_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2839103486(__this, method) ((  bool (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m2839103486_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1072299913_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2_t2492407411  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1072299913(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t440094893 *, KeyValuePair_2_t2492407411 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1072299913_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m715141919_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2_t2492407411  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m715141919(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t440094893 *, KeyValuePair_2_t2492407411 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m715141919_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1658139197_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2U5BU5D_t4160868962* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1658139197(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t440094893 *, KeyValuePair_2U5BU5D_t4160868962*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1658139197_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3209003176_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2_t2492407411  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3209003176(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t440094893 *, KeyValuePair_2_t2492407411 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3209003176_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m309534568_gshared (Dictionary_2_t440094893 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m309534568(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m309534568_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m163130887_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m163130887(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m163130887_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4207359798_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4207359798(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4207359798_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3803325785_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3803325785(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3803325785_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2163055164_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2163055164(__this, method) ((  int32_t (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_get_Count_m2163055164_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Item(TKey)
extern "C"  IndexInfo_t848034765  Dictionary_2_get_Item_m1362623473_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1362623473(__this, ___key0, method) ((  IndexInfo_t848034765  (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1362623473_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3534888674_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3534888674(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_set_Item_m3534888674_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1596672500_gshared (Dictionary_2_t440094893 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1596672500(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t440094893 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1596672500_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2789932639_gshared (Dictionary_2_t440094893 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2789932639(__this, ___size0, method) ((  void (*) (Dictionary_2_t440094893 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2789932639_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m491683745_gshared (Dictionary_2_t440094893 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m491683745(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m491683745_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2492407411  Dictionary_2_make_pair_m2579036051_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2579036051(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2492407411  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_make_pair_m2579036051_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m679525787_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m679525787(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_pick_key_m679525787_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::pick_value(TKey,TValue)
extern "C"  IndexInfo_t848034765  Dictionary_2_pick_value_m4014539291_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m4014539291(__this /* static, unused */, ___key0, ___value1, method) ((  IndexInfo_t848034765  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_pick_value_m4014539291_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m775603190_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2U5BU5D_t4160868962* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m775603190(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t440094893 *, KeyValuePair_2U5BU5D_t4160868962*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m775603190_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Resize()
extern "C"  void Dictionary_2_Resize_m3800280170_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3800280170(__this, method) ((  void (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_Resize_m3800280170_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3402002127_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3402002127(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_Add_m3402002127_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Clear()
extern "C"  void Dictionary_2_Clear_m984180591_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m984180591(__this, method) ((  void (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_Clear_m984180591_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4028955705_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4028955705(__this, ___key0, method) ((  bool (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m4028955705_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2481025913_gshared (Dictionary_2_t440094893 * __this, IndexInfo_t848034765  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2481025913(__this, ___value0, method) ((  bool (*) (Dictionary_2_t440094893 *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_ContainsValue_m2481025913_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m455762620_gshared (Dictionary_2_t440094893 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m455762620(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t440094893 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m455762620_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2978275474_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2978275474(__this, ___sender0, method) ((  void (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2978275474_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2304439799_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2304439799(__this, ___key0, method) ((  bool (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m2304439799_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m832847054_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, IndexInfo_t848034765 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m832847054(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t440094893 *, Il2CppObject *, IndexInfo_t848034765 *, const MethodInfo*))Dictionary_2_TryGetValue_m832847054_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Keys()
extern "C"  KeyCollection_t2923592664 * Dictionary_2_get_Keys_m2819321701_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2819321701(__this, method) ((  KeyCollection_t2923592664 * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_get_Keys_m2819321701_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Values()
extern "C"  ValueCollection_t3438122032 * Dictionary_2_get_Values_m602769453_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m602769453(__this, method) ((  ValueCollection_t3438122032 * (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_get_Values_m602769453_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m400256044_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m400256044(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m400256044_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ToTValue(System.Object)
extern "C"  IndexInfo_t848034765  Dictionary_2_ToTValue_m352477044_gshared (Dictionary_2_t440094893 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m352477044(__this, ___value0, method) ((  IndexInfo_t848034765  (*) (Dictionary_2_t440094893 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m352477044_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m308602102_gshared (Dictionary_2_t440094893 * __this, KeyValuePair_2_t2492407411  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m308602102(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t440094893 *, KeyValuePair_2_t2492407411 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m308602102_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetEnumerator()
extern "C"  Enumerator_t1760119595  Dictionary_2_GetEnumerator_m2221815687_gshared (Dictionary_2_t440094893 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2221815687(__this, method) ((  Enumerator_t1760119595  (*) (Dictionary_2_t440094893 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2221815687_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m2348193036_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, IndexInfo_t848034765  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m2348193036(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, IndexInfo_t848034765 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2348193036_gshared)(__this /* static, unused */, ___key0, ___value1, method)
