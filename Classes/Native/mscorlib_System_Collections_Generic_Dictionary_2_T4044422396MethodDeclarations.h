﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3566403282MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m3170308176(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t4044422396 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1599957462_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m1833868456(__this, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4044422396 *, String_t*, IndexInfo_t848034765 , const MethodInfo*))Transform_1_Invoke_m3190314638_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1325483423(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t4044422396 *, String_t*, IndexInfo_t848034765 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m4241234627_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m1082588570(__this, ___result0, method) ((  DictionaryEntry_t3048875398  (*) (Transform_1_t4044422396 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m432553760_gshared)(__this, ___result0, method)
