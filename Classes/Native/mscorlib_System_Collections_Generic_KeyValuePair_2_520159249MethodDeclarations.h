﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4007301440(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t520159249 *, String_t*, IndexInfo_t848034765 , const MethodInfo*))KeyValuePair_2__ctor_m950010662_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Key()
#define KeyValuePair_2_get_Key_m3542339134(__this, method) ((  String_t* (*) (KeyValuePair_2_t520159249 *, const MethodInfo*))KeyValuePair_2_get_Key_m3094063940_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2449705669(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t520159249 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m2868193553_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Value()
#define KeyValuePair_2_get_Value_m1146943806(__this, method) ((  IndexInfo_t848034765  (*) (KeyValuePair_2_t520159249 *, const MethodInfo*))KeyValuePair_2_get_Value_m1745261316_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1349564293(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t520159249 *, IndexInfo_t848034765 , const MethodInfo*))KeyValuePair_2_set_Value_m2932767089_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::ToString()
#define KeyValuePair_2_ToString_m4224369147(__this, method) ((  String_t* (*) (KeyValuePair_2_t520159249 *, const MethodInfo*))KeyValuePair_2_ToString_m3738640143_gshared)(__this, method)
