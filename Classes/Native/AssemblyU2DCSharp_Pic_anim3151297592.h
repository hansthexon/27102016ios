﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pic_anim
struct  Pic_anim_t3151297592  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Pic_anim::picButton
	GameObject_t1756533147 * ___picButton_2;
	// UnityEngine.Animator Pic_anim::picAnim
	Animator_t69676727 * ___picAnim_3;

public:
	inline static int32_t get_offset_of_picButton_2() { return static_cast<int32_t>(offsetof(Pic_anim_t3151297592, ___picButton_2)); }
	inline GameObject_t1756533147 * get_picButton_2() const { return ___picButton_2; }
	inline GameObject_t1756533147 ** get_address_of_picButton_2() { return &___picButton_2; }
	inline void set_picButton_2(GameObject_t1756533147 * value)
	{
		___picButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___picButton_2, value);
	}

	inline static int32_t get_offset_of_picAnim_3() { return static_cast<int32_t>(offsetof(Pic_anim_t3151297592, ___picAnim_3)); }
	inline Animator_t69676727 * get_picAnim_3() const { return ___picAnim_3; }
	inline Animator_t69676727 ** get_address_of_picAnim_3() { return &___picAnim_3; }
	inline void set_picAnim_3(Animator_t69676727 * value)
	{
		___picAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___picAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
