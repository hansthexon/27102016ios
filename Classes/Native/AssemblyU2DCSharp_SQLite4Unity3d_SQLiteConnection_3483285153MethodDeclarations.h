﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey18
struct U3CUpdateAllU3Ec__AnonStorey18_t3483285153;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey18::.ctor()
extern "C"  void U3CUpdateAllU3Ec__AnonStorey18__ctor_m506082482 (U3CUpdateAllU3Ec__AnonStorey18_t3483285153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<UpdateAll>c__AnonStorey18::<>m__B()
extern "C"  void U3CUpdateAllU3Ec__AnonStorey18_U3CU3Em__B_m4125482677 (U3CUpdateAllU3Ec__AnonStorey18_t3483285153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
