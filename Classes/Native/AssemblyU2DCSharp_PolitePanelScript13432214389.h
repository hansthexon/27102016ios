﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// DataService
struct DataService_t2602786891;
// Code
struct Code_t686167511;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PolitePanelScript1
struct  PolitePanelScript1_t3432214389  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text PolitePanelScript1::ScoreText
	Text_t356221433 * ___ScoreText_2;
	// UnityEngine.GameObject PolitePanelScript1::mainScreen
	GameObject_t1756533147 * ___mainScreen_3;
	// System.Boolean PolitePanelScript1::replay
	bool ___replay_4;
	// System.String PolitePanelScript1::vivdeoname
	String_t* ___vivdeoname_5;
	// UnityEngine.GameObject PolitePanelScript1::ReplayPanel
	GameObject_t1756533147 * ___ReplayPanel_6;
	// UnityEngine.GameObject PolitePanelScript1::PopupsPanel
	GameObject_t1756533147 * ___PopupsPanel_7;
	// UnityEngine.GameObject PolitePanelScript1::PolitePanel
	GameObject_t1756533147 * ___PolitePanel_8;
	// UnityEngine.GameObject PolitePanelScript1::PoliteSky
	GameObject_t1756533147 * ___PoliteSky_9;
	// UnityEngine.GameObject PolitePanelScript1::ScorePanel
	GameObject_t1756533147 * ___ScorePanel_10;
	// UnityEngine.GameObject[] PolitePanelScript1::PanelstBeClosed
	GameObjectU5BU5D_t3057952154* ___PanelstBeClosed_11;
	// UnityEngine.GameObject PolitePanelScript1::videoPrefab
	GameObject_t1756533147 * ___videoPrefab_12;
	// UnityEngine.GameObject PolitePanelScript1::videoManager
	GameObject_t1756533147 * ___videoManager_13;
	// MediaPlayerCtrl PolitePanelScript1::player
	MediaPlayerCtrl_t1284484152 * ___player_14;
	// DataService PolitePanelScript1::ds
	DataService_t2602786891 * ___ds_15;
	// Code PolitePanelScript1::codeobject
	Code_t686167511 * ___codeobject_16;
	// UnityEngine.GameObject[] PolitePanelScript1::PoliteButtons
	GameObjectU5BU5D_t3057952154* ___PoliteButtons_17;
	// UnityEngine.GameObject[] PolitePanelScript1::fuckthemall
	GameObjectU5BU5D_t3057952154* ___fuckthemall_18;

public:
	inline static int32_t get_offset_of_ScoreText_2() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___ScoreText_2)); }
	inline Text_t356221433 * get_ScoreText_2() const { return ___ScoreText_2; }
	inline Text_t356221433 ** get_address_of_ScoreText_2() { return &___ScoreText_2; }
	inline void set_ScoreText_2(Text_t356221433 * value)
	{
		___ScoreText_2 = value;
		Il2CppCodeGenWriteBarrier(&___ScoreText_2, value);
	}

	inline static int32_t get_offset_of_mainScreen_3() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___mainScreen_3)); }
	inline GameObject_t1756533147 * get_mainScreen_3() const { return ___mainScreen_3; }
	inline GameObject_t1756533147 ** get_address_of_mainScreen_3() { return &___mainScreen_3; }
	inline void set_mainScreen_3(GameObject_t1756533147 * value)
	{
		___mainScreen_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainScreen_3, value);
	}

	inline static int32_t get_offset_of_replay_4() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___replay_4)); }
	inline bool get_replay_4() const { return ___replay_4; }
	inline bool* get_address_of_replay_4() { return &___replay_4; }
	inline void set_replay_4(bool value)
	{
		___replay_4 = value;
	}

	inline static int32_t get_offset_of_vivdeoname_5() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___vivdeoname_5)); }
	inline String_t* get_vivdeoname_5() const { return ___vivdeoname_5; }
	inline String_t** get_address_of_vivdeoname_5() { return &___vivdeoname_5; }
	inline void set_vivdeoname_5(String_t* value)
	{
		___vivdeoname_5 = value;
		Il2CppCodeGenWriteBarrier(&___vivdeoname_5, value);
	}

	inline static int32_t get_offset_of_ReplayPanel_6() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___ReplayPanel_6)); }
	inline GameObject_t1756533147 * get_ReplayPanel_6() const { return ___ReplayPanel_6; }
	inline GameObject_t1756533147 ** get_address_of_ReplayPanel_6() { return &___ReplayPanel_6; }
	inline void set_ReplayPanel_6(GameObject_t1756533147 * value)
	{
		___ReplayPanel_6 = value;
		Il2CppCodeGenWriteBarrier(&___ReplayPanel_6, value);
	}

	inline static int32_t get_offset_of_PopupsPanel_7() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___PopupsPanel_7)); }
	inline GameObject_t1756533147 * get_PopupsPanel_7() const { return ___PopupsPanel_7; }
	inline GameObject_t1756533147 ** get_address_of_PopupsPanel_7() { return &___PopupsPanel_7; }
	inline void set_PopupsPanel_7(GameObject_t1756533147 * value)
	{
		___PopupsPanel_7 = value;
		Il2CppCodeGenWriteBarrier(&___PopupsPanel_7, value);
	}

	inline static int32_t get_offset_of_PolitePanel_8() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___PolitePanel_8)); }
	inline GameObject_t1756533147 * get_PolitePanel_8() const { return ___PolitePanel_8; }
	inline GameObject_t1756533147 ** get_address_of_PolitePanel_8() { return &___PolitePanel_8; }
	inline void set_PolitePanel_8(GameObject_t1756533147 * value)
	{
		___PolitePanel_8 = value;
		Il2CppCodeGenWriteBarrier(&___PolitePanel_8, value);
	}

	inline static int32_t get_offset_of_PoliteSky_9() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___PoliteSky_9)); }
	inline GameObject_t1756533147 * get_PoliteSky_9() const { return ___PoliteSky_9; }
	inline GameObject_t1756533147 ** get_address_of_PoliteSky_9() { return &___PoliteSky_9; }
	inline void set_PoliteSky_9(GameObject_t1756533147 * value)
	{
		___PoliteSky_9 = value;
		Il2CppCodeGenWriteBarrier(&___PoliteSky_9, value);
	}

	inline static int32_t get_offset_of_ScorePanel_10() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___ScorePanel_10)); }
	inline GameObject_t1756533147 * get_ScorePanel_10() const { return ___ScorePanel_10; }
	inline GameObject_t1756533147 ** get_address_of_ScorePanel_10() { return &___ScorePanel_10; }
	inline void set_ScorePanel_10(GameObject_t1756533147 * value)
	{
		___ScorePanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___ScorePanel_10, value);
	}

	inline static int32_t get_offset_of_PanelstBeClosed_11() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___PanelstBeClosed_11)); }
	inline GameObjectU5BU5D_t3057952154* get_PanelstBeClosed_11() const { return ___PanelstBeClosed_11; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PanelstBeClosed_11() { return &___PanelstBeClosed_11; }
	inline void set_PanelstBeClosed_11(GameObjectU5BU5D_t3057952154* value)
	{
		___PanelstBeClosed_11 = value;
		Il2CppCodeGenWriteBarrier(&___PanelstBeClosed_11, value);
	}

	inline static int32_t get_offset_of_videoPrefab_12() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___videoPrefab_12)); }
	inline GameObject_t1756533147 * get_videoPrefab_12() const { return ___videoPrefab_12; }
	inline GameObject_t1756533147 ** get_address_of_videoPrefab_12() { return &___videoPrefab_12; }
	inline void set_videoPrefab_12(GameObject_t1756533147 * value)
	{
		___videoPrefab_12 = value;
		Il2CppCodeGenWriteBarrier(&___videoPrefab_12, value);
	}

	inline static int32_t get_offset_of_videoManager_13() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___videoManager_13)); }
	inline GameObject_t1756533147 * get_videoManager_13() const { return ___videoManager_13; }
	inline GameObject_t1756533147 ** get_address_of_videoManager_13() { return &___videoManager_13; }
	inline void set_videoManager_13(GameObject_t1756533147 * value)
	{
		___videoManager_13 = value;
		Il2CppCodeGenWriteBarrier(&___videoManager_13, value);
	}

	inline static int32_t get_offset_of_player_14() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___player_14)); }
	inline MediaPlayerCtrl_t1284484152 * get_player_14() const { return ___player_14; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_player_14() { return &___player_14; }
	inline void set_player_14(MediaPlayerCtrl_t1284484152 * value)
	{
		___player_14 = value;
		Il2CppCodeGenWriteBarrier(&___player_14, value);
	}

	inline static int32_t get_offset_of_ds_15() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___ds_15)); }
	inline DataService_t2602786891 * get_ds_15() const { return ___ds_15; }
	inline DataService_t2602786891 ** get_address_of_ds_15() { return &___ds_15; }
	inline void set_ds_15(DataService_t2602786891 * value)
	{
		___ds_15 = value;
		Il2CppCodeGenWriteBarrier(&___ds_15, value);
	}

	inline static int32_t get_offset_of_codeobject_16() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___codeobject_16)); }
	inline Code_t686167511 * get_codeobject_16() const { return ___codeobject_16; }
	inline Code_t686167511 ** get_address_of_codeobject_16() { return &___codeobject_16; }
	inline void set_codeobject_16(Code_t686167511 * value)
	{
		___codeobject_16 = value;
		Il2CppCodeGenWriteBarrier(&___codeobject_16, value);
	}

	inline static int32_t get_offset_of_PoliteButtons_17() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___PoliteButtons_17)); }
	inline GameObjectU5BU5D_t3057952154* get_PoliteButtons_17() const { return ___PoliteButtons_17; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_PoliteButtons_17() { return &___PoliteButtons_17; }
	inline void set_PoliteButtons_17(GameObjectU5BU5D_t3057952154* value)
	{
		___PoliteButtons_17 = value;
		Il2CppCodeGenWriteBarrier(&___PoliteButtons_17, value);
	}

	inline static int32_t get_offset_of_fuckthemall_18() { return static_cast<int32_t>(offsetof(PolitePanelScript1_t3432214389, ___fuckthemall_18)); }
	inline GameObjectU5BU5D_t3057952154* get_fuckthemall_18() const { return ___fuckthemall_18; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_fuckthemall_18() { return &___fuckthemall_18; }
	inline void set_fuckthemall_18(GameObjectU5BU5D_t3057952154* value)
	{
		___fuckthemall_18 = value;
		Il2CppCodeGenWriteBarrier(&___fuckthemall_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
