﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JsonExample/ExampleSerializedClass
struct ExampleSerializedClass_t1787974205;

#include "codegen/il2cpp-codegen.h"

// System.Void JsonExample/ExampleSerializedClass::.ctor()
extern "C"  void ExampleSerializedClass__ctor_m614884280 (ExampleSerializedClass_t1787974205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
