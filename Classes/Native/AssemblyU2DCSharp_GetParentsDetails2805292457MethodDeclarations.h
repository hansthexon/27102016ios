﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GetParentsDetails
struct GetParentsDetails_t2805292457;

#include "codegen/il2cpp-codegen.h"

// System.Void GetParentsDetails::.ctor()
extern "C"  void GetParentsDetails__ctor_m2067769960 (GetParentsDetails_t2805292457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GetParentsDetails::OnEnable()
extern "C"  void GetParentsDetails_OnEnable_m1969781488 (GetParentsDetails_t2805292457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
