﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey1C_2__ctor_m3995719360_gshared (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 * __this, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey1C_2__ctor_m3995719360(__this, method) ((  void (*) (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey1C_2__ctor_m3995719360_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>::<>m__33(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey1C_2_U3CU3Em__33_m2805572642_gshared (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 * __this, Il2CppObject * ___input0, const MethodInfo* method);
#define U3CRegisterImporterU3Ec__AnonStorey1C_2_U3CU3Em__33_m2805572642(__this, ___input0, method) ((  Il2CppObject * (*) (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 *, Il2CppObject *, const MethodInfo*))U3CRegisterImporterU3Ec__AnonStorey1C_2_U3CU3Em__33_m2805572642_gshared)(__this, ___input0, method)
