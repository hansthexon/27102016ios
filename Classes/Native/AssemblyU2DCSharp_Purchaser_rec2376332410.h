﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Purchaser/rec
struct  rec_t2376332410  : public Il2CppObject
{
public:
	// System.String Purchaser/rec::Store
	String_t* ___Store_0;
	// System.String Purchaser/rec::TransactionID
	String_t* ___TransactionID_1;
	// System.String Purchaser/rec::Payload
	String_t* ___Payload_2;

public:
	inline static int32_t get_offset_of_Store_0() { return static_cast<int32_t>(offsetof(rec_t2376332410, ___Store_0)); }
	inline String_t* get_Store_0() const { return ___Store_0; }
	inline String_t** get_address_of_Store_0() { return &___Store_0; }
	inline void set_Store_0(String_t* value)
	{
		___Store_0 = value;
		Il2CppCodeGenWriteBarrier(&___Store_0, value);
	}

	inline static int32_t get_offset_of_TransactionID_1() { return static_cast<int32_t>(offsetof(rec_t2376332410, ___TransactionID_1)); }
	inline String_t* get_TransactionID_1() const { return ___TransactionID_1; }
	inline String_t** get_address_of_TransactionID_1() { return &___TransactionID_1; }
	inline void set_TransactionID_1(String_t* value)
	{
		___TransactionID_1 = value;
		Il2CppCodeGenWriteBarrier(&___TransactionID_1, value);
	}

	inline static int32_t get_offset_of_Payload_2() { return static_cast<int32_t>(offsetof(rec_t2376332410, ___Payload_2)); }
	inline String_t* get_Payload_2() const { return ___Payload_2; }
	inline String_t** get_address_of_Payload_2() { return &___Payload_2; }
	inline void set_Payload_2(String_t* value)
	{
		___Payload_2 = value;
		Il2CppCodeGenWriteBarrier(&___Payload_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
