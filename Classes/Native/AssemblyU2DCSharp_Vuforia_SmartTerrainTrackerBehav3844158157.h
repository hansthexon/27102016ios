﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke4026264541.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.SmartTerrainTrackerBehaviour
struct  SmartTerrainTrackerBehaviour_t3844158157  : public SmartTerrainTrackerAbstractBehaviour_t4026264541
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
