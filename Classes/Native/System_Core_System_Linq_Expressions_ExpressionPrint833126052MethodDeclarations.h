﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ExpressionPrinter
struct ExpressionPrinter_t833126052;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Linq.Expressions.ElementInit
struct ElementInit_t3898206436;
// System.Linq.Expressions.MemberBinding
struct MemberBinding_t3487798541;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Linq.Expressions.UnaryExpression
struct UnaryExpression_t4253534555;
// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// System.Linq.Expressions.TypeBinaryExpression
struct TypeBinaryExpression_t4211182725;
// System.Linq.Expressions.ConstantExpression
struct ConstantExpression_t305952364;
// System.Linq.Expressions.ConditionalExpression
struct ConditionalExpression_t4183435840;
// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t3015504955;
// System.Linq.Expressions.MemberExpression
struct MemberExpression_t1790982958;
// System.Linq.Expressions.MethodCallExpression
struct MethodCallExpression_t3367820543;
// System.Linq.Expressions.MemberAssignment
struct MemberAssignment_t3987033531;
// System.Linq.Expressions.MemberMemberBinding
struct MemberMemberBinding_t950649153;
// System.Linq.Expressions.MemberListBinding
struct MemberListBinding_t1321006879;
// System.Linq.Expressions.LambdaExpression
struct LambdaExpression_t2811402413;
// System.Linq.Expressions.NewExpression
struct NewExpression_t1045017810;
// System.Linq.Expressions.MemberInitExpression
struct MemberInitExpression_t137172540;
// System.Linq.Expressions.ListInitExpression
struct ListInitExpression_t1376237998;
// System.Linq.Expressions.NewArrayExpression
struct NewArrayExpression_t2420949259;
// System.Linq.Expressions.InvocationExpression
struct InvocationExpression_t267914806;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "System_Core_System_Linq_Expressions_ElementInit3898206436.h"
#include "System_Core_System_Linq_Expressions_MemberBinding3487798541.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Linq_Expressions_UnaryExpressio4253534555.h"
#include "System_Core_System_Linq_Expressions_BinaryExpressi2159924157.h"
#include "System_Core_System_Linq_Expressions_TypeBinaryExpr4211182725.h"
#include "System_Core_System_Linq_Expressions_ConstantExpress305952364.h"
#include "System_Core_System_Linq_Expressions_ConditionalExp4183435840.h"
#include "System_Core_System_Linq_Expressions_ParameterExpre3015504955.h"
#include "System_Core_System_Linq_Expressions_MemberExpressi1790982958.h"
#include "System_Core_System_Linq_Expressions_MethodCallExpr3367820543.h"
#include "System_Core_System_Linq_Expressions_MemberAssignme3987033531.h"
#include "System_Core_System_Linq_Expressions_MemberMemberBin950649153.h"
#include "System_Core_System_Linq_Expressions_MemberListBind1321006879.h"
#include "System_Core_System_Linq_Expressions_LambdaExpressi2811402413.h"
#include "System_Core_System_Linq_Expressions_NewExpression1045017810.h"
#include "System_Core_System_Linq_Expressions_MemberInitExpre137172540.h"
#include "System_Core_System_Linq_Expressions_ListInitExpres1376237998.h"
#include "System_Core_System_Linq_Expressions_NewArrayExpres2420949259.h"
#include "System_Core_System_Linq_Expressions_InvocationExpre267914806.h"

// System.Void System.Linq.Expressions.ExpressionPrinter::.ctor(System.Text.StringBuilder)
extern "C"  void ExpressionPrinter__ctor_m3084567955 (ExpressionPrinter_t833126052 * __this, StringBuilder_t1221177846 * ___builder0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::.ctor()
extern "C"  void ExpressionPrinter__ctor_m3234687099 (ExpressionPrinter_t833126052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ExpressionPrinter::ToString(System.Linq.Expressions.Expression)
extern "C"  String_t* ExpressionPrinter_ToString_m1236936434 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ExpressionPrinter::ToString(System.Linq.Expressions.ElementInit)
extern "C"  String_t* ExpressionPrinter_ToString_m3093233840 (Il2CppObject * __this /* static, unused */, ElementInit_t3898206436 * ___init0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ExpressionPrinter::ToString(System.Linq.Expressions.MemberBinding)
extern "C"  String_t* ExpressionPrinter_ToString_m3296789017 (Il2CppObject * __this /* static, unused */, MemberBinding_t3487798541 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::Print(System.String)
extern "C"  void ExpressionPrinter_Print_m1732571154 (ExpressionPrinter_t833126052 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::Print(System.Object)
extern "C"  void ExpressionPrinter_Print_m3158729820 (ExpressionPrinter_t833126052 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::Print(System.String,System.Object[])
extern "C"  void ExpressionPrinter_Print_m3327878160 (ExpressionPrinter_t833126052 * __this, String_t* ___str0, ObjectU5BU5D_t3614634134* ___objs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitElementInitializer(System.Linq.Expressions.ElementInit)
extern "C"  void ExpressionPrinter_VisitElementInitializer_m1170813292 (ExpressionPrinter_t833126052 * __this, ElementInit_t3898206436 * ___initializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitUnary(System.Linq.Expressions.UnaryExpression)
extern "C"  void ExpressionPrinter_VisitUnary_m1020661518 (ExpressionPrinter_t833126052 * __this, UnaryExpression_t4253534555 * ___unary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ExpressionPrinter::OperatorToString(System.Linq.Expressions.BinaryExpression)
extern "C"  String_t* ExpressionPrinter_OperatorToString_m1459833133 (Il2CppObject * __this /* static, unused */, BinaryExpression_t2159924157 * ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.ExpressionPrinter::IsBoolean(System.Linq.Expressions.Expression)
extern "C"  bool ExpressionPrinter_IsBoolean_m1275310743 (Il2CppObject * __this /* static, unused */, Expression_t114864668 * ___expression0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::PrintArrayIndex(System.Linq.Expressions.BinaryExpression)
extern "C"  void ExpressionPrinter_PrintArrayIndex_m1239268422 (ExpressionPrinter_t833126052 * __this, BinaryExpression_t2159924157 * ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitBinary(System.Linq.Expressions.BinaryExpression)
extern "C"  void ExpressionPrinter_VisitBinary_m1781542252 (ExpressionPrinter_t833126052 * __this, BinaryExpression_t2159924157 * ___binary0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitTypeIs(System.Linq.Expressions.TypeBinaryExpression)
extern "C"  void ExpressionPrinter_VisitTypeIs_m1027377957 (ExpressionPrinter_t833126052 * __this, TypeBinaryExpression_t4211182725 * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitConstant(System.Linq.Expressions.ConstantExpression)
extern "C"  void ExpressionPrinter_VisitConstant_m3486358418 (ExpressionPrinter_t833126052 * __this, ConstantExpression_t305952364 * ___constant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Linq.Expressions.ExpressionPrinter::HasStringRepresentation(System.Object)
extern "C"  bool ExpressionPrinter_HasStringRepresentation_m2754387575 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitConditional(System.Linq.Expressions.ConditionalExpression)
extern "C"  void ExpressionPrinter_VisitConditional_m2463825518 (ExpressionPrinter_t833126052 * __this, ConditionalExpression_t4183435840 * ___conditional0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitParameter(System.Linq.Expressions.ParameterExpression)
extern "C"  void ExpressionPrinter_VisitParameter_m668120462 (ExpressionPrinter_t833126052 * __this, ParameterExpression_t3015504955 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMemberAccess(System.Linq.Expressions.MemberExpression)
extern "C"  void ExpressionPrinter_VisitMemberAccess_m2198524212 (ExpressionPrinter_t833126052 * __this, MemberExpression_t1790982958 * ___access0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMethodCall(System.Linq.Expressions.MethodCallExpression)
extern "C"  void ExpressionPrinter_VisitMethodCall_m2362395896 (ExpressionPrinter_t833126052 * __this, MethodCallExpression_t3367820543 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMemberAssignment(System.Linq.Expressions.MemberAssignment)
extern "C"  void ExpressionPrinter_VisitMemberAssignment_m373286334 (ExpressionPrinter_t833126052 * __this, MemberAssignment_t3987033531 * ___assignment0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMemberMemberBinding(System.Linq.Expressions.MemberMemberBinding)
extern "C"  void ExpressionPrinter_VisitMemberMemberBinding_m1298889644 (ExpressionPrinter_t833126052 * __this, MemberMemberBinding_t950649153 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMemberListBinding(System.Linq.Expressions.MemberListBinding)
extern "C"  void ExpressionPrinter_VisitMemberListBinding_m4086099020 (ExpressionPrinter_t833126052 * __this, MemberListBinding_t1321006879 * ___binding0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitLambda(System.Linq.Expressions.LambdaExpression)
extern "C"  void ExpressionPrinter_VisitLambda_m4282918956 (ExpressionPrinter_t833126052 * __this, LambdaExpression_t2811402413 * ___lambda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitNew(System.Linq.Expressions.NewExpression)
extern "C"  void ExpressionPrinter_VisitNew_m3214394542 (ExpressionPrinter_t833126052 * __this, NewExpression_t1045017810 * ___nex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitMemberInit(System.Linq.Expressions.MemberInitExpression)
extern "C"  void ExpressionPrinter_VisitMemberInit_m643814146 (ExpressionPrinter_t833126052 * __this, MemberInitExpression_t137172540 * ___init0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitListInit(System.Linq.Expressions.ListInitExpression)
extern "C"  void ExpressionPrinter_VisitListInit_m3570073902 (ExpressionPrinter_t833126052 * __this, ListInitExpression_t1376237998 * ___init0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitNewArray(System.Linq.Expressions.NewArrayExpression)
extern "C"  void ExpressionPrinter_VisitNewArray_m3249985720 (ExpressionPrinter_t833126052 * __this, NewArrayExpression_t2420949259 * ___newArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Linq.Expressions.ExpressionPrinter::VisitInvocation(System.Linq.Expressions.InvocationExpression)
extern "C"  void ExpressionPrinter_VisitInvocation_m2418794222 (ExpressionPrinter_t833126052 * __this, InvocationExpression_t267914806 * ___invocation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
