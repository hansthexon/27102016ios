﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey13
struct  U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_t2464633316  : public Il2CppObject
{
public:
	// System.Object SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey13::obj
	Il2CppObject * ___obj_0;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_t2464633316, ___obj_0)); }
	inline Il2CppObject * get_obj_0() const { return ___obj_0; }
	inline Il2CppObject ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Il2CppObject * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier(&___obj_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
