﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0
struct U3CDeObfuscateU3Ec__AnonStorey0_t1106656056;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0::.ctor()
extern "C"  void U3CDeObfuscateU3Ec__AnonStorey0__ctor_m1242923309 (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine.Purchasing.Security.Obfuscator/<DeObfuscate>c__AnonStorey0::<>m__0(System.Byte)
extern "C"  uint8_t U3CDeObfuscateU3Ec__AnonStorey0_U3CU3Em__0_m2910553385 (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056 * __this, uint8_t ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
