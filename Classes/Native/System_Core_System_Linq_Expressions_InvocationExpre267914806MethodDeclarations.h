﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.InvocationExpression
struct InvocationExpression_t267914806;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_t300650360;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.Expression System.Linq.Expressions.InvocationExpression::get_Expression()
extern "C"  Expression_t114864668 * InvocationExpression_get_Expression_m1355357315 (InvocationExpression_t267914806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression> System.Linq.Expressions.InvocationExpression::get_Arguments()
extern "C"  ReadOnlyCollection_1_t300650360 * InvocationExpression_get_Arguments_m2892230354 (InvocationExpression_t267914806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
