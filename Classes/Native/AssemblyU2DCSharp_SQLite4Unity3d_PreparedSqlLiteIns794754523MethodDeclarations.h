﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.PreparedSqlLiteInsertCommand
struct PreparedSqlLiteInsertCommand_t794754523;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::.ctor(SQLite4Unity3d.SQLiteConnection)
extern "C"  void PreparedSqlLiteInsertCommand__ctor_m2060767744 (PreparedSqlLiteInsertCommand_t794754523 * __this, SQLiteConnection_t3529499386 * ___conn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::.cctor()
extern "C"  void PreparedSqlLiteInsertCommand__cctor_m4164580071 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.PreparedSqlLiteInsertCommand::get_Initialized()
extern "C"  bool PreparedSqlLiteInsertCommand_get_Initialized_m256612877 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::set_Initialized(System.Boolean)
extern "C"  void PreparedSqlLiteInsertCommand_set_Initialized_m818305318 (PreparedSqlLiteInsertCommand_t794754523 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.PreparedSqlLiteInsertCommand::get_Connection()
extern "C"  SQLiteConnection_t3529499386 * PreparedSqlLiteInsertCommand_get_Connection_m3194615758 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::set_Connection(SQLite4Unity3d.SQLiteConnection)
extern "C"  void PreparedSqlLiteInsertCommand_set_Connection_m2251827419 (PreparedSqlLiteInsertCommand_t794754523 * __this, SQLiteConnection_t3529499386 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.PreparedSqlLiteInsertCommand::get_CommandText()
extern "C"  String_t* PreparedSqlLiteInsertCommand_get_CommandText_m1770553554 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::set_CommandText(System.String)
extern "C"  void PreparedSqlLiteInsertCommand_set_CommandText_m605234721 (PreparedSqlLiteInsertCommand_t794754523 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.PreparedSqlLiteInsertCommand::get_Statement()
extern "C"  IntPtr_t PreparedSqlLiteInsertCommand_get_Statement_m355624129 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::set_Statement(System.IntPtr)
extern "C"  void PreparedSqlLiteInsertCommand_set_Statement_m4105959750 (PreparedSqlLiteInsertCommand_t794754523 * __this, IntPtr_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.PreparedSqlLiteInsertCommand::ExecuteNonQuery(System.Object[])
extern "C"  int32_t PreparedSqlLiteInsertCommand_ExecuteNonQuery_m2329397630 (PreparedSqlLiteInsertCommand_t794754523 * __this, ObjectU5BU5D_t3614634134* ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.PreparedSqlLiteInsertCommand::Prepare()
extern "C"  IntPtr_t PreparedSqlLiteInsertCommand_Prepare_m3429192836 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::Dispose()
extern "C"  void PreparedSqlLiteInsertCommand_Dispose_m118814143 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::Dispose(System.Boolean)
extern "C"  void PreparedSqlLiteInsertCommand_Dispose_m828930790 (PreparedSqlLiteInsertCommand_t794754523 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.PreparedSqlLiteInsertCommand::Finalize()
extern "C"  void PreparedSqlLiteInsertCommand_Finalize_m1919026682 (PreparedSqlLiteInsertCommand_t794754523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
