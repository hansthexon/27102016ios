﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_3672778802.h"
#include "winrt_U3CModuleU3E3783534214.h"
#include "winrt_UnityEngine_Purchasing_Default_Factory1430638288.h"
#include "winrt_UnityEngine_Purchasing_Default_WinProductDes1075111405.h"
#include "Security_U3CModuleU3E3783534214.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Parser914015216.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Util2059476207.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Oid113668572.h"
#include "Security_LipingShare_LCLib_Asn1Processor_RelativeOi880150712.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidX51630759105.h"
#include "Security_UnityEngine_Purchasing_Security_PKCS71974940522.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804.h"
#include "Security_UnityEngine_Purchasing_Security_IAPSecuri3038093501.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidSig488933488.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidPK4123278833.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidTi3933748955.h"
#include "Security_UnityEngine_Purchasing_Security_Unsupport2780725255.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRS1674954323.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla4061171767.h"
#include "Security_UnityEngine_Purchasing_Security_AppleVali3837389912.h"
#include "Security_UnityEngine_Purchasing_Security_AppleRece3991411794.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato2878230988.h"
#include "Security_UnityEngine_Purchasing_Security_Obfuscato1106656056.h"
#include "Security_UnityEngine_Purchasing_Security_StoreNotSu958190973.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidBu3011288315.h"
#include "Security_UnityEngine_Purchasing_Security_InvalidRe1032464292.h"
#include "Security_UnityEngine_Purchasing_Security_MissingSt3547862692.h"
#include "Security_UnityEngine_Purchasing_Security_CrossPlatf305796431.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePur2057805495.h"
#include "Security_UnityEngine_Purchasing_Security_GooglePla2643016893.h"
#include "Security_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Security_U3CPrivateImplementationDetailsU3E_U24Arr1568637719.h"
#include "Stores_U3CModuleU3E3783534214.h"
#include "Stores_UnityEngine_Purchasing_AndroidJavaStore2772549594.h"
#include "Stores_UnityEngine_Purchasing_SerializationExtensi3531056818.h"
#include "Stores_UnityEngine_Purchasing_JSONSerializer501879906.h"
#include "Stores_UnityEngine_Purchasing_JavaBridge44746847.h"
#include "Stores_UnityEngine_Purchasing_NativeJSONStore3685388740.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1803[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1805[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1807[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1809[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1810[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1816[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[14] = 
{
	0,
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_6(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_7(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_8(),
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1818[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1822[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[3] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1834[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1837[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1843[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[4] = 
{
	0,
	Shadow_t4269599528::get_offset_of_m_EffectColor_4(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_5(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U24U24fieldU2D0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (U24ArrayTypeU2412_t3672778807)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t3672778807_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (Factory_t1430638288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (WinProductDescription_t1075111405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[9] = 
{
	WinProductDescription_t1075111405::get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0(),
	WinProductDescription_t1075111405::get_offset_of_U3CpriceU3Ek__BackingField_1(),
	WinProductDescription_t1075111405::get_offset_of_U3CtitleU3Ek__BackingField_2(),
	WinProductDescription_t1075111405::get_offset_of_U3CdescriptionU3Ek__BackingField_3(),
	WinProductDescription_t1075111405::get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4(),
	WinProductDescription_t1075111405::get_offset_of_U3CpriceDecimalU3Ek__BackingField_5(),
	WinProductDescription_t1075111405::get_offset_of_U3CreceiptU3Ek__BackingField_6(),
	WinProductDescription_t1075111405::get_offset_of_U3CtransactionIDU3Ek__BackingField_7(),
	WinProductDescription_t1075111405::get_offset_of_U3CconsumableU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (Asn1Node_t1770761751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[18] = 
{
	Asn1Node_t1770761751::get_offset_of_tag_0(),
	Asn1Node_t1770761751::get_offset_of_dataOffset_1(),
	Asn1Node_t1770761751::get_offset_of_dataLength_2(),
	Asn1Node_t1770761751::get_offset_of_lengthFieldBytes_3(),
	Asn1Node_t1770761751::get_offset_of_data_4(),
	Asn1Node_t1770761751::get_offset_of_childNodeList_5(),
	Asn1Node_t1770761751::get_offset_of_unusedBits_6(),
	Asn1Node_t1770761751::get_offset_of_deepness_7(),
	Asn1Node_t1770761751::get_offset_of_path_8(),
	0,
	Asn1Node_t1770761751::get_offset_of_parentNode_10(),
	Asn1Node_t1770761751::get_offset_of_requireRecalculatePar_11(),
	Asn1Node_t1770761751::get_offset_of_isIndefiniteLength_12(),
	Asn1Node_t1770761751::get_offset_of_parseEncapsulatedData_13(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Asn1Parser_t914015216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[2] = 
{
	Asn1Parser_t914015216::get_offset_of_rawData_0(),
	Asn1Parser_t914015216::get_offset_of_rootNode_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (Asn1Util_t2059476207), -1, sizeof(Asn1Util_t2059476207_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[1] = 
{
	Asn1Util_t2059476207_StaticFields::get_offset_of_hexDigits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (Oid_t113668572), -1, sizeof(Oid_t113668572_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1863[1] = 
{
	Oid_t113668572_StaticFields::get_offset_of_oidDictionary_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (RelativeOid_t880150712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (DistinguishedName_t1881593989), -1, sizeof(DistinguishedName_t1881593989_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1865[8] = 
{
	DistinguishedName_t1881593989::get_offset_of_U3CCountryU3Ek__BackingField_0(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationU3Ek__BackingField_1(),
	DistinguishedName_t1881593989::get_offset_of_U3COrganizationalUnitU3Ek__BackingField_2(),
	DistinguishedName_t1881593989::get_offset_of_U3CDnqU3Ek__BackingField_3(),
	DistinguishedName_t1881593989::get_offset_of_U3CStateU3Ek__BackingField_4(),
	DistinguishedName_t1881593989::get_offset_of_U3CCommonNameU3Ek__BackingField_5(),
	DistinguishedName_t1881593989::get_offset_of_U3CSerialNumberU3Ek__BackingField_6(),
	DistinguishedName_t1881593989_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (X509Cert_t481809278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[10] = 
{
	X509Cert_t481809278::get_offset_of_U3CSerialNumberU3Ek__BackingField_0(),
	X509Cert_t481809278::get_offset_of_U3CValidAfterU3Ek__BackingField_1(),
	X509Cert_t481809278::get_offset_of_U3CValidBeforeU3Ek__BackingField_2(),
	X509Cert_t481809278::get_offset_of_U3CPubKeyU3Ek__BackingField_3(),
	X509Cert_t481809278::get_offset_of_U3CSelfSignedU3Ek__BackingField_4(),
	X509Cert_t481809278::get_offset_of_U3CSubjectU3Ek__BackingField_5(),
	X509Cert_t481809278::get_offset_of_U3CIssuerU3Ek__BackingField_6(),
	X509Cert_t481809278::get_offset_of_TbsCertificate_7(),
	X509Cert_t481809278::get_offset_of_U3CSignatureU3Ek__BackingField_8(),
	X509Cert_t481809278::get_offset_of_rawTBSCertificate_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (InvalidX509Data_t1630759105), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (PKCS7_t1974940522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[5] = 
{
	PKCS7_t1974940522::get_offset_of_root_0(),
	PKCS7_t1974940522::get_offset_of_U3CdataU3Ek__BackingField_1(),
	PKCS7_t1974940522::get_offset_of_U3CsinfosU3Ek__BackingField_2(),
	PKCS7_t1974940522::get_offset_of_U3CcertChainU3Ek__BackingField_3(),
	PKCS7_t1974940522::get_offset_of_validStructure_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (SignerInfo_t4122348804), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[3] = 
{
	SignerInfo_t4122348804::get_offset_of_U3CVersionU3Ek__BackingField_0(),
	SignerInfo_t4122348804::get_offset_of_U3CIssuerSerialNumberU3Ek__BackingField_1(),
	SignerInfo_t4122348804::get_offset_of_U3CEncryptedDigestU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (IAPSecurityException_t3038093501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (InvalidSignatureException_t488933488), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (InvalidPKCS7Data_t4123278833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (InvalidTimeFormat_t3933748955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (UnsupportedSignerInfoVersion_t2780725255), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (RSAKey_t446464277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	RSAKey_t446464277::get_offset_of_U3CrsaU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (InvalidRSAData_t1674954323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (GooglePlayValidator_t4061171767), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	GooglePlayValidator_t4061171767::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (AppleValidator_t3837389912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	AppleValidator_t3837389912::get_offset_of_cert_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (AppleReceipt_t3991411794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[7] = 
{
	AppleReceipt_t3991411794::get_offset_of_U3CbundleIDU3Ek__BackingField_0(),
	AppleReceipt_t3991411794::get_offset_of_U3CappVersionU3Ek__BackingField_1(),
	AppleReceipt_t3991411794::get_offset_of_U3CopaqueU3Ek__BackingField_2(),
	AppleReceipt_t3991411794::get_offset_of_U3ChashU3Ek__BackingField_3(),
	AppleReceipt_t3991411794::get_offset_of_U3CoriginalApplicationVersionU3Ek__BackingField_4(),
	AppleReceipt_t3991411794::get_offset_of_U3CreceiptCreationDateU3Ek__BackingField_5(),
	AppleReceipt_t3991411794::get_offset_of_inAppPurchaseReceipts_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (AppleInAppPurchaseReceipt_t3271698749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[8] = 
{
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CquantityU3Ek__BackingField_0(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CproductIDU3Ek__BackingField_1(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CtransactionIDU3Ek__BackingField_2(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalTransactionIdentifierU3Ek__BackingField_3(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CoriginalPurchaseDateU3Ek__BackingField_5(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CsubscriptionExpirationDateU3Ek__BackingField_6(),
	AppleInAppPurchaseReceipt_t3271698749::get_offset_of_U3CcancellationDateU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (Obfuscator_t2878230988), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (U3CDeObfuscateU3Ec__AnonStorey0_t1106656056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	U3CDeObfuscateU3Ec__AnonStorey0_t1106656056::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (StoreNotSupportedException_t958190973), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (InvalidBundleIdException_t3011288315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (InvalidReceiptDataException_t1032464292), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (MissingStoreSecretException_t3547862692), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (CrossPlatformValidator_t305796431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[4] = 
{
	CrossPlatformValidator_t305796431::get_offset_of_google_0(),
	CrossPlatformValidator_t305796431::get_offset_of_apple_1(),
	CrossPlatformValidator_t305796431::get_offset_of_googleBundleId_2(),
	CrossPlatformValidator_t305796431::get_offset_of_appleBundleId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (GooglePurchaseState_t2057805495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	GooglePurchaseState_t2057805495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (GooglePlayReceipt_t2643016893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[6] = 
{
	GooglePlayReceipt_t2643016893::get_offset_of_U3CproductIDU3Ek__BackingField_0(),
	GooglePlayReceipt_t2643016893::get_offset_of_U3CtransactionIDU3Ek__BackingField_1(),
	GooglePlayReceipt_t2643016893::get_offset_of_U3CpackageNameU3Ek__BackingField_2(),
	GooglePlayReceipt_t2643016893::get_offset_of_U3CpurchaseTokenU3Ek__BackingField_3(),
	GooglePlayReceipt_t2643016893::get_offset_of_U3CpurchaseDateU3Ek__BackingField_4(),
	GooglePlayReceipt_t2643016893::get_offset_of_U3CpurchaseStateU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1891[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D59F5BD34B6C013DEACC784F69C67E95150033A84_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (AndroidJavaStore_t2772549594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	AndroidJavaStore_t2772549594::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (SerializationExtensions_t3531056818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (JSONSerializer_t501879906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (JavaBridge_t44746847), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[1] = 
{
	JavaBridge_t44746847::get_offset_of_forwardTo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (NativeJSONStore_t3685388740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[2] = 
{
	NativeJSONStore_t3685388740::get_offset_of_unity_0(),
	NativeJSONStore_t3685388740::get_offset_of_store_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
