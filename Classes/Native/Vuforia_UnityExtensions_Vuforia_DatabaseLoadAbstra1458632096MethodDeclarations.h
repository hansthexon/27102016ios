﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.DatabaseLoadAbstractBehaviour
struct DatabaseLoadAbstractBehaviour_t1458632096;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Vuforia.DatabaseLoadAbstractBehaviour::LoadDatasets()
extern "C"  void DatabaseLoadAbstractBehaviour_LoadDatasets_m2642421143 (DatabaseLoadAbstractBehaviour_t1458632096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern "C"  void DatabaseLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m1034342592 (DatabaseLoadAbstractBehaviour_t1458632096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
extern "C"  void DatabaseLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2498310769 (DatabaseLoadAbstractBehaviour_t1458632096 * __this, String_t* ___searchDir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::Start()
extern "C"  void DatabaseLoadAbstractBehaviour_Start_m2120855892 (DatabaseLoadAbstractBehaviour_t1458632096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DatabaseLoadAbstractBehaviour::.ctor()
extern "C"  void DatabaseLoadAbstractBehaviour__ctor_m3414578076 (DatabaseLoadAbstractBehaviour_t1458632096 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
