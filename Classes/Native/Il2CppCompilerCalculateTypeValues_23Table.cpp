﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_U3CF2043699159.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_U3CF3342064577.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_Orm330478854.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand2935685145.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand_Bin1831577297.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_PreparedSqlLiteIns794754523.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery3770317269.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery_Or1038862794.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3150757461.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_Result1450270481.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ExtendedR1691412462.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ConfigOpt4001894229.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ColType2341528904.h"
#include "AssemblyU2DCSharp_SaveFileANdPlay1144534430.h"
#include "AssemblyU2DCSharp_SaveFileANdPlay_U3CDownloadU3Ec_4266829119.h"
#include "AssemblyU2DCSharp_ScrollSnapRect671131207.h"
#include "AssemblyU2DCSharp_TestScript905662927.h"
#include "AssemblyU2DCSharp_TestScript_U3CTestU3Ec__Iterator1507409797.h"
#include "AssemblyU2DCSharp_TogglePack1165964989.h"
#include "AssemblyU2DCSharp_JsonExample695984128.h"
#include "AssemblyU2DCSharp_JsonExample_ExampleSerializedCla1787974205.h"
#include "AssemblyU2DCSharp_LitJson_Extensions_Extensions3889416928.h"
#include "AssemblyU2DCSharp_LitJson_JsonType3145703806.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreWhen1352154806.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnore466986466.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreMember766617150.h"
#include "AssemblyU2DCSharp_LitJson_JsonInclude3368190666.h"
#include "AssemblyU2DCSharp_LitJson_JsonAlias2355411216.h"
#include "AssemblyU2DCSharp_LitJson_JsonData269267574.h"
#include "AssemblyU2DCSharp_LitJson_OrderedDictionaryEnumera3437478891.h"
#include "AssemblyU2DCSharp_LitJson_JsonException613047007.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper800426905.h"
#include "AssemblyU2DCSharp_LitJson_JsonMockWrapper4150814299.h"
#include "AssemblyU2DCSharp_LitJson_JsonToken2852816099.h"
#include "AssemblyU2DCSharp_LitJson_JsonReader1077921503.h"
#include "AssemblyU2DCSharp_LitJson_Condition1980525237.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext4137194742.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "AssemblyU2DCSharp_LitJson_FsmContext1296252303.h"
#include "AssemblyU2DCSharp_LitJson_Lexer186508296.h"
#include "AssemblyU2DCSharp_LitJson_Lexer_StateHandler387387051.h"
#include "AssemblyU2DCSharp_LitJson_ParserToken1554180950.h"
#include "AssemblyU2DCSharp_LitJson_UnityTypeBindings1763022095.h"
#include "AssemblyU2DCSharp_LitJson_UnityTypeBindings_U3CRegi484806451.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DatabaseLoadBehaviour450246482.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_DeviceTrackerBehaviour1884988499.h"
#include "AssemblyU2DCSharp_Vuforia_DigitalEyewearBehaviour495394589.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour1532923751.h"
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour1265232977.h"
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour2994129365.h"
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour3504654311.h"
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour3836044259.h"
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour966064926.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour4009935945.h"
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTarget2111803406.h"
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehav3844158157.h"
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour2405314212.h"
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour3400239837.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour3058161409.h"
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour584991835.h"
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildin4184040062.h"
#include "AssemblyU2DCSharp_VRIntegrationHelper556656694.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour3161817952.h"
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundManager3515346924.h"
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour2515041812.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviour359035403.h"
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour407765638.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventH1535150527.h"
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour3366478421.h"
#include "AssemblyU2DCSharp_Warmmup_anim335950655.h"
#include "AssemblyU2DCSharp_leftoright233454036.h"
#include "AssemblyU2DCSharp_play_anim_on_ui_button872675790.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc408878057.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc2977850894.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CFindColumnWithPropertyNameU3Ec__AnonStorey19_t2043699159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[1] = 
{
	U3CFindColumnWithPropertyNameU3Ec__AnonStorey19_t2043699159::get_offset_of_propertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (U3CFindColumnU3Ec__AnonStorey1A_t3342064577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[1] = 
{
	U3CFindColumnU3Ec__AnonStorey1A_t3342064577::get_offset_of_columnName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (Orm_t330478854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (SQLiteCommand_t2935685145), -1, sizeof(SQLiteCommand_t2935685145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[4] = 
{
	SQLiteCommand_t2935685145::get_offset_of__conn_0(),
	SQLiteCommand_t2935685145::get_offset_of__bindings_1(),
	SQLiteCommand_t2935685145_StaticFields::get_offset_of_NegativePointer_2(),
	SQLiteCommand_t2935685145::get_offset_of_U3CCommandTextU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (Binding_t1831577297), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2304[3] = 
{
	Binding_t1831577297::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Binding_t1831577297::get_offset_of_U3CValueU3Ek__BackingField_1(),
	Binding_t1831577297::get_offset_of_U3CIndexU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (PreparedSqlLiteInsertCommand_t794754523), -1, sizeof(PreparedSqlLiteInsertCommand_t794754523_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[5] = 
{
	PreparedSqlLiteInsertCommand_t794754523_StaticFields::get_offset_of_NullStatement_0(),
	PreparedSqlLiteInsertCommand_t794754523::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	PreparedSqlLiteInsertCommand_t794754523::get_offset_of_U3CConnectionU3Ek__BackingField_2(),
	PreparedSqlLiteInsertCommand_t794754523::get_offset_of_U3CCommandTextU3Ek__BackingField_3(),
	PreparedSqlLiteInsertCommand_t794754523::get_offset_of_U3CStatementU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (BaseTableQuery_t3770317269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (Ordering_t1038862794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[2] = 
{
	Ordering_t1038862794::get_offset_of_U3CColumnNameU3Ek__BackingField_0(),
	Ordering_t1038862794::get_offset_of_U3CAscendingU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (SQLite3_t150757461), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (Result_t1450270481)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2312[32] = 
{
	Result_t1450270481::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (ExtendedResult_t1691412462)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[46] = 
{
	ExtendedResult_t1691412462::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (ConfigOption_t4001894229)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[4] = 
{
	ConfigOption_t4001894229::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (ColType_t2341528904)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[6] = 
{
	ColType_t2341528904::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (SaveFileANdPlay_t1144534430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[2] = 
{
	SaveFileANdPlay_t1144534430::get_offset_of_scuk_2(),
	SaveFileANdPlay_t1144534430::get_offset_of_t_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (U3CDownloadU3Ec__Iterator10_t4266829119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[4] = 
{
	U3CDownloadU3Ec__Iterator10_t4266829119::get_offset_of_U3CwwwU3E__0_0(),
	U3CDownloadU3Ec__Iterator10_t4266829119::get_offset_of_U24PC_1(),
	U3CDownloadU3Ec__Iterator10_t4266829119::get_offset_of_U24current_2(),
	U3CDownloadU3Ec__Iterator10_t4266829119::get_offset_of_U3CU3Ef__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (ScrollSnapRect_t671131207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[26] = 
{
	ScrollSnapRect_t671131207::get_offset_of_Dog_2(),
	ScrollSnapRect_t671131207::get_offset_of_startingPage_3(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdTime_4(),
	ScrollSnapRect_t671131207::get_offset_of_fastSwipeThresholdDistance_5(),
	ScrollSnapRect_t671131207::get_offset_of_decelerationRate_6(),
	ScrollSnapRect_t671131207::get_offset_of_prevButton_7(),
	ScrollSnapRect_t671131207::get_offset_of_nextButton_8(),
	ScrollSnapRect_t671131207::get_offset_of_unselectedPage_9(),
	ScrollSnapRect_t671131207::get_offset_of_selectedPage_10(),
	ScrollSnapRect_t671131207::get_offset_of_pageSelectionIcons_11(),
	ScrollSnapRect_t671131207::get_offset_of__fastSwipeThresholdMaxLimit_12(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectComponent_13(),
	ScrollSnapRect_t671131207::get_offset_of__scrollRectRect_14(),
	ScrollSnapRect_t671131207::get_offset_of__container_15(),
	ScrollSnapRect_t671131207::get_offset_of__horizontal_16(),
	ScrollSnapRect_t671131207::get_offset_of__pageCount_17(),
	ScrollSnapRect_t671131207::get_offset_of__currentPage_18(),
	ScrollSnapRect_t671131207::get_offset_of__lerp_19(),
	ScrollSnapRect_t671131207::get_offset_of__lerpTo_20(),
	ScrollSnapRect_t671131207::get_offset_of__pagePositions_21(),
	ScrollSnapRect_t671131207::get_offset_of__dragging_22(),
	ScrollSnapRect_t671131207::get_offset_of__timeStamp_23(),
	ScrollSnapRect_t671131207::get_offset_of__startPosition_24(),
	ScrollSnapRect_t671131207::get_offset_of__showPageSelection_25(),
	ScrollSnapRect_t671131207::get_offset_of__previousPageSelectionIndex_26(),
	ScrollSnapRect_t671131207::get_offset_of__pageSelectionImages_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (TestScript_t905662927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[1] = 
{
	TestScript_t905662927::get_offset_of_ds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (U3CTestU3Ec__Iterator11_t1507409797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[4] = 
{
	U3CTestU3Ec__Iterator11_t1507409797::get_offset_of_U3CformU3E__0_0(),
	U3CTestU3Ec__Iterator11_t1507409797::get_offset_of_U3CwwwU3E__1_1(),
	U3CTestU3Ec__Iterator11_t1507409797::get_offset_of_U24PC_2(),
	U3CTestU3Ec__Iterator11_t1507409797::get_offset_of_U24current_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (TogglePack_t1165964989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[9] = 
{
	TogglePack_t1165964989::get_offset_of_purchaser_2(),
	TogglePack_t1165964989::get_offset_of_two_3(),
	TogglePack_t1165964989::get_offset_of_one_4(),
	TogglePack_t1165964989::get_offset_of_three_5(),
	TogglePack_t1165964989::get_offset_of_togg_6(),
	TogglePack_t1165964989::get_offset_of_img3_7(),
	TogglePack_t1165964989::get_offset_of_img2_8(),
	TogglePack_t1165964989::get_offset_of_img1_9(),
	TogglePack_t1165964989::get_offset_of_togcheck_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (JsonExample_t695984128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ExampleSerializedClass_t1787974205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[15] = 
{
	ExampleSerializedClass_t1787974205::get_offset_of_myInt_0(),
	ExampleSerializedClass_t1787974205::get_offset_of_myString_1(),
	ExampleSerializedClass_t1787974205::get_offset_of_myFloat_2(),
	ExampleSerializedClass_t1787974205::get_offset_of_myBool_3(),
	ExampleSerializedClass_t1787974205::get_offset_of_myVector2_4(),
	ExampleSerializedClass_t1787974205::get_offset_of_myVector3_5(),
	ExampleSerializedClass_t1787974205::get_offset_of_myVector4_6(),
	ExampleSerializedClass_t1787974205::get_offset_of_myQuaternion_7(),
	ExampleSerializedClass_t1787974205::get_offset_of_myColor_8(),
	ExampleSerializedClass_t1787974205::get_offset_of_myColor32_9(),
	ExampleSerializedClass_t1787974205::get_offset_of_myBounds_10(),
	ExampleSerializedClass_t1787974205::get_offset_of_myRect_11(),
	ExampleSerializedClass_t1787974205::get_offset_of_myRectOffset_12(),
	ExampleSerializedClass_t1787974205::get_offset_of_myIntArray_13(),
	ExampleSerializedClass_t1787974205::get_offset_of_myIgnoredTransform_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (Extensions_t3889416928), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (JsonType_t3145703806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2325[8] = 
{
	JsonType_t3145703806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (JsonIgnoreWhen_t1352154806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2327[4] = 
{
	JsonIgnoreWhen_t1352154806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (JsonIgnore_t466986466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[1] = 
{
	JsonIgnore_t466986466::get_offset_of_U3CUsageU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (JsonIgnoreMember_t766617150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[1] = 
{
	JsonIgnoreMember_t766617150::get_offset_of_U3CMembersU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (JsonInclude_t3368190666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (JsonAlias_t2355411216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[2] = 
{
	JsonAlias_t2355411216::get_offset_of_U3CAliasU3Ek__BackingField_0(),
	JsonAlias_t2355411216::get_offset_of_U3CAcceptOriginalU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (JsonData_t269267574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[4] = 
{
	JsonData_t269267574::get_offset_of_val_0(),
	JsonData_t269267574::get_offset_of_json_1(),
	JsonData_t269267574::get_offset_of_type_2(),
	JsonData_t269267574::get_offset_of_list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (OrderedDictionaryEnumerator_t3437478891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	OrderedDictionaryEnumerator_t3437478891::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (JsonException_t613047007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (PropertyMetadata_t3693826136)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[6] = 
{
	PropertyMetadata_t3693826136::get_offset_of_U3CTypeU3Ek__BackingField_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_U3CInfoU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_U3CIgnoreU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_U3CAliasU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_U3CIsFieldU3Ek__BackingField_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PropertyMetadata_t3693826136::get_offset_of_U3CIncludeU3Ek__BackingField_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (ArrayMetadata_t2008834462)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	ArrayMetadata_t2008834462::get_offset_of_elemType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_U3CIsArrayU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ArrayMetadata_t2008834462::get_offset_of_U3CIsListU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (ObjectMetadata_t3995922398)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[3] = 
{
	ObjectMetadata_t3995922398::get_offset_of_elemType_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_U3CPropertiesU3Ek__BackingField_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ObjectMetadata_t3995922398::get_offset_of_U3CIsDictionaryU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (JsonMapper_t800426905), -1, sizeof(JsonMapper_t800426905_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[39] = 
{
	JsonMapper_t800426905_StaticFields::get_offset_of_maxNestingDepth_0(),
	JsonMapper_t800426905_StaticFields::get_offset_of_datetimeFormat_1(),
	JsonMapper_t800426905_StaticFields::get_offset_of_baseExportTable_2(),
	JsonMapper_t800426905_StaticFields::get_offset_of_customExportTable_3(),
	JsonMapper_t800426905_StaticFields::get_offset_of_baseImportTable_4(),
	JsonMapper_t800426905_StaticFields::get_offset_of_customImportTable_5(),
	JsonMapper_t800426905_StaticFields::get_offset_of_customFactoryTable_6(),
	JsonMapper_t800426905_StaticFields::get_offset_of_arrayMetadata_7(),
	JsonMapper_t800426905_StaticFields::get_offset_of_convOps_8(),
	JsonMapper_t800426905_StaticFields::get_offset_of_objectMetadata_9(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_10(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheB_11(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheC_12(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheD_13(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheE_14(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cacheF_15(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache10_16(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache11_17(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache12_18(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache13_19(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache14_20(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache15_21(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache16_22(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache17_23(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache18_24(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache19_25(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1A_26(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1B_27(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1C_28(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1D_29(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1E_30(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache1F_31(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache20_32(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache21_33(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache22_34(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache23_35(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache24_36(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache25_37(),
	JsonMapper_t800426905_StaticFields::get_offset_of_U3CU3Ef__amU24cache26_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (JsonMockWrapper_t4150814299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (JsonToken_t2852816099)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2343[12] = 
{
	JsonToken_t2852816099::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (JsonReader_t1077921503), -1, sizeof(JsonReader_t1077921503_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2344[18] = 
{
	JsonReader_t1077921503_StaticFields::get_offset_of_parseTable_0(),
	JsonReader_t1077921503::get_offset_of_automationStack_1(),
	JsonReader_t1077921503::get_offset_of_lexer_2(),
	JsonReader_t1077921503::get_offset_of_reader_3(),
	JsonReader_t1077921503::get_offset_of_currentInput_4(),
	JsonReader_t1077921503::get_offset_of_currentSymbol_5(),
	JsonReader_t1077921503::get_offset_of_parserInString_6(),
	JsonReader_t1077921503::get_offset_of_parserReturn_7(),
	JsonReader_t1077921503::get_offset_of_readStarted_8(),
	JsonReader_t1077921503::get_offset_of_readerIsOwned_9(),
	JsonReader_t1077921503::get_offset_of_U3CSkipNonMembersU3Ek__BackingField_10(),
	JsonReader_t1077921503::get_offset_of_U3CTypeHintingU3Ek__BackingField_11(),
	JsonReader_t1077921503::get_offset_of_U3CHintTypeNameU3Ek__BackingField_12(),
	JsonReader_t1077921503::get_offset_of_U3CHintValueNameU3Ek__BackingField_13(),
	JsonReader_t1077921503::get_offset_of_U3CEndOfInputU3Ek__BackingField_14(),
	JsonReader_t1077921503::get_offset_of_U3CEndOfJsonU3Ek__BackingField_15(),
	JsonReader_t1077921503::get_offset_of_U3CTokenU3Ek__BackingField_16(),
	JsonReader_t1077921503::get_offset_of_U3CValueU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (Condition_t1980525237)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[6] = 
{
	Condition_t1980525237::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (WriterContext_t4137194742), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[5] = 
{
	WriterContext_t4137194742::get_offset_of_Count_0(),
	WriterContext_t4137194742::get_offset_of_Padding_1(),
	WriterContext_t4137194742::get_offset_of_InArray_2(),
	WriterContext_t4137194742::get_offset_of_InObject_3(),
	WriterContext_t4137194742::get_offset_of_ExpectingValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (JsonWriter_t1927598499), -1, sizeof(JsonWriter_t1927598499_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2347[14] = 
{
	JsonWriter_t1927598499_StaticFields::get_offset_of_numberFormat_0(),
	JsonWriter_t1927598499::get_offset_of_context_1(),
	JsonWriter_t1927598499::get_offset_of_ctxStack_2(),
	JsonWriter_t1927598499::get_offset_of_hasReachedEnd_3(),
	JsonWriter_t1927598499::get_offset_of_hexSeq_4(),
	JsonWriter_t1927598499::get_offset_of_indentation_5(),
	JsonWriter_t1927598499::get_offset_of_indentValue_6(),
	JsonWriter_t1927598499::get_offset_of_stringBuilder_7(),
	JsonWriter_t1927598499::get_offset_of_U3CPrettyPrintU3Ek__BackingField_8(),
	JsonWriter_t1927598499::get_offset_of_U3CValidateU3Ek__BackingField_9(),
	JsonWriter_t1927598499::get_offset_of_U3CTypeHintingU3Ek__BackingField_10(),
	JsonWriter_t1927598499::get_offset_of_U3CHintTypeNameU3Ek__BackingField_11(),
	JsonWriter_t1927598499::get_offset_of_U3CHintValueNameU3Ek__BackingField_12(),
	JsonWriter_t1927598499::get_offset_of_U3CTextWriterU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (FsmContext_t1296252303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[4] = 
{
	FsmContext_t1296252303::get_offset_of_L_0(),
	FsmContext_t1296252303::get_offset_of_Return_1(),
	FsmContext_t1296252303::get_offset_of_NextState_2(),
	FsmContext_t1296252303::get_offset_of_StateStack_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (Lexer_t186508296), -1, sizeof(Lexer_t186508296_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2349[14] = 
{
	Lexer_t186508296_StaticFields::get_offset_of_returnTable_0(),
	Lexer_t186508296_StaticFields::get_offset_of_handlerTable_1(),
	Lexer_t186508296::get_offset_of_inputBuffer_2(),
	Lexer_t186508296::get_offset_of_inputChar_3(),
	Lexer_t186508296::get_offset_of_state_4(),
	Lexer_t186508296::get_offset_of_unichar_5(),
	Lexer_t186508296::get_offset_of_context_6(),
	Lexer_t186508296::get_offset_of_reader_7(),
	Lexer_t186508296::get_offset_of_stringBuffer_8(),
	Lexer_t186508296::get_offset_of_U3CAllowCommentsU3Ek__BackingField_9(),
	Lexer_t186508296::get_offset_of_U3CAllowSingleQuotedStringsU3Ek__BackingField_10(),
	Lexer_t186508296::get_offset_of_U3CEndOfInputU3Ek__BackingField_11(),
	Lexer_t186508296::get_offset_of_U3CTokenU3Ek__BackingField_12(),
	Lexer_t186508296::get_offset_of_U3CStringValueU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (StateHandler_t387387051), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (ParserToken_t1554180950)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2351[20] = 
{
	ParserToken_t1554180950::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (UnityTypeBindings_t1763022095), -1, sizeof(UnityTypeBindings_t1763022095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2352[11] = 
{
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_registerd_0(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_8(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_9(),
	UnityTypeBindings_t1763022095_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (U3CRegisterU3Ec__AnonStorey1E_t484806451), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[2] = 
{
	U3CRegisterU3Ec__AnonStorey1E_t484806451::get_offset_of_writeVector2_0(),
	U3CRegisterU3Ec__AnonStorey1E_t484806451::get_offset_of_writeVector3_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (DatabaseLoadBehaviour_t450246482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[3] = 
{
	0,
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_3(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2359[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[4] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_cantrackagain_2(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_i_3(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_gm_4(),
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (DeviceTrackerBehaviour_t1884988499), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (DigitalEyewearBehaviour_t495394589), -1, sizeof(DigitalEyewearBehaviour_t495394589_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2362[1] = 
{
	DigitalEyewearBehaviour_t495394589_StaticFields::get_offset_of_mDigitalEyewearBehaviour_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2363[3] = 
{
	0,
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_3(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (KeepAliveBehaviour_t1532923751), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (MarkerBehaviour_t1265232977), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (MaskOutBehaviour_t2994129365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (MultiTargetBehaviour_t3504654311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (ObjectTargetBehaviour_t3836044259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (PropBehaviour_t966064926), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (ReconstructionBehaviour_t4009935945), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (ReconstructionFromTargetBehaviour_t2111803406), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (SmartTerrainTrackerBehaviour_t3844158157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (SurfaceBehaviour_t2405314212), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (TextRecoBehaviour_t3400239837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (TurnOffBehaviour_t3058161409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (TurnOffWordBehaviour_t584991835), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (UserDefinedTargetBuildingBehaviour_t4184040062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (VRIntegrationHelper_t556656694), -1, sizeof(VRIntegrationHelper_t556656694_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2384[12] = 
{
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraMatrixOriginal_2(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraMatrixOriginal_3(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCamera_4(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCamera_5(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftExcessAreaBehaviour_6(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightExcessAreaBehaviour_7(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraPixelRect_8(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraPixelRect_9(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mLeftCameraDataAcquired_10(),
	VRIntegrationHelper_t556656694_StaticFields::get_offset_of_mRightCameraDataAcquired_11(),
	VRIntegrationHelper_t556656694::get_offset_of_IsLeft_12(),
	VRIntegrationHelper_t556656694::get_offset_of_TrackableParent_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (VideoBackgroundBehaviour_t3161817952), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2386[1] = 
{
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (VirtualButtonBehaviour_t2515041812), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (VuforiaBehaviour_t359035403), -1, sizeof(VuforiaBehaviour_t359035403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2388[1] = 
{
	VuforiaBehaviour_t359035403_StaticFields::get_offset_of_mVuforiaBehaviour_43(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (WebCamBehaviour_t407765638), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (WireframeBehaviour_t2494532455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[4] = 
{
	WireframeBehaviour_t2494532455::get_offset_of_lineMaterial_2(),
	WireframeBehaviour_t2494532455::get_offset_of_ShowLines_3(),
	WireframeBehaviour_t2494532455::get_offset_of_LineColor_4(),
	WireframeBehaviour_t2494532455::get_offset_of_mLineMaterial_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (WireframeTrackableEventHandler_t1535150527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[1] = 
{
	WireframeTrackableEventHandler_t1535150527::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (WordBehaviour_t3366478421), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (Warmmup_anim_t335950655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[2] = 
{
	Warmmup_anim_t335950655::get_offset_of_warmmupButton_2(),
	Warmmup_anim_t335950655::get_offset_of_warmmupAnim_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (leftoright_t233454036), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (play_anim_on_ui_button_t872675790), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[4] = 
{
	play_anim_on_ui_button_t872675790::get_offset_of_Text_2(),
	play_anim_on_ui_button_t872675790::get_offset_of_sound_3(),
	play_anim_on_ui_button_t872675790::get_offset_of_ani_4(),
	play_anim_on_ui_button_t872675790::get_offset_of_yourcanvas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (ExporterFunc_t408878057), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (ImporterFunc_t2977850894), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
