﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>>
struct IDictionary_2_t1339185049;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t3159605602;
// LitJson.Lexer
struct Lexer_t186508296;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LitJson_JsonToken2852816099.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonReader
struct  JsonReader_t1077921503  : public Il2CppObject
{
public:
	// System.Collections.Generic.Stack`1<System.Int32> LitJson.JsonReader::automationStack
	Stack_1_t3159605602 * ___automationStack_1;
	// LitJson.Lexer LitJson.JsonReader::lexer
	Lexer_t186508296 * ___lexer_2;
	// System.IO.TextReader LitJson.JsonReader::reader
	TextReader_t1561828458 * ___reader_3;
	// System.Int32 LitJson.JsonReader::currentInput
	int32_t ___currentInput_4;
	// System.Int32 LitJson.JsonReader::currentSymbol
	int32_t ___currentSymbol_5;
	// System.Boolean LitJson.JsonReader::parserInString
	bool ___parserInString_6;
	// System.Boolean LitJson.JsonReader::parserReturn
	bool ___parserReturn_7;
	// System.Boolean LitJson.JsonReader::readStarted
	bool ___readStarted_8;
	// System.Boolean LitJson.JsonReader::readerIsOwned
	bool ___readerIsOwned_9;
	// System.Boolean LitJson.JsonReader::<SkipNonMembers>k__BackingField
	bool ___U3CSkipNonMembersU3Ek__BackingField_10;
	// System.Boolean LitJson.JsonReader::<TypeHinting>k__BackingField
	bool ___U3CTypeHintingU3Ek__BackingField_11;
	// System.String LitJson.JsonReader::<HintTypeName>k__BackingField
	String_t* ___U3CHintTypeNameU3Ek__BackingField_12;
	// System.String LitJson.JsonReader::<HintValueName>k__BackingField
	String_t* ___U3CHintValueNameU3Ek__BackingField_13;
	// System.Boolean LitJson.JsonReader::<EndOfInput>k__BackingField
	bool ___U3CEndOfInputU3Ek__BackingField_14;
	// System.Boolean LitJson.JsonReader::<EndOfJson>k__BackingField
	bool ___U3CEndOfJsonU3Ek__BackingField_15;
	// LitJson.JsonToken LitJson.JsonReader::<Token>k__BackingField
	int32_t ___U3CTokenU3Ek__BackingField_16;
	// System.Object LitJson.JsonReader::<Value>k__BackingField
	Il2CppObject * ___U3CValueU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_automationStack_1() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___automationStack_1)); }
	inline Stack_1_t3159605602 * get_automationStack_1() const { return ___automationStack_1; }
	inline Stack_1_t3159605602 ** get_address_of_automationStack_1() { return &___automationStack_1; }
	inline void set_automationStack_1(Stack_1_t3159605602 * value)
	{
		___automationStack_1 = value;
		Il2CppCodeGenWriteBarrier(&___automationStack_1, value);
	}

	inline static int32_t get_offset_of_lexer_2() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___lexer_2)); }
	inline Lexer_t186508296 * get_lexer_2() const { return ___lexer_2; }
	inline Lexer_t186508296 ** get_address_of_lexer_2() { return &___lexer_2; }
	inline void set_lexer_2(Lexer_t186508296 * value)
	{
		___lexer_2 = value;
		Il2CppCodeGenWriteBarrier(&___lexer_2, value);
	}

	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___reader_3)); }
	inline TextReader_t1561828458 * get_reader_3() const { return ___reader_3; }
	inline TextReader_t1561828458 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(TextReader_t1561828458 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier(&___reader_3, value);
	}

	inline static int32_t get_offset_of_currentInput_4() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___currentInput_4)); }
	inline int32_t get_currentInput_4() const { return ___currentInput_4; }
	inline int32_t* get_address_of_currentInput_4() { return &___currentInput_4; }
	inline void set_currentInput_4(int32_t value)
	{
		___currentInput_4 = value;
	}

	inline static int32_t get_offset_of_currentSymbol_5() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___currentSymbol_5)); }
	inline int32_t get_currentSymbol_5() const { return ___currentSymbol_5; }
	inline int32_t* get_address_of_currentSymbol_5() { return &___currentSymbol_5; }
	inline void set_currentSymbol_5(int32_t value)
	{
		___currentSymbol_5 = value;
	}

	inline static int32_t get_offset_of_parserInString_6() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___parserInString_6)); }
	inline bool get_parserInString_6() const { return ___parserInString_6; }
	inline bool* get_address_of_parserInString_6() { return &___parserInString_6; }
	inline void set_parserInString_6(bool value)
	{
		___parserInString_6 = value;
	}

	inline static int32_t get_offset_of_parserReturn_7() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___parserReturn_7)); }
	inline bool get_parserReturn_7() const { return ___parserReturn_7; }
	inline bool* get_address_of_parserReturn_7() { return &___parserReturn_7; }
	inline void set_parserReturn_7(bool value)
	{
		___parserReturn_7 = value;
	}

	inline static int32_t get_offset_of_readStarted_8() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___readStarted_8)); }
	inline bool get_readStarted_8() const { return ___readStarted_8; }
	inline bool* get_address_of_readStarted_8() { return &___readStarted_8; }
	inline void set_readStarted_8(bool value)
	{
		___readStarted_8 = value;
	}

	inline static int32_t get_offset_of_readerIsOwned_9() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___readerIsOwned_9)); }
	inline bool get_readerIsOwned_9() const { return ___readerIsOwned_9; }
	inline bool* get_address_of_readerIsOwned_9() { return &___readerIsOwned_9; }
	inline void set_readerIsOwned_9(bool value)
	{
		___readerIsOwned_9 = value;
	}

	inline static int32_t get_offset_of_U3CSkipNonMembersU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CSkipNonMembersU3Ek__BackingField_10)); }
	inline bool get_U3CSkipNonMembersU3Ek__BackingField_10() const { return ___U3CSkipNonMembersU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CSkipNonMembersU3Ek__BackingField_10() { return &___U3CSkipNonMembersU3Ek__BackingField_10; }
	inline void set_U3CSkipNonMembersU3Ek__BackingField_10(bool value)
	{
		___U3CSkipNonMembersU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTypeHintingU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CTypeHintingU3Ek__BackingField_11)); }
	inline bool get_U3CTypeHintingU3Ek__BackingField_11() const { return ___U3CTypeHintingU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CTypeHintingU3Ek__BackingField_11() { return &___U3CTypeHintingU3Ek__BackingField_11; }
	inline void set_U3CTypeHintingU3Ek__BackingField_11(bool value)
	{
		___U3CTypeHintingU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CHintTypeNameU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CHintTypeNameU3Ek__BackingField_12)); }
	inline String_t* get_U3CHintTypeNameU3Ek__BackingField_12() const { return ___U3CHintTypeNameU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CHintTypeNameU3Ek__BackingField_12() { return &___U3CHintTypeNameU3Ek__BackingField_12; }
	inline void set_U3CHintTypeNameU3Ek__BackingField_12(String_t* value)
	{
		___U3CHintTypeNameU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHintTypeNameU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CHintValueNameU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CHintValueNameU3Ek__BackingField_13)); }
	inline String_t* get_U3CHintValueNameU3Ek__BackingField_13() const { return ___U3CHintValueNameU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CHintValueNameU3Ek__BackingField_13() { return &___U3CHintValueNameU3Ek__BackingField_13; }
	inline void set_U3CHintValueNameU3Ek__BackingField_13(String_t* value)
	{
		___U3CHintValueNameU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHintValueNameU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CEndOfInputU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CEndOfInputU3Ek__BackingField_14)); }
	inline bool get_U3CEndOfInputU3Ek__BackingField_14() const { return ___U3CEndOfInputU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CEndOfInputU3Ek__BackingField_14() { return &___U3CEndOfInputU3Ek__BackingField_14; }
	inline void set_U3CEndOfInputU3Ek__BackingField_14(bool value)
	{
		___U3CEndOfInputU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CEndOfJsonU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CEndOfJsonU3Ek__BackingField_15)); }
	inline bool get_U3CEndOfJsonU3Ek__BackingField_15() const { return ___U3CEndOfJsonU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CEndOfJsonU3Ek__BackingField_15() { return &___U3CEndOfJsonU3Ek__BackingField_15; }
	inline void set_U3CEndOfJsonU3Ek__BackingField_15(bool value)
	{
		___U3CEndOfJsonU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CTokenU3Ek__BackingField_16)); }
	inline int32_t get_U3CTokenU3Ek__BackingField_16() const { return ___U3CTokenU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CTokenU3Ek__BackingField_16() { return &___U3CTokenU3Ek__BackingField_16; }
	inline void set_U3CTokenU3Ek__BackingField_16(int32_t value)
	{
		___U3CTokenU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503, ___U3CValueU3Ek__BackingField_17)); }
	inline Il2CppObject * get_U3CValueU3Ek__BackingField_17() const { return ___U3CValueU3Ek__BackingField_17; }
	inline Il2CppObject ** get_address_of_U3CValueU3Ek__BackingField_17() { return &___U3CValueU3Ek__BackingField_17; }
	inline void set_U3CValueU3Ek__BackingField_17(Il2CppObject * value)
	{
		___U3CValueU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CValueU3Ek__BackingField_17, value);
	}
};

struct JsonReader_t1077921503_StaticFields
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Collections.Generic.IDictionary`2<System.Int32,System.Int32[]>> LitJson.JsonReader::parseTable
	Il2CppObject* ___parseTable_0;

public:
	inline static int32_t get_offset_of_parseTable_0() { return static_cast<int32_t>(offsetof(JsonReader_t1077921503_StaticFields, ___parseTable_0)); }
	inline Il2CppObject* get_parseTable_0() const { return ___parseTable_0; }
	inline Il2CppObject** get_address_of_parseTable_0() { return &___parseTable_0; }
	inline void set_parseTable_0(Il2CppObject* value)
	{
		___parseTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___parseTable_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
