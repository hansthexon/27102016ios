﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Purchaser/<Paytoserver>c__Iterator4
struct U3CPaytoserverU3Ec__Iterator4_t2045108845;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Purchaser/<Paytoserver>c__Iterator4::.ctor()
extern "C"  void U3CPaytoserverU3Ec__Iterator4__ctor_m3767305892 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Purchaser/<Paytoserver>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPaytoserverU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m941515390 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Purchaser/<Paytoserver>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPaytoserverU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1500391014 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Purchaser/<Paytoserver>c__Iterator4::MoveNext()
extern "C"  bool U3CPaytoserverU3Ec__Iterator4_MoveNext_m2901040480 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser/<Paytoserver>c__Iterator4::Dispose()
extern "C"  void U3CPaytoserverU3Ec__Iterator4_Dispose_m572833503 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Purchaser/<Paytoserver>c__Iterator4::Reset()
extern "C"  void U3CPaytoserverU3Ec__Iterator4_Reset_m178449585 (U3CPaytoserverU3Ec__Iterator4_t2045108845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
