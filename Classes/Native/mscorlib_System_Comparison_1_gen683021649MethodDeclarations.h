﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Comparison_1_t683021649;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3205530687_gshared (Comparison_1_t683021649 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m3205530687(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t683021649 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m3205530687_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m1435415269_gshared (Comparison_1_t683021649 * __this, KeyValuePair_2_t3716250094  ___x0, KeyValuePair_2_t3716250094  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m1435415269(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t683021649 *, KeyValuePair_2_t3716250094 , KeyValuePair_2_t3716250094 , const MethodInfo*))Comparison_1_Invoke_m1435415269_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m2126254898_gshared (Comparison_1_t683021649 * __this, KeyValuePair_2_t3716250094  ___x0, KeyValuePair_2_t3716250094  ___y1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m2126254898(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t683021649 *, KeyValuePair_2_t3716250094 , KeyValuePair_2_t3716250094 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m2126254898_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m3110716299_gshared (Comparison_1_t683021649 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m3110716299(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t683021649 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m3110716299_gshared)(__this, ___result0, method)
