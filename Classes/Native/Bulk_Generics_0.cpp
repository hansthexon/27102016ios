﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// LitJson.ExporterFunc`1<System.Object>
struct ExporterFunc_1_t3926562242;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// LitJson.ExporterFunc`1<UnityEngine.Bounds>
struct ExporterFunc_1_t4270476650;
// LitJson.ExporterFunc`1<UnityEngine.Color>
struct ExporterFunc_1_t3257505022;
// LitJson.ExporterFunc`1<UnityEngine.Color32>
struct ExporterFunc_1_t2111630465;
// LitJson.ExporterFunc`1<UnityEngine.Quaternion>
struct ExporterFunc_1_t972219569;
// LitJson.ExporterFunc`1<UnityEngine.Rect>
struct ExporterFunc_1_t623901277;
// LitJson.ExporterFunc`1<UnityEngine.Vector2>
struct ExporterFunc_1_t3480820526;
// LitJson.ExporterFunc`1<UnityEngine.Vector3>
struct ExporterFunc_1_t3480820527;
// LitJson.ExporterFunc`1<UnityEngine.Vector4>
struct ExporterFunc_1_t3480820528;
// LitJson.FactoryFunc`1<System.Object>
struct FactoryFunc_1_t741989655;
// LitJson.ImporterFunc`2<System.Object,System.Object>
struct ImporterFunc_2_t2548729379;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Bounds>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t4440352;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3286436020;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color32>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t2140561463;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Quaternion>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t1001150567;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Rect>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t652832275;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector2>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751524;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector3>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751525;
// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector4>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751526;
// LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>
struct U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741;
// LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>
struct U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t1463927430;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t958855109;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t3641524600;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2854920344;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t1809345768;
// SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>
struct U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>
struct CompileResult_t4163790932;
// System.String
struct String_t;
// SQLite4Unity3d.TableQuery`1<System.Object>
struct TableQuery_1_t4026329353;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;
// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// System.Linq.Expressions.Expression`1<System.Func`2<System.Object,System.Boolean>>
struct Expression_1_t4002038744;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// SQLite4Unity3d.SQLiteCommand
struct SQLiteCommand_t2935685145;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.BaseTableQuery/Ordering>
struct IEnumerable_1_t1330989839;
// System.Func`2<SQLite4Unity3d.BaseTableQuery/Ordering,System.String>
struct Func_2_t4282907944;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>>
struct IEnumerable_1_t160950681;
// System.Func`2<SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>,System.String>
struct Func_2_t1767698742;
// System.Type
struct Type_t;
// System.Linq.Expressions.BinaryExpression
struct BinaryExpression_t2159924157;
// SQLite4Unity3d.BaseTableQuery/Ordering
struct Ordering_t1038862794;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>
struct Action_1_t2755832024;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t904151438;
// System.Action`1<Vuforia.VuforiaUnity/InitError>
struct Action_1_t1951195598;
// System.Action`2<System.Boolean,System.Int32>
struct Action_2_t1907880187;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>
struct Action_2_t2790035381;
// System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>
struct Action_2_t1158962578;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Action`2<UnityEngine.Vector2,System.Object>
struct Action_2_t3938509041;
// System.Action`2<UnityEngine.Vector3,System.Object>
struct Action_2_t129729484;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen4270476650.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen4270476650MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3257505022.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3257505022MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen2111630465.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen2111630465MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen972219569.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen972219569MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen623901277.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen623901277MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820526.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820526MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820527.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820527MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820528.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3480820528MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "AssemblyU2DCSharp_LitJson_FactoryFunc_1_gen741989655.h"
#include "AssemblyU2DCSharp_LitJson_FactoryFunc_1_gen741989655MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc_2_gen2548729379MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3955493240.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3955493240MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExpor4440352.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExpor4440352MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3286436020.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3286436020MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx2140561463.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx2140561463MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx1001150567.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx1001150567MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExp652832275.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterExp652832275MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751524.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751524MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751525.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751525MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751526.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterEx3509751526MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterFac955442741.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterFac955442741MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm2585850632.h"
#include "AssemblyU2DCSharp_LitJson_JsonMapper_U3CRegisterIm2585850632MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1463927430.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_1463927430MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T958855109.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2281509423MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Threading_Monitor3228523394MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_T958855109MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException2785117854.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke470039898.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va984569266.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand_U3C3483008074.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand_U3C3483008074MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked1625106012MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand2935685145.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand2935685145MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3150757461MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ColType2341528904.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_Result1450270481.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_Comp4163790932.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_Comp4163790932MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_gen4026329353.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_gen4026329353MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery3770317269MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_CreateFlags3110178347.h"
#include "System_Core_System_Linq_Expressions_Expression_1_g4002038744.h"
#include "System_Core_System_Linq_Expressions_Expression114864668MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_LambdaExpressi2811402413MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_LambdaExpressi2811402413.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "System_Core_System_Linq_Expressions_ExpressionType1567188220.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Nullable_1_gen334943763MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_BinaryExpressi2159924157.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen407983926MethodDeclarations.h"
#include "System_Core_System_Func_2_gen4282907944MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2058570427.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery3770317269.h"
#include "mscorlib_System_Collections_Generic_List_1_gen407983926.h"
#include "System_Core_System_Func_2_gen4282907944.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery_Or1038862794.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "System_Core_System_Linq_Expressions_BinaryExpressi2159924157MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_MethodCallExpr3367820543MethodDeclarations.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol300650360MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_ConstantExpress305952364MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_UnaryExpressio4253534555MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_MemberExpressi1790982958MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder1221177846MethodDeclarations.h"
#include "System_Core_System_Linq_Expressions_MethodCallExpr3367820543.h"
#include "System_Core_System_Linq_Expressions_ConstantExpress305952364.h"
#include "System_Core_System_Linq_Expressions_UnaryExpressio4253534555.h"
#include "System_Core_System_Linq_Expressions_MemberExpressi1790982958.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "mscorlib_System_Reflection_FieldInfo255040150.h"
#include "mscorlib_System_Text_StringBuilder1221177846.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCol300650360.h"
#include "mscorlib_System_Reflection_MethodInfo3330546337.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo4043097260.h"
#include "System_Core_System_Func_2_gen1767698742.h"
#include "System_Core_System_Func_2_gen1767698742MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberTypes3343038963.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo255040150MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545MethodDeclarations.h"
#include "mscorlib_System_Enum2459695545.h"
#include "mscorlib_System_Nullable1429764613MethodDeclarations.h"
#include "mscorlib_System_Convert2607082565MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery_Or1038862794MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2755832024.h"
#include "mscorlib_System_Action_1_gen2755832024MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Init2954032642.h"
#include "mscorlib_System_Action_1_gen904151438.h"
#include "mscorlib_System_Action_1_gen904151438MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainInitia1102352056.h"
#include "mscorlib_System_Action_1_gen1951195598.h"
#include "mscorlib_System_Action_1_gen1951195598MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "System_Core_System_Action_2_gen1907880187.h"
#include "System_Core_System_Action_2_gen1907880187MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2790035381.h"
#include "System_Core_System_Action_2_gen2790035381MethodDeclarations.h"
#include "System_Core_System_Action_2_gen1158962578.h"
#include "System_Core_System_Action_2_gen1158962578MethodDeclarations.h"
#include "UnityEngine_Purchasing_UnityEngine_Purchasing_Purc1322959839.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "System_Core_System_Action_2_gen3938509041.h"
#include "System_Core_System_Action_2_gen3938509041MethodDeclarations.h"
#include "System_Core_System_Action_2_gen129729484.h"
#include "System_Core_System_Action_2_gen129729484MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2867586724.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2867586724MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen559707364.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen559707364MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen257611102.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen257611102MethodDeclarations.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1532912250.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1532912250MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1706787027.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1706787027MethodDeclarations.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3866418389.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3866418389MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3142776066.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3142776066MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22284023804.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen216992074.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen216992074MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23653207108.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2204080010.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2204080010MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21345327748.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1901983748.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1901983748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21043231486.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3351159673.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3351159673MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2541979553.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2541979553MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21683227291.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490007519.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3490007519MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22631255257.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4227791396.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4227791396MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23369039134.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2999800314.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2999800314MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22141048052.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2022531261.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3126312864.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3126312864MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2267560602.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1551957931.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1551957931MethodDeclarations.h"
#include "mscorlib_System_DateTime693205669.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1583453339.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1583453339MethodDeclarations.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen641800647.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen641800647MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen605030880.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen605030880MethodDeclarations.h"
#include "mscorlib_System_Int164041245914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2930629710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2930629710MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1767830299.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1767830299MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3362812871.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3362812871MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3548201557MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952909805.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen952909805MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2356950176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2356950176MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen275897710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen275897710MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD3712112744.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen654694480.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen654694480MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelF4090909514.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008311600.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1008311600MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo149559338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679387182.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2679387182MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier1820634920.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1191988411.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1191988411MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCa333236149.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen496834202.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen496834202MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI3933049236.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen999961858.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen999961858MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Bi141209596.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1313169811.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1313169811MethodDeclarations.h"
#include "mscorlib_System_SByte454417549.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen842163687.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen842163687MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat4278378721.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2935262194MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3583626735MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark2724874473.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4289011211MethodDeclarations.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1845634873MethodDeclarations.h"
#include "mscorlib_System_UInt16986882611.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3008434283MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3767949176MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2735343205.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2735343205MethodDeclarations.h"
#include "System_System_Uri_UriScheme1876590943.h"

// SQLite4Unity3d.TableQuery`1<!!0> SQLite4Unity3d.TableQuery`1<System.Object>::Clone<System.Object>()
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Clone_TisIl2CppObject_m1961640408_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method);
#define TableQuery_1_Clone_TisIl2CppObject_m1961640408(__this, method) ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))TableQuery_1_Clone_TisIl2CppObject_m1961640408_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
#define Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t2825504181 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<SQLite4Unity3d.BaseTableQuery/Ordering,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisOrdering_t1038862794_TisString_t_m2214280582(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t4282907944 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3301715283(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1953054010(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_Select_TisCompileResult_t4163790932_TisString_t_m2741123520(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1767698742 *, const MethodInfo*))Enumerable_Select_TisIl2CppObject_TisIl2CppObject_m737819943_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 SQLite4Unity3d.SQLiteCommand::ExecuteScalar<System.Int32>()
extern "C"  int32_t SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099_gshared (SQLiteCommand_t2935685145 * __this, const MethodInfo* method);
#define SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099(__this, method) ((  int32_t (*) (SQLiteCommand_t2935685145 *, const MethodInfo*))SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> SQLite4Unity3d.SQLiteCommand::ExecuteQuery<System.Object>()
extern "C"  List_1_t2058570427 * SQLiteCommand_ExecuteQuery_TisIl2CppObject_m651492738_gshared (SQLiteCommand_t2935685145 * __this, const MethodInfo* method);
#define SQLiteCommand_ExecuteQuery_TisIl2CppObject_m651492738(__this, method) ((  List_1_t2058570427 * (*) (SQLiteCommand_t2935685145 *, const MethodInfo*))SQLiteCommand_ExecuteQuery_TisIl2CppObject_m651492738_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<!!0> SQLite4Unity3d.SQLiteCommand::ExecuteDeferredQuery<System.Object>()
extern "C"  Il2CppObject* SQLiteCommand_ExecuteDeferredQuery_TisIl2CppObject_m1473299058_gshared (SQLiteCommand_t2935685145 * __this, const MethodInfo* method);
#define SQLiteCommand_ExecuteDeferredQuery_TisIl2CppObject_m1473299058(__this, method) ((  Il2CppObject* (*) (SQLiteCommand_t2935685145 *, const MethodInfo*))SQLiteCommand_ExecuteDeferredQuery_TisIl2CppObject_m1473299058_gshared)(__this, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3361462832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3361462832(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m2000881923_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m2000881923(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m2000881923_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::FirstOrDefault<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_FirstOrDefault_TisIl2CppObject_m3734406196_gshared)(__this /* static, unused */, p0, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<LitJson.ArrayMetadata>(System.Int32)
extern "C"  ArrayMetadata_t2008834462  Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323(__this, p0, method) ((  ArrayMetadata_t2008834462  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisArrayMetadata_t2008834462_m2121890323_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<LitJson.ObjectMetadata>(System.Int32)
extern "C"  ObjectMetadata_t3995922398  Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155(__this, p0, method) ((  ObjectMetadata_t3995922398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisObjectMetadata_t3995922398_m2864510155_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<LitJson.PropertyMetadata>(System.Int32)
extern "C"  PropertyMetadata_t3693826136  Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445(__this, p0, method) ((  PropertyMetadata_t3693826136  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisPropertyMetadata_t3693826136_m629882445_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, p0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<SQLite4Unity3d.SQLiteConnection/IndexedColumn>(System.Int32)
extern "C"  IndexedColumn_t674159988  Array_InternalArray__get_Item_TisIndexedColumn_t674159988_m3374485756_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIndexedColumn_t674159988_m3374485756(__this, p0, method) ((  IndexedColumn_t674159988  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIndexedColumn_t674159988_m3374485756_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<SQLite4Unity3d.SQLiteConnection/IndexInfo>(System.Int32)
extern "C"  IndexInfo_t848034765  Array_InternalArray__get_Item_TisIndexInfo_t848034765_m995089407_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIndexInfo_t848034765_m995089407(__this, p0, method) ((  IndexInfo_t848034765  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIndexInfo_t848034765_m995089407_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, p0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, p0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118(__this, p0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>(System.Int32)
extern "C"  KeyValuePair_2_t3007666127  Array_InternalArray__get_Item_TisKeyValuePair_2_t3007666127_m524916950_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3007666127_m524916950(__this, p0, method) ((  KeyValuePair_2_t3007666127  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3007666127_m524916950_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>(System.Int32)
extern "C"  KeyValuePair_2_t2284023804  Array_InternalArray__get_Item_TisKeyValuePair_2_t2284023804_m3029700973_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2284023804_m3029700973(__this, p0, method) ((  KeyValuePair_2_t2284023804  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2284023804_m3029700973_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t3653207108  Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578(__this, p0, method) ((  KeyValuePair_2_t3653207108  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3653207108_m2224743578_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t1345327748  Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408(__this, p0, method) ((  KeyValuePair_2_t1345327748  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1345327748_m1077835408_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>(System.Int32)
extern "C"  KeyValuePair_2_t1043231486  Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066(__this, p0, method) ((  KeyValuePair_2_t1043231486  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1043231486_m1426260066_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>(System.Int32)
extern "C"  KeyValuePair_2_t2492407411  Array_InternalArray__get_Item_TisKeyValuePair_2_t2492407411_m780705448_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2492407411_m780705448(__this, p0, method) ((  KeyValuePair_2_t2492407411  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2492407411_m780705448_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t1174980068  Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642(__this, p0, method) ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>(System.Int32)
extern "C"  KeyValuePair_2_t1683227291  Array_InternalArray__get_Item_TisKeyValuePair_2_t1683227291_m3180983282_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1683227291_m3180983282(__this, p0, method) ((  KeyValuePair_2_t1683227291  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1683227291_m3180983282_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630(__this, p0, method) ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t38854645  Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821(__this, p0, method) ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>(System.Int32)
extern "C"  KeyValuePair_2_t2631255257  Array_InternalArray__get_Item_TisKeyValuePair_2_t2631255257_m3121053769_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2631255257_m3121053769(__this, p0, method) ((  KeyValuePair_2_t2631255257  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2631255257_m3121053769_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>(System.Int32)
extern "C"  KeyValuePair_2_t3369039134  Array_InternalArray__get_Item_TisKeyValuePair_2_t3369039134_m3337114397_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3369039134_m3337114397(__this, p0, method) ((  KeyValuePair_2_t3369039134  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3369039134_m3337114397_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t2141048052  Array_InternalArray__get_Item_TisKeyValuePair_2_t2141048052_m2771391093_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2141048052_m2771391093(__this, p0, method) ((  KeyValuePair_2_t2141048052  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2141048052_m2771391093_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2723257478  Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655(__this, p0, method) ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2022531261  Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551(__this, p0, method) ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2267560602  Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430(__this, p0, method) ((  Slot_t2267560602  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2267560602_m1004716430_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t693205669  Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220(__this, p0, method) ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t693205669_m3661692220_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t724701077  Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600(__this, p0, method) ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t724701077_m4156246600_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t4078015681_m2215331088_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t4041245914_m2533263979_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2071877448_m966348849_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t909078037_m1431563204_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m210946760(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m210946760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m371871810(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m371871810_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t94157543  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745(__this, p0, method) ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t94157543_m4258992745_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t1498197914  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094(__this, p0, method) ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t1498197914_m1864496094_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t3712112744  Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768(__this, p0, method) ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t3712112744_m863115768_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t4090909514  Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142(__this, p0, method) ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t4090909514_m2966857142_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t149559338  Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537(__this, p0, method) ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t149559338_m2004750537_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t1820634920  Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304(__this, p0, method) ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t1820634920_m1898755304_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t333236149  Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631(__this, p0, method) ((  ResourceCacheItem_t333236149  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t333236149_m649009631_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t3933049236  Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352(__this, p0, method) ((  ResourceInfo_t3933049236  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t3933049236_m107404352_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t141209596_m1747911007_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t454417549_m3315206452_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t4278378721  Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500(__this, p0, method) ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t4278378721_m4197592500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t2076509932_m1495809753_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t2724874473  Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706(__this, p0, method) ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t2724874473_m2044327706_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t3430258949  Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260(__this, p0, method) ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t3430258949_m1147719260_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t986882611_m2599215710_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t2149682021_m2554907852_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t2909196914_m2580870875_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t1876590943  Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697(__this, p0, method) ((  UriScheme_t1876590943  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t1876590943_m1821482697_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LitJson.ExporterFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m104989128_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1089798616_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m1089798616((ExporterFunc_1_t3926562242 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<System.Object>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m729158479_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___obj0;
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2442068774_gshared (ExporterFunc_1_t3926562242 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m732779106_gshared (ExporterFunc_1_t4270476650 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m361856306_gshared (ExporterFunc_1_t4270476650 * __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m361856306((ExporterFunc_1_t4270476650 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Bounds>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Bounds_t3033363703_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m2799634367_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m2799634367_gshared (ExporterFunc_1_t4270476650 * __this, Bounds_t3033363703  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m2799634367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Bounds_t3033363703_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Bounds>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m2748156268_gshared (ExporterFunc_1_t4270476650 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m1115524192_gshared (ExporterFunc_1_t3257505022 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m6700552_gshared (ExporterFunc_1_t3257505022 * __this, Color_t2020392075  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m6700552((ExporterFunc_1_t3257505022 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Color>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m3907141715_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m3907141715_gshared (ExporterFunc_1_t3257505022 * __this, Color_t2020392075  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m3907141715_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m449144682_gshared (ExporterFunc_1_t3257505022 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m413661069_gshared (ExporterFunc_1_t2111630465 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color32>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m3470495253_gshared (ExporterFunc_1_t2111630465 * __this, Color32_t874517518  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m3470495253((ExporterFunc_1_t2111630465 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Color32>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m1344397916_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m1344397916_gshared (ExporterFunc_1_t2111630465 * __this, Color32_t874517518  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m1344397916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m442287619_gshared (ExporterFunc_1_t2111630465 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m1175308825_gshared (ExporterFunc_1_t972219569 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Quaternion>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1919571497_gshared (ExporterFunc_1_t972219569 * __this, Quaternion_t4030073918  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m1919571497((ExporterFunc_1_t972219569 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t4030073918  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t4030073918  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Quaternion>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m1414879018_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m1414879018_gshared (ExporterFunc_1_t972219569 * __this, Quaternion_t4030073918  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m1414879018_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m3500039615_gshared (ExporterFunc_1_t972219569 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m456614893_gshared (ExporterFunc_1_t623901277 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Rect>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m294833885_gshared (ExporterFunc_1_t623901277 * __this, Rect_t3681755626  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m294833885((ExporterFunc_1_t623901277 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t3681755626  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t3681755626  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Rect>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m929896488_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m929896488_gshared (ExporterFunc_1_t623901277 * __this, Rect_t3681755626  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m929896488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Rect_t3681755626_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m858257203_gshared (ExporterFunc_1_t623901277 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m3160275564_gshared (ExporterFunc_1_t3480820526 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector2>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m1894960556_gshared (ExporterFunc_1_t3480820526 * __this, Vector2_t2243707579  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m1894960556((ExporterFunc_1_t3480820526 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Vector2>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m1681129073_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m1681129073_gshared (ExporterFunc_1_t3480820526 * __this, Vector2_t2243707579  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m1681129073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m1522026190_gshared (ExporterFunc_1_t3480820526 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m3607187655_gshared (ExporterFunc_1_t3480820527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector3>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m3009428519_gshared (ExporterFunc_1_t3480820527 * __this, Vector3_t2243707580  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m3009428519((ExporterFunc_1_t3480820527 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Vector3>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m1106969526_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m1106969526_gshared (ExporterFunc_1_t3480820527 * __this, Vector3_t2243707580  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m1106969526_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m4141925737_gshared (ExporterFunc_1_t3480820527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void ExporterFunc_1__ctor_m185295394_gshared (ExporterFunc_1_t3480820528 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector4>::Invoke(T,LitJson.JsonWriter)
extern "C"  void ExporterFunc_1_Invoke_m2757911370_gshared (ExporterFunc_1_t3480820528 * __this, Vector4_t2243707581  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ExporterFunc_1_Invoke_m2757911370((ExporterFunc_1_t3480820528 *)__this->get_prev_9(),___obj0, ___writer1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0, ___writer1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.Vector4>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t ExporterFunc_1_BeginInvoke_m2178510951_MetadataUsageId;
extern "C"  Il2CppObject * ExporterFunc_1_BeginInvoke_m2178510951_gshared (ExporterFunc_1_t3480820528 * __this, Vector4_t2243707581  ___obj0, JsonWriter_t1927598499 * ___writer1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExporterFunc_1_BeginInvoke_m2178510951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	__d_args[1] = ___writer1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void LitJson.ExporterFunc`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void ExporterFunc_1_EndInvoke_m3097667352_gshared (ExporterFunc_1_t3480820528 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LitJson.FactoryFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void FactoryFunc_1__ctor_m1059335099_gshared (FactoryFunc_1_t741989655 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T LitJson.FactoryFunc`1<System.Object>::Invoke()
extern "C"  Il2CppObject * FactoryFunc_1_Invoke_m2225877924_gshared (FactoryFunc_1_t741989655 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		FactoryFunc_1_Invoke_m2225877924((FactoryFunc_1_t741989655 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.FactoryFunc`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FactoryFunc_1_BeginInvoke_m4292257436_gshared (FactoryFunc_1_t741989655 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T LitJson.FactoryFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * FactoryFunc_1_EndInvoke_m3177408120_gshared (FactoryFunc_1_t741989655 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.ImporterFunc`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ImporterFunc_2__ctor_m2910045588_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::Invoke(TJson)
extern "C"  Il2CppObject * ImporterFunc_2_Invoke_m3373032134_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ImporterFunc_2_Invoke_m3373032134((ImporterFunc_2_t2548729379 *)__this->get_prev_9(),___input0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___input0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___input0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult LitJson.ImporterFunc`2<System.Object,System.Object>::BeginInvoke(TJson,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ImporterFunc_2_BeginInvoke_m2065437887_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___input0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___input0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue LitJson.ImporterFunc`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ImporterFunc_2_EndInvoke_m2375822358_gshared (ImporterFunc_2_t2548729379 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m650532481_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m900643521_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3926562242 * L_0 = (ExporterFunc_1_t3926562242 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3926562242 *)L_0);
		((  void (*) (ExporterFunc_1_t3926562242 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3926562242 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Bounds>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m1262093705_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t4440352 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Bounds>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m4237185177_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t4440352 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t4270476650 * L_0 = (ExporterFunc_1_t4270476650 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t4270476650 *)L_0);
		((  void (*) (ExporterFunc_1_t4270476650 *, Bounds_t3033363703 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t4270476650 *)L_0, (Bounds_t3033363703 )((*(Bounds_t3033363703 *)((Bounds_t3033363703 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m2422376385_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3286436020 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m3306265025_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3286436020 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3257505022 * L_0 = (ExporterFunc_1_t3257505022 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3257505022 *)L_0);
		((  void (*) (ExporterFunc_1_t3257505022 *, Color_t2020392075 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3257505022 *)L_0, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color32>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m1808529390_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t2140561463 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Color32>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m3039318324_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t2140561463 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t2111630465 * L_0 = (ExporterFunc_1_t2111630465 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t2111630465 *)L_0);
		((  void (*) (ExporterFunc_1_t2111630465 *, Color32_t874517518 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t2111630465 *)L_0, (Color32_t874517518 )((*(Color32_t874517518 *)((Color32_t874517518 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m3738270818_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t1001150567 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Quaternion>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m914713760_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t1001150567 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t972219569 * L_0 = (ExporterFunc_1_t972219569 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t972219569 *)L_0);
		((  void (*) (ExporterFunc_1_t972219569 *, Quaternion_t4030073918 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t972219569 *)L_0, (Quaternion_t4030073918 )((*(Quaternion_t4030073918 *)((Quaternion_t4030073918 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Rect>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m1085739924_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t652832275 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Rect>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m256534894_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t652832275 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t623901277 * L_0 = (ExporterFunc_1_t623901277 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t623901277 *)L_0);
		((  void (*) (ExporterFunc_1_t623901277 *, Rect_t3681755626 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t623901277 *)L_0, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m1202330051_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751524 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector2>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m133353215_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751524 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3480820526 * L_0 = (ExporterFunc_1_t3480820526 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3480820526 *)L_0);
		((  void (*) (ExporterFunc_1_t3480820526 *, Vector2_t2243707579 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3480820526 *)L_0, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m405354952_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751525 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector3>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m3735488858_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751525 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3480820527 * L_0 = (ExporterFunc_1_t3480820527 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3480820527 *)L_0);
		((  void (*) (ExporterFunc_1_t3480820527 *, Vector3_t2243707580 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3480820527 *)L_0, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector4>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m4090642361_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751526 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<UnityEngine.Vector4>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m2161913289_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3509751526 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method)
{
	{
		ExporterFunc_1_t3480820528 * L_0 = (ExporterFunc_1_t3480820528 *)__this->get_exporter_0();
		Il2CppObject * L_1 = ___obj0;
		JsonWriter_t1927598499 * L_2 = ___writer1;
		NullCheck((ExporterFunc_1_t3480820528 *)L_0);
		((  void (*) (ExporterFunc_1_t3480820528 *, Vector4_t2243707581 , JsonWriter_t1927598499 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ExporterFunc_1_t3480820528 *)L_0, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))))), (JsonWriter_t1927598499 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>::.ctor()
extern "C"  void U3CRegisterFactoryU3Ec__AnonStorey1D_1__ctor_m1595076882_gshared (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>::<>m__34()
extern "C"  Il2CppObject * U3CRegisterFactoryU3Ec__AnonStorey1D_1_U3CU3Em__34_m1776693373_gshared (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 * __this, const MethodInfo* method)
{
	{
		FactoryFunc_1_t741989655 * L_0 = (FactoryFunc_1_t741989655 *)__this->get_factory_0();
		NullCheck((FactoryFunc_1_t741989655 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (FactoryFunc_1_t741989655 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((FactoryFunc_1_t741989655 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_1;
	}
}
// System.Void LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CRegisterImporterU3Ec__AnonStorey1C_2__ctor_m3995719360_gshared (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object LitJson.JsonMapper/<RegisterImporter>c__AnonStorey1C`2<System.Object,System.Object>::<>m__33(System.Object)
extern "C"  Il2CppObject * U3CRegisterImporterU3Ec__AnonStorey1C_2_U3CU3Em__33_m2805572642_gshared (U3CRegisterImporterU3Ec__AnonStorey1C_2_t2585850632 * __this, Il2CppObject * ___input0, const MethodInfo* method)
{
	{
		ImporterFunc_2_t2548729379 * L_0 = (ImporterFunc_2_t2548729379 *)__this->get_importer_0();
		Il2CppObject * L_1 = ___input0;
		NullCheck((ImporterFunc_2_t2548729379 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ImporterFunc_2_t2548729379 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((ImporterFunc_2_t2548729379 *)L_0, (Il2CppObject *)((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2__ctor_m2422343709_gshared (ThreadSafeDictionary_2_t1463927430 * __this, ThreadSafeDictionaryValueFactory_2_t958855109 * ___valueFactory0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2__ctor_m2422343709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m2551263788(L_0, /*hidden argument*/NULL);
		__this->set__lock_0(L_0);
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_1 = ___valueFactory0;
		__this->set__valueFactory_1(L_1);
		return;
	}
}
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m1443063812_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		return (Il2CppObject *)L_3;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_Get_m1259355240_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_2;
	}

IL_0013:
	{
		Dictionary_2_t2281509423 * L_3 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		Il2CppObject * L_4 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_3);
		bool L_5 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_3, (Il2CppObject *)L_4, (Il2CppObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		if (L_5)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_6 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		return L_7;
	}

IL_002e:
	{
		Il2CppObject * L_8 = V_0;
		return L_8;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_AddValue_m2068149316_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Dictionary_2_t2281509423 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ThreadSafeDictionaryValueFactory_2_t958855109 * L_0 = (ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get__valueFactory_1();
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionaryValueFactory_2_t958855109 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((ThreadSafeDictionaryValueFactory_2_t958855109 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		V_0 = (Il2CppObject *)L_2;
		Il2CppObject * L_3 = (Il2CppObject *)__this->get__lock_0();
		V_1 = (Il2CppObject *)L_3;
		Il2CppObject * L_4 = V_1;
		Monitor_Enter_m2136705809(NULL /*static, unused*/, (Il2CppObject *)L_4, /*hidden argument*/NULL);
	}

IL_001a:
	try
	{ // begin try (depth: 1)
		{
			Dictionary_2_t2281509423 * L_5 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			if (L_5)
			{
				goto IL_0042;
			}
		}

IL_0025:
		{
			Dictionary_2_t2281509423 * L_6 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)(L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			__this->set__dictionary_2(L_6);
			Dictionary_2_t2281509423 * L_7 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_8 = ___key0;
			Il2CppObject * L_9 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_7);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_7, (Il2CppObject *)L_8, (Il2CppObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			goto IL_0078;
		}

IL_0042:
		{
			Dictionary_2_t2281509423 * L_10 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Il2CppObject * L_11 = ___key0;
			NullCheck((Dictionary_2_t2281509423 *)L_10);
			bool L_12 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject **, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((Dictionary_2_t2281509423 *)L_10, (Il2CppObject *)L_11, (Il2CppObject **)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
			if (!L_12)
			{
				goto IL_005d;
			}
		}

IL_0055:
		{
			Il2CppObject * L_13 = V_2;
			V_4 = (Il2CppObject *)L_13;
			IL2CPP_LEAVE(0x86, FINALLY_007d);
		}

IL_005d:
		{
			Dictionary_2_t2281509423 * L_14 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
			Dictionary_2_t2281509423 * L_15 = (Dictionary_2_t2281509423 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5));
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_15, (Il2CppObject*)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
			V_3 = (Dictionary_2_t2281509423 *)L_15;
			Dictionary_2_t2281509423 * L_16 = V_3;
			Il2CppObject * L_17 = ___key0;
			Il2CppObject * L_18 = V_0;
			NullCheck((Dictionary_2_t2281509423 *)L_16);
			((  void (*) (Dictionary_2_t2281509423 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((Dictionary_2_t2281509423 *)L_16, (Il2CppObject *)L_17, (Il2CppObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
			Dictionary_2_t2281509423 * L_19 = V_3;
			__this->set__dictionary_2(L_19);
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x84, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		Il2CppObject * L_20 = V_1;
		Monitor_Exit_m2677760297(NULL /*static, unused*/, (Il2CppObject *)L_20, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(125)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0084:
	{
		Il2CppObject * L_21 = V_0;
		return L_21;
	}

IL_0086:
	{
		Il2CppObject * L_22 = V_4;
		return L_22;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m2282389750_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m2282389750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::ContainsKey(TKey)
extern "C"  bool ThreadSafeDictionary_2_ContainsKey_m379210650_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		Il2CppObject * L_1 = ___key0;
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t2281509423 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((Dictionary_2_t2281509423 *)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_2;
	}
}
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Keys_m2226270717_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		KeyCollection_t470039898 * L_1 = ((  KeyCollection_t470039898 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_1;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m2158894434_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m2158894434_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m2158894434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool ThreadSafeDictionary_2_TryGetValue_m620057635_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject ** ___value1, const MethodInfo* method)
{
	{
		Il2CppObject ** L_0 = ___value1;
		Il2CppObject * L_1 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		(*(Il2CppObject **)L_0) = L_2;
		Il2CppCodeGenWriteBarrier((Il2CppObject **)L_0, L_2);
		return (bool)1;
	}
}
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_get_Values_m2761852509_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		ValueCollection_t984569266 * L_1 = ((  ValueCollection_t984569266 * (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		return L_1;
	}
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionary_2_get_Item_m2261720138_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		NullCheck((ThreadSafeDictionary_2_t1463927430 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (ThreadSafeDictionary_2_t1463927430 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13)->methodPointer)((ThreadSafeDictionary_2_t1463927430 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_1;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_set_Item_m2513745515_gshared (ThreadSafeDictionary_2_t1463927430 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_set_Item_m2513745515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Add_m1306324747_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Add_m1306324747_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_Clear_m3155495606_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Clear_m3155495606_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Contains_m1533726473_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Contains_m1533726473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId;
extern "C"  void ThreadSafeDictionary_2_CopyTo_m1781093487_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2U5BU5D_t2854920344* ___array0, int32_t ___arrayIndex1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_CopyTo_m1781093487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C"  int32_t ThreadSafeDictionary_2_get_Count_m3265115815_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		return L_1;
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_get_IsReadOnly_m1417152376_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern Il2CppClass* NotImplementedException_t2785117854_il2cpp_TypeInfo_var;
extern const uint32_t ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId;
extern "C"  bool ThreadSafeDictionary_2_Remove_m1651487354_gshared (ThreadSafeDictionary_2_t1463927430 * __this, KeyValuePair_2_t38854645  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ThreadSafeDictionary_2_Remove_m1651487354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t2785117854 * L_0 = (NotImplementedException_t2785117854 *)il2cpp_codegen_object_new(NotImplementedException_t2785117854_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m808189835(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ThreadSafeDictionary_2_GetEnumerator_m527450770_gshared (ThreadSafeDictionary_2_t1463927430 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t2281509423 * L_0 = (Dictionary_2_t2281509423 *)__this->get__dictionary_2();
		NullCheck((Dictionary_2_t2281509423 *)L_0);
		Enumerator_t3601534125  L_1 = ((  Enumerator_t3601534125  (*) (Dictionary_2_t2281509423 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((Dictionary_2_t2281509423 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		Enumerator_t3601534125  L_2 = L_1;
		Il2CppObject * L_3 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 1), &L_2);
		return (Il2CppObject*)L_3;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void ThreadSafeDictionaryValueFactory_2__ctor_m3821979982_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::Invoke(TKey)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ThreadSafeDictionaryValueFactory_2_Invoke_m1040912419((ThreadSafeDictionaryValueFactory_2_t958855109 *)__this->get_prev_9(),___key0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ___key0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___key0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_BeginInvoke_m2175992264_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___key0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___key0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * ThreadSafeDictionaryValueFactory_2_EndInvoke_m235081874_gshared (ThreadSafeDictionaryValueFactory_2_t958855109 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::.ctor()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1__ctor_m1814300075_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m127510910_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_10();
		return L_0;
	}
}
// System.Object SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m2116757585_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_10();
		return L_0;
	}
}
// System.Collections.IEnumerator SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1832336732_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	{
		NullCheck((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964834083_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_9();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * L_2 = (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)L_2;
		U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * L_3 = V_0;
		SQLiteCommand_t2935685145 * L_4 = (SQLiteCommand_t2935685145 *)__this->get_U3CU3Ef__this_12();
		NullCheck(L_3);
		L_3->set_U3CU3Ef__this_12(L_4);
		U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * L_5 = V_0;
		TableMapping_t3898710812 * L_6 = (TableMapping_t3898710812 *)__this->get_U3CU24U3Emap_11();
		NullCheck(L_5);
		L_5->set_map_4(L_6);
		U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::MoveNext()
extern Il2CppClass* ColumnU5BU5D_t1579265676_il2cpp_TypeInfo_var;
extern const uint32_t U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421_MetadataUsageId;
extern "C"  bool U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_9();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_9((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_01f2;
	}

IL_0023:
	{
		SQLiteCommand_t2935685145 * L_2 = (SQLiteCommand_t2935685145 *)__this->get_U3CU3Ef__this_12();
		NullCheck(L_2);
		SQLiteConnection_t3529499386 * L_3 = (SQLiteConnection_t3529499386 *)L_2->get__conn_0();
		NullCheck((SQLiteConnection_t3529499386 *)L_3);
		bool L_4 = SQLiteConnection_get_Trace_m699095851((SQLiteConnection_t3529499386 *)L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}

IL_0038:
	{
		SQLiteCommand_t2935685145 * L_5 = (SQLiteCommand_t2935685145 *)__this->get_U3CU3Ef__this_12();
		NullCheck((SQLiteCommand_t2935685145 *)L_5);
		IntPtr_t L_6 = SQLiteCommand_Prepare_m1554312292((SQLiteCommand_t2935685145 *)L_5, /*hidden argument*/NULL);
		__this->set_U3CstmtU3E__0_0(L_6);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_004c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_7 = V_0;
			if (((int32_t)((int32_t)L_7-(int32_t)1)) == 0)
			{
				goto IL_01c9;
			}
		}

IL_0058:
		{
			IntPtr_t L_8 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
			int32_t L_9 = SQLite3_ColumnCount_m3918418219(NULL /*static, unused*/, (IntPtr_t)L_8, /*hidden argument*/NULL);
			__this->set_U3CcolsU3E__1_1(((ColumnU5BU5D_t1579265676*)SZArrayNew(ColumnU5BU5D_t1579265676_il2cpp_TypeInfo_var, (uint32_t)L_9)));
			__this->set_U3CiU3E__2_2(0);
			goto IL_00bd;
		}

IL_007a:
		{
			IntPtr_t L_10 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
			int32_t L_11 = (int32_t)__this->get_U3CiU3E__2_2();
			String_t* L_12 = SQLite3_ColumnName16_m4083644(NULL /*static, unused*/, (IntPtr_t)L_10, (int32_t)L_11, /*hidden argument*/NULL);
			__this->set_U3CnameU3E__3_3(L_12);
			ColumnU5BU5D_t1579265676* L_13 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			int32_t L_14 = (int32_t)__this->get_U3CiU3E__2_2();
			TableMapping_t3898710812 * L_15 = (TableMapping_t3898710812 *)__this->get_map_4();
			String_t* L_16 = (String_t*)__this->get_U3CnameU3E__3_3();
			NullCheck((TableMapping_t3898710812 *)L_15);
			Column_t441055761 * L_17 = TableMapping_FindColumn_m1663565178((TableMapping_t3898710812 *)L_15, (String_t*)L_16, /*hidden argument*/NULL);
			NullCheck(L_13);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
			ArrayElementTypeCheck (L_13, L_17);
			(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Column_t441055761 *)L_17);
			int32_t L_18 = (int32_t)__this->get_U3CiU3E__2_2();
			__this->set_U3CiU3E__2_2(((int32_t)((int32_t)L_18+(int32_t)1)));
		}

IL_00bd:
		{
			int32_t L_19 = (int32_t)__this->get_U3CiU3E__2_2();
			ColumnU5BU5D_t1579265676* L_20 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			NullCheck(L_20);
			if ((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length)))))))
			{
				goto IL_007a;
			}
		}

IL_00d0:
		{
			goto IL_01c9;
		}

IL_00d5:
		{
			TableMapping_t3898710812 * L_21 = (TableMapping_t3898710812 *)__this->get_map_4();
			NullCheck((TableMapping_t3898710812 *)L_21);
			Type_t * L_22 = TableMapping_get_MappedType_m2186451513((TableMapping_t3898710812 *)L_21, /*hidden argument*/NULL);
			Il2CppObject * L_23 = Activator_CreateInstance_m383294261(NULL /*static, unused*/, (Type_t *)L_22, /*hidden argument*/NULL);
			__this->set_U3CobjU3E__4_5(L_23);
			__this->set_U3CiU3E__5_6(0);
			goto IL_0186;
		}

IL_00f7:
		{
			ColumnU5BU5D_t1579265676* L_24 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			int32_t L_25 = (int32_t)__this->get_U3CiU3E__5_6();
			NullCheck(L_24);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_25);
			int32_t L_26 = L_25;
			Column_t441055761 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
			if (L_27)
			{
				goto IL_010e;
			}
		}

IL_0109:
		{
			goto IL_0178;
		}

IL_010e:
		{
			IntPtr_t L_28 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
			int32_t L_29 = (int32_t)__this->get_U3CiU3E__5_6();
			int32_t L_30 = SQLite3_ColumnType_m566938654(NULL /*static, unused*/, (IntPtr_t)L_28, (int32_t)L_29, /*hidden argument*/NULL);
			__this->set_U3CcolTypeU3E__6_7(L_30);
			SQLiteCommand_t2935685145 * L_31 = (SQLiteCommand_t2935685145 *)__this->get_U3CU3Ef__this_12();
			IntPtr_t L_32 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
			int32_t L_33 = (int32_t)__this->get_U3CiU3E__5_6();
			int32_t L_34 = (int32_t)__this->get_U3CcolTypeU3E__6_7();
			ColumnU5BU5D_t1579265676* L_35 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			int32_t L_36 = (int32_t)__this->get_U3CiU3E__5_6();
			NullCheck(L_35);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
			int32_t L_37 = L_36;
			Column_t441055761 * L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
			NullCheck((Column_t441055761 *)L_38);
			Type_t * L_39 = Column_get_ColumnType_m951700143((Column_t441055761 *)L_38, /*hidden argument*/NULL);
			NullCheck((SQLiteCommand_t2935685145 *)L_31);
			Il2CppObject * L_40 = SQLiteCommand_ReadCol_m1290746203((SQLiteCommand_t2935685145 *)L_31, (IntPtr_t)L_32, (int32_t)L_33, (int32_t)L_34, (Type_t *)L_39, /*hidden argument*/NULL);
			__this->set_U3CvalU3E__7_8(L_40);
			ColumnU5BU5D_t1579265676* L_41 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			int32_t L_42 = (int32_t)__this->get_U3CiU3E__5_6();
			NullCheck(L_41);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
			int32_t L_43 = L_42;
			Column_t441055761 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
			Il2CppObject * L_45 = (Il2CppObject *)__this->get_U3CobjU3E__4_5();
			Il2CppObject * L_46 = (Il2CppObject *)__this->get_U3CvalU3E__7_8();
			NullCheck((Column_t441055761 *)L_44);
			Column_SetValue_m2549925353((Column_t441055761 *)L_44, (Il2CppObject *)L_45, (Il2CppObject *)L_46, /*hidden argument*/NULL);
		}

IL_0178:
		{
			int32_t L_47 = (int32_t)__this->get_U3CiU3E__5_6();
			__this->set_U3CiU3E__5_6(((int32_t)((int32_t)L_47+(int32_t)1)));
		}

IL_0186:
		{
			int32_t L_48 = (int32_t)__this->get_U3CiU3E__5_6();
			ColumnU5BU5D_t1579265676* L_49 = (ColumnU5BU5D_t1579265676*)__this->get_U3CcolsU3E__1_1();
			NullCheck(L_49);
			if ((((int32_t)L_48) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length)))))))
			{
				goto IL_00f7;
			}
		}

IL_0199:
		{
			SQLiteCommand_t2935685145 * L_50 = (SQLiteCommand_t2935685145 *)__this->get_U3CU3Ef__this_12();
			Il2CppObject * L_51 = (Il2CppObject *)__this->get_U3CobjU3E__4_5();
			NullCheck((SQLiteCommand_t2935685145 *)L_50);
			VirtActionInvoker1< Il2CppObject * >::Invoke(4 /* System.Void SQLite4Unity3d.SQLiteCommand::OnInstanceCreated(System.Object) */, (SQLiteCommand_t2935685145 *)L_50, (Il2CppObject *)L_51);
			Il2CppObject * L_52 = (Il2CppObject *)__this->get_U3CobjU3E__4_5();
			__this->set_U24current_10(((Il2CppObject *)Castclass(L_52, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0))));
			__this->set_U24PC_9(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0x1F4, FINALLY_01e0);
		}

IL_01c9:
		{
			IntPtr_t L_53 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
			int32_t L_54 = SQLite3_Step_m2897001430(NULL /*static, unused*/, (IntPtr_t)L_53, /*hidden argument*/NULL);
			if ((((int32_t)L_54) == ((int32_t)((int32_t)100))))
			{
				goto IL_00d5;
			}
		}

IL_01db:
		{
			IL2CPP_LEAVE(0x1EB, FINALLY_01e0);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_01e0;
	}

FINALLY_01e0:
	{ // begin finally (depth: 1)
		{
			bool L_55 = V_1;
			if (!L_55)
			{
				goto IL_01e4;
			}
		}

IL_01e3:
		{
			IL2CPP_END_FINALLY(480)
		}

IL_01e4:
		{
			NullCheck((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this);
			((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
			IL2CPP_END_FINALLY(480)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(480)
	{
		IL2CPP_JUMP_TBL(0x1F4, IL_01f4)
		IL2CPP_JUMP_TBL(0x1EB, IL_01eb)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_01eb:
	{
		__this->set_U24PC_9((-1));
	}

IL_01f2:
	{
		return (bool)0;
	}

IL_01f4:
	{
		return (bool)1;
	}
	// Dead block : IL_01f6: ldloc.2
}
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::Dispose()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_Dispose_m497419912_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_9();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_9((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_002d;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_002d;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x2D, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		NullCheck((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this);
		((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x2D, IL_002d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_002d:
	{
		return;
	}
}
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858_MetadataUsageId;
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::<>__Finally0()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_U3CU3E__Finally0_m3335794584_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (IntPtr_t)__this->get_U3CstmtU3E__0_0();
		SQLite3_Finalize_m1083398254(NULL /*static, unused*/, (IntPtr_t)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::.ctor()
extern "C"  void CompileResult__ctor_m3789497005_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::get_CommandText()
extern "C"  String_t* CompileResult_get_CommandText_m2764066139_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (String_t*)__this->get_U3CCommandTextU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::set_CommandText(System.String)
extern "C"  void CompileResult_set_CommandText_m1496493232_gshared (CompileResult_t4163790932 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCommandTextU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Object SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::get_Value()
extern "C"  Il2CppObject * CompileResult_get_Value_m2706335512_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U3CValueU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::set_Value(System.Object)
extern "C"  void CompileResult_set_Value_m420182477_gshared (CompileResult_t4163790932 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_U3CValueU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::.ctor(SQLite4Unity3d.SQLiteConnection,SQLite4Unity3d.TableMapping)
extern "C"  void TableQuery_1__ctor_m2525682756_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___conn0, TableMapping_t3898710812 * ___table1, const MethodInfo* method)
{
	{
		NullCheck((BaseTableQuery_t3770317269 *)__this);
		BaseTableQuery__ctor_m2606818376((BaseTableQuery_t3770317269 *)__this, /*hidden argument*/NULL);
		SQLiteConnection_t3529499386 * L_0 = ___conn0;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		((  void (*) (TableQuery_1_t4026329353 *, SQLiteConnection_t3529499386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TableQuery_1_t4026329353 *)__this, (SQLiteConnection_t3529499386 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		TableMapping_t3898710812 * L_1 = ___table1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		((  void (*) (TableQuery_1_t4026329353 *, TableMapping_t3898710812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((TableQuery_1_t4026329353 *)__this, (TableMapping_t3898710812 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::.ctor(SQLite4Unity3d.SQLiteConnection)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TableQuery_1__ctor_m821176910_MetadataUsageId;
extern "C"  void TableQuery_1__ctor_m821176910_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___conn0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1__ctor_m821176910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((BaseTableQuery_t3770317269 *)__this);
		BaseTableQuery__ctor_m2606818376((BaseTableQuery_t3770317269 *)__this, /*hidden argument*/NULL);
		SQLiteConnection_t3529499386 * L_0 = ___conn0;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		((  void (*) (TableQuery_1_t4026329353 *, SQLiteConnection_t3529499386 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((TableQuery_1_t4026329353 *)__this, (SQLiteConnection_t3529499386 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		NullCheck((TableQuery_1_t4026329353 *)__this);
		SQLiteConnection_t3529499386 * L_1 = ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 3)), /*hidden argument*/NULL);
		NullCheck((SQLiteConnection_t3529499386 *)L_1);
		TableMapping_t3898710812 * L_3 = SQLiteConnection_GetMapping_m822344580((SQLiteConnection_t3529499386 *)L_1, (Type_t *)L_2, (int32_t)0, /*hidden argument*/NULL);
		NullCheck((TableQuery_1_t4026329353 *)__this);
		((  void (*) (TableQuery_1_t4026329353 *, TableMapping_t3898710812 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((TableQuery_1_t4026329353 *)__this, (TableMapping_t3898710812 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Collections.IEnumerator SQLite4Unity3d.TableQuery`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_0;
	}
}
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.TableQuery`1<System.Object>::get_Connection()
extern "C"  SQLiteConnection_t3529499386 * TableQuery_1_get_Connection_m2018438234_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	{
		SQLiteConnection_t3529499386 * L_0 = (SQLiteConnection_t3529499386 *)__this->get_U3CConnectionU3Ek__BackingField_11();
		return L_0;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::set_Connection(SQLite4Unity3d.SQLiteConnection)
extern "C"  void TableQuery_1_set_Connection_m3046933261_gshared (TableQuery_1_t4026329353 * __this, SQLiteConnection_t3529499386 * ___value0, const MethodInfo* method)
{
	{
		SQLiteConnection_t3529499386 * L_0 = ___value0;
		__this->set_U3CConnectionU3Ek__BackingField_11(L_0);
		return;
	}
}
// SQLite4Unity3d.TableMapping SQLite4Unity3d.TableQuery`1<System.Object>::get_Table()
extern "C"  TableMapping_t3898710812 * TableQuery_1_get_Table_m2172867230_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	{
		TableMapping_t3898710812 * L_0 = (TableMapping_t3898710812 *)__this->get_U3CTableU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::set_Table(SQLite4Unity3d.TableMapping)
extern "C"  void TableQuery_1_set_Table_m3580780791_gshared (TableQuery_1_t4026329353 * __this, TableMapping_t3898710812 * ___value0, const MethodInfo* method)
{
	{
		TableMapping_t3898710812 * L_0 = ___value0;
		__this->set_U3CTableU3Ek__BackingField_12(L_0);
		return;
	}
}
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Where(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral375889354;
extern const uint32_t TableQuery_1_Where_m4178518972_MetadataUsageId;
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Where_m4178518972_gshared (TableQuery_1_t4026329353 * __this, Expression_1_t4002038744 * ___predExpr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_Where_m4178518972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LambdaExpression_t2811402413 * V_0 = NULL;
	Expression_t114864668 * V_1 = NULL;
	TableQuery_1_t4026329353 * V_2 = NULL;
	{
		Expression_1_t4002038744 * L_0 = ___predExpr0;
		NullCheck((Expression_t114864668 *)L_0);
		int32_t L_1 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_0026;
		}
	}
	{
		Expression_1_t4002038744 * L_2 = ___predExpr0;
		V_0 = (LambdaExpression_t2811402413 *)L_2;
		LambdaExpression_t2811402413 * L_3 = V_0;
		NullCheck((LambdaExpression_t2811402413 *)L_3);
		Expression_t114864668 * L_4 = LambdaExpression_get_Body_m3261107648((LambdaExpression_t2811402413 *)L_3, /*hidden argument*/NULL);
		V_1 = (Expression_t114864668 *)L_4;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_5 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_2 = (TableQuery_1_t4026329353 *)L_5;
		TableQuery_1_t4026329353 * L_6 = V_2;
		Expression_t114864668 * L_7 = V_1;
		NullCheck((TableQuery_1_t4026329353 *)L_6);
		((  void (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)((TableQuery_1_t4026329353 *)L_6, (Expression_t114864668 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		TableQuery_1_t4026329353 * L_8 = V_2;
		return L_8;
	}

IL_0026:
	{
		NotSupportedException_t1793819818 * L_9 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_9, (String_t*)_stringLiteral375889354, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}
}
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Take(System.Int32)
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern const uint32_t TableQuery_1_Take_m2215285898_MetadataUsageId;
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Take_m2215285898_gshared (TableQuery_1_t4026329353 * __this, int32_t ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_Take_m2215285898_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TableQuery_1_t4026329353 * V_0 = NULL;
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_0 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (TableQuery_1_t4026329353 *)L_0;
		TableQuery_1_t4026329353 * L_1 = V_0;
		int32_t L_2 = ___n0;
		Nullable_1_t334943763  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m255206972(&L_3, (int32_t)L_2, /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		NullCheck(L_1);
		L_1->set__limit_2(L_3);
		TableQuery_1_t4026329353 * L_4 = V_0;
		return L_4;
	}
}
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Skip(System.Int32)
extern const MethodInfo* Nullable_1__ctor_m255206972_MethodInfo_var;
extern const uint32_t TableQuery_1_Skip_m2335687758_MetadataUsageId;
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Skip_m2335687758_gshared (TableQuery_1_t4026329353 * __this, int32_t ___n0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_Skip_m2335687758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TableQuery_1_t4026329353 * V_0 = NULL;
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_0 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (TableQuery_1_t4026329353 *)L_0;
		TableQuery_1_t4026329353 * L_1 = V_0;
		int32_t L_2 = ___n0;
		Nullable_1_t334943763  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m255206972(&L_3, (int32_t)L_2, /*hidden argument*/Nullable_1__ctor_m255206972_MethodInfo_var);
		NullCheck(L_1);
		L_1->set__offset_3(L_3);
		TableQuery_1_t4026329353 * L_4 = V_0;
		return L_4;
	}
}
// T SQLite4Unity3d.TableQuery`1<System.Object>::ElementAt(System.Int32)
extern "C"  Il2CppObject * TableQuery_1_ElementAt_m4176579987_gshared (TableQuery_1_t4026329353 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_1 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)((TableQuery_1_t4026329353 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		NullCheck((TableQuery_1_t4026329353 *)L_1);
		TableQuery_1_t4026329353 * L_2 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((TableQuery_1_t4026329353 *)L_1, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		NullCheck((TableQuery_1_t4026329353 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((TableQuery_1_t4026329353 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
		return L_3;
	}
}
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::Deferred()
extern "C"  TableQuery_1_t4026329353 * TableQuery_1_Deferred_m2927110549_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	TableQuery_1_t4026329353 * V_0 = NULL;
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_0 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		V_0 = (TableQuery_1_t4026329353 *)L_0;
		TableQuery_1_t4026329353 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set__deferred_10((bool)1);
		TableQuery_1_t4026329353 * L_2 = V_0;
		return L_2;
	}
}
// System.Void SQLite4Unity3d.TableQuery`1<System.Object>::AddWhere(System.Linq.Expressions.Expression)
extern "C"  void TableQuery_1_AddWhere_m752477770_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___pred0, const MethodInfo* method)
{
	{
		Expression_t114864668 * L_0 = (Expression_t114864668 *)__this->get__where_0();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		Expression_t114864668 * L_1 = ___pred0;
		__this->set__where_0(L_1);
		goto IL_0029;
	}

IL_0017:
	{
		Expression_t114864668 * L_2 = (Expression_t114864668 *)__this->get__where_0();
		Expression_t114864668 * L_3 = ___pred0;
		BinaryExpression_t2159924157 * L_4 = Expression_AndAlso_m86695741(NULL /*static, unused*/, (Expression_t114864668 *)L_2, (Expression_t114864668 *)L_3, /*hidden argument*/NULL);
		__this->set__where_0(L_4);
	}

IL_0029:
	{
		return;
	}
}
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.TableQuery`1<System.Object>::GenerateCommand(System.String)
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2058570427_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t4282907944_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m365405030_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m2272141160_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m2746132342_MethodInfo_var;
extern const MethodInfo* Enumerable_Select_TisOrdering_t1038862794_TisString_t_m2214280582_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_HasValue_m4233035231_MethodInfo_var;
extern const MethodInfo* Nullable_1_get_Value_m3953935279_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3072408725_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral193538032;
extern Il2CppCodeGenString* _stringLiteral4130517934;
extern Il2CppCodeGenString* _stringLiteral1763872980;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral2114025497;
extern Il2CppCodeGenString* _stringLiteral811305474;
extern Il2CppCodeGenString* _stringLiteral3211501729;
extern Il2CppCodeGenString* _stringLiteral4137286549;
extern Il2CppCodeGenString* _stringLiteral147301;
extern Il2CppCodeGenString* _stringLiteral1756701361;
extern const uint32_t TableQuery_1_GenerateCommand_m4193037006_MetadataUsageId;
extern "C"  SQLiteCommand_t2935685145 * TableQuery_1_GenerateCommand_m4193037006_gshared (TableQuery_1_t4026329353 * __this, String_t* ___selectionList0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_GenerateCommand_m4193037006_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t2058570427 * V_1 = NULL;
	CompileResult_t4163790932 * V_2 = NULL;
	String_t* V_3 = NULL;
	List_1_t407983926 * G_B9_0 = NULL;
	String_t* G_B9_1 = NULL;
	List_1_t407983926 * G_B8_0 = NULL;
	String_t* G_B8_1 = NULL;
	{
		BaseTableQuery_t3770317269 * L_0 = (BaseTableQuery_t3770317269 *)__this->get__joinInner_4();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		BaseTableQuery_t3770317269 * L_1 = (BaseTableQuery_t3770317269 *)__this->get__joinOuter_6();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		NotSupportedException_t1793819818 * L_2 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_2, (String_t*)_stringLiteral193538032, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0021:
	{
		StringU5BU5D_t1642385972* L_3 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, _stringLiteral4130517934);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral4130517934);
		StringU5BU5D_t1642385972* L_4 = (StringU5BU5D_t1642385972*)L_3;
		String_t* L_5 = ___selectionList0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 1);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_5);
		StringU5BU5D_t1642385972* L_6 = (StringU5BU5D_t1642385972*)L_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1763872980);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1763872980);
		StringU5BU5D_t1642385972* L_7 = (StringU5BU5D_t1642385972*)L_6;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableMapping_t3898710812 * L_8 = ((  TableMapping_t3898710812 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		NullCheck((TableMapping_t3898710812 *)L_8);
		String_t* L_9 = TableMapping_get_TableName_m467523828((TableMapping_t3898710812 *)L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_9);
		StringU5BU5D_t1642385972* L_10 = (StringU5BU5D_t1642385972*)L_7;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral372029312);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029312);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_10, /*hidden argument*/NULL);
		V_0 = (String_t*)L_11;
		List_1_t2058570427 * L_12 = (List_1_t2058570427 *)il2cpp_codegen_object_new(List_1_t2058570427_il2cpp_TypeInfo_var);
		List_1__ctor_m365405030(L_12, /*hidden argument*/List_1__ctor_m365405030_MethodInfo_var);
		V_1 = (List_1_t2058570427 *)L_12;
		Expression_t114864668 * L_13 = (Expression_t114864668 *)__this->get__where_0();
		if (!L_13)
		{
			goto IL_0088;
		}
	}
	{
		Expression_t114864668 * L_14 = (Expression_t114864668 *)__this->get__where_0();
		List_1_t2058570427 * L_15 = V_1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_16 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_14, (List_1_t2058570427 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_2 = (CompileResult_t4163790932 *)L_16;
		String_t* L_17 = V_0;
		CompileResult_t4163790932 * L_18 = V_2;
		NullCheck((CompileResult_t4163790932 *)L_18);
		String_t* L_19 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_17, (String_t*)_stringLiteral2114025497, (String_t*)L_19, /*hidden argument*/NULL);
		V_0 = (String_t*)L_20;
	}

IL_0088:
	{
		List_1_t407983926 * L_21 = (List_1_t407983926 *)__this->get__orderBys_1();
		if (!L_21)
		{
			goto IL_00e9;
		}
	}
	{
		List_1_t407983926 * L_22 = (List_1_t407983926 *)__this->get__orderBys_1();
		NullCheck((List_1_t407983926 *)L_22);
		int32_t L_23 = List_1_get_Count_m2272141160((List_1_t407983926 *)L_22, /*hidden argument*/List_1_get_Count_m2272141160_MethodInfo_var);
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_00e9;
		}
	}
	{
		List_1_t407983926 * L_24 = (List_1_t407983926 *)__this->get__orderBys_1();
		Func_2_t4282907944 * L_25 = ((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_U3CU3Ef__amU24cacheD_13();
		G_B8_0 = L_24;
		G_B8_1 = _stringLiteral811305474;
		if (L_25)
		{
			G_B9_0 = L_24;
			G_B9_1 = _stringLiteral811305474;
			goto IL_00c7;
		}
	}
	{
		IntPtr_t L_26;
		L_26.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 14));
		Func_2_t4282907944 * L_27 = (Func_2_t4282907944 *)il2cpp_codegen_object_new(Func_2_t4282907944_il2cpp_TypeInfo_var);
		Func_2__ctor_m2746132342(L_27, (Il2CppObject *)NULL, (IntPtr_t)L_26, /*hidden argument*/Func_2__ctor_m2746132342_MethodInfo_var);
		((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->set_U3CU3Ef__amU24cacheD_13(L_27);
		G_B9_0 = G_B8_0;
		G_B9_1 = G_B8_1;
	}

IL_00c7:
	{
		Func_2_t4282907944 * L_28 = ((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_U3CU3Ef__amU24cacheD_13();
		Il2CppObject* L_29 = Enumerable_Select_TisOrdering_t1038862794_TisString_t_m2214280582(NULL /*static, unused*/, (Il2CppObject*)G_B9_0, (Func_2_t4282907944 *)L_28, /*hidden argument*/Enumerable_Select_TisOrdering_t1038862794_TisString_t_m2214280582_MethodInfo_var);
		StringU5BU5D_t1642385972* L_30 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, (Il2CppObject*)L_29, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Join_m1966872927(NULL /*static, unused*/, (String_t*)G_B9_1, (StringU5BU5D_t1642385972*)L_30, /*hidden argument*/NULL);
		V_3 = (String_t*)L_31;
		String_t* L_32 = V_0;
		String_t* L_33 = V_3;
		String_t* L_34 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)L_32, (String_t*)_stringLiteral3211501729, (String_t*)L_33, /*hidden argument*/NULL);
		V_0 = (String_t*)L_34;
	}

IL_00e9:
	{
		Nullable_1_t334943763 * L_35 = (Nullable_1_t334943763 *)__this->get_address_of__limit_2();
		bool L_36 = Nullable_1_get_HasValue_m4233035231((Nullable_1_t334943763 *)L_35, /*hidden argument*/Nullable_1_get_HasValue_m4233035231_MethodInfo_var);
		if (!L_36)
		{
			goto IL_0115;
		}
	}
	{
		String_t* L_37 = V_0;
		Nullable_1_t334943763 * L_38 = (Nullable_1_t334943763 *)__this->get_address_of__limit_2();
		int32_t L_39 = Nullable_1_get_Value_m3953935279((Nullable_1_t334943763 *)L_38, /*hidden argument*/Nullable_1_get_Value_m3953935279_MethodInfo_var);
		int32_t L_40 = L_39;
		Il2CppObject * L_41 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_40);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_42 = String_Concat_m2000667605(NULL /*static, unused*/, (Il2CppObject *)L_37, (Il2CppObject *)_stringLiteral4137286549, (Il2CppObject *)L_41, /*hidden argument*/NULL);
		V_0 = (String_t*)L_42;
	}

IL_0115:
	{
		Nullable_1_t334943763 * L_43 = (Nullable_1_t334943763 *)__this->get_address_of__offset_3();
		bool L_44 = Nullable_1_get_HasValue_m4233035231((Nullable_1_t334943763 *)L_43, /*hidden argument*/Nullable_1_get_HasValue_m4233035231_MethodInfo_var);
		if (!L_44)
		{
			goto IL_015d;
		}
	}
	{
		Nullable_1_t334943763 * L_45 = (Nullable_1_t334943763 *)__this->get_address_of__limit_2();
		bool L_46 = Nullable_1_get_HasValue_m4233035231((Nullable_1_t334943763 *)L_45, /*hidden argument*/Nullable_1_get_HasValue_m4233035231_MethodInfo_var);
		if (L_46)
		{
			goto IL_0141;
		}
	}
	{
		String_t* L_47 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_48 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)L_47, (String_t*)_stringLiteral147301, /*hidden argument*/NULL);
		V_0 = (String_t*)L_48;
	}

IL_0141:
	{
		String_t* L_49 = V_0;
		Nullable_1_t334943763 * L_50 = (Nullable_1_t334943763 *)__this->get_address_of__offset_3();
		int32_t L_51 = Nullable_1_get_Value_m3953935279((Nullable_1_t334943763 *)L_50, /*hidden argument*/Nullable_1_get_Value_m3953935279_MethodInfo_var);
		int32_t L_52 = L_51;
		Il2CppObject * L_53 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_52);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_54 = String_Concat_m2000667605(NULL /*static, unused*/, (Il2CppObject *)L_49, (Il2CppObject *)_stringLiteral1756701361, (Il2CppObject *)L_53, /*hidden argument*/NULL);
		V_0 = (String_t*)L_54;
	}

IL_015d:
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		SQLiteConnection_t3529499386 * L_55 = ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 2));
		String_t* L_56 = V_0;
		List_1_t2058570427 * L_57 = V_1;
		NullCheck((List_1_t2058570427 *)L_57);
		ObjectU5BU5D_t3614634134* L_58 = List_1_ToArray_m3072408725((List_1_t2058570427 *)L_57, /*hidden argument*/List_1_ToArray_m3072408725_MethodInfo_var);
		NullCheck((SQLiteConnection_t3529499386 *)L_55);
		SQLiteCommand_t2935685145 * L_59 = SQLiteConnection_CreateCommand_m393862614((SQLiteConnection_t3529499386 *)L_55, (String_t*)L_56, (ObjectU5BU5D_t3614634134*)L_58, /*hidden argument*/NULL);
		return L_59;
	}
}
// SQLite4Unity3d.TableQuery`1/CompileResult<T> SQLite4Unity3d.TableQuery`1<System.Object>::CompileExpr(System.Linq.Expressions.Expression,System.Collections.Generic.List`1<System.Object>)
extern const Il2CppType* String_t_0_0_0_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppClass* BinaryExpression_t2159924157_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* MethodCallExpression_t3367820543_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ConstantExpression_t305952364_il2cpp_TypeInfo_var;
extern Il2CppClass* UnaryExpression_t4253534555_il2cpp_TypeInfo_var;
extern Il2CppClass* MemberExpression_t1790982958_il2cpp_TypeInfo_var;
extern Il2CppClass* PropertyInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* FieldInfo_t_il2cpp_TypeInfo_var;
extern Il2CppClass* MemberTypes_t3343038963_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_t2911409499_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t3975231481_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t1221177846_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* ExpressionType_t1567188220_il2cpp_TypeInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Count_m757747194_MethodInfo_var;
extern const MethodInfo* ReadOnlyCollection_1_get_Item_m2979002709_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* List_1_Add_m567051994_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m4253763168_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m3615096820_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1709822095;
extern Il2CppCodeGenString* _stringLiteral372029331;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern Il2CppCodeGenString* _stringLiteral3457520015;
extern Il2CppCodeGenString* _stringLiteral3199644883;
extern Il2CppCodeGenString* _stringLiteral1789950331;
extern Il2CppCodeGenString* _stringLiteral1010958311;
extern Il2CppCodeGenString* _stringLiteral913036430;
extern Il2CppCodeGenString* _stringLiteral3923541145;
extern Il2CppCodeGenString* _stringLiteral2969948949;
extern Il2CppCodeGenString* _stringLiteral796699147;
extern Il2CppCodeGenString* _stringLiteral2605454916;
extern Il2CppCodeGenString* _stringLiteral3183958472;
extern Il2CppCodeGenString* _stringLiteral1014339031;
extern Il2CppCodeGenString* _stringLiteral2616673141;
extern Il2CppCodeGenString* _stringLiteral689406616;
extern Il2CppCodeGenString* _stringLiteral884689995;
extern Il2CppCodeGenString* _stringLiteral689641025;
extern Il2CppCodeGenString* _stringLiteral639503754;
extern Il2CppCodeGenString* _stringLiteral372029314;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral2244287635;
extern Il2CppCodeGenString* _stringLiteral4206147057;
extern Il2CppCodeGenString* _stringLiteral3516623994;
extern const uint32_t TableQuery_1_CompileExpr_m3818611973_MetadataUsageId;
extern "C"  CompileResult_t4163790932 * TableQuery_1_CompileExpr_m3818611973_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___expr0, List_1_t2058570427 * ___queryArgs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_CompileExpr_m3818611973_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BinaryExpression_t2159924157 * V_0 = NULL;
	CompileResult_t4163790932 * V_1 = NULL;
	CompileResult_t4163790932 * V_2 = NULL;
	String_t* V_3 = NULL;
	MethodCallExpression_t3367820543 * V_4 = NULL;
	CompileResultU5BU5D_t3217057757* V_5 = NULL;
	CompileResult_t4163790932 * V_6 = NULL;
	int32_t V_7 = 0;
	String_t* V_8 = NULL;
	ConstantExpression_t305952364 * V_9 = NULL;
	UnaryExpression_t4253534555 * V_10 = NULL;
	Type_t * V_11 = NULL;
	CompileResult_t4163790932 * V_12 = NULL;
	MemberExpression_t1790982958 * V_13 = NULL;
	String_t* V_14 = NULL;
	Il2CppObject * V_15 = NULL;
	CompileResult_t4163790932 * V_16 = NULL;
	Il2CppObject * V_17 = NULL;
	PropertyInfo_t * V_18 = NULL;
	FieldInfo_t * V_19 = NULL;
	StringBuilder_t1221177846 * V_20 = NULL;
	String_t* V_21 = NULL;
	Il2CppObject * V_22 = NULL;
	Il2CppObject * V_23 = NULL;
	CompileResult_t4163790932 * V_24 = NULL;
	Il2CppObject * V_25 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	CompileResult_t4163790932 * G_B15_0 = NULL;
	CompileResultU5BU5D_t3217057757* G_B46_0 = NULL;
	String_t* G_B46_1 = NULL;
	String_t* G_B46_2 = NULL;
	String_t* G_B46_3 = NULL;
	CompileResultU5BU5D_t3217057757* G_B45_0 = NULL;
	String_t* G_B45_1 = NULL;
	String_t* G_B45_2 = NULL;
	String_t* G_B45_3 = NULL;
	CompileResult_t4163790932 * G_B53_0 = NULL;
	CompileResult_t4163790932 * G_B52_0 = NULL;
	Il2CppObject * G_B54_0 = NULL;
	CompileResult_t4163790932 * G_B54_1 = NULL;
	{
		Expression_t114864668 * L_0 = ___expr0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		NotSupportedException_t1793819818 * L_1 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_1, (String_t*)_stringLiteral1709822095, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Expression_t114864668 * L_2 = ___expr0;
		if (!((BinaryExpression_t2159924157 *)IsInst(L_2, BinaryExpression_t2159924157_il2cpp_TypeInfo_var)))
		{
			goto IL_00f5;
		}
	}
	{
		Expression_t114864668 * L_3 = ___expr0;
		V_0 = (BinaryExpression_t2159924157 *)((BinaryExpression_t2159924157 *)Castclass(L_3, BinaryExpression_t2159924157_il2cpp_TypeInfo_var));
		BinaryExpression_t2159924157 * L_4 = V_0;
		NullCheck((BinaryExpression_t2159924157 *)L_4);
		Expression_t114864668 * L_5 = BinaryExpression_get_Left_m2831265245((BinaryExpression_t2159924157 *)L_4, /*hidden argument*/NULL);
		List_1_t2058570427 * L_6 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_7 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_5, (List_1_t2058570427 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_1 = (CompileResult_t4163790932 *)L_7;
		BinaryExpression_t2159924157 * L_8 = V_0;
		NullCheck((BinaryExpression_t2159924157 *)L_8);
		Expression_t114864668 * L_9 = BinaryExpression_get_Right_m1785810714((BinaryExpression_t2159924157 *)L_8, /*hidden argument*/NULL);
		List_1_t2058570427 * L_10 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_11 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_9, (List_1_t2058570427 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_2 = (CompileResult_t4163790932 *)L_11;
		CompileResult_t4163790932 * L_12 = V_1;
		NullCheck((CompileResult_t4163790932 *)L_12);
		String_t* L_13 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_13, (String_t*)_stringLiteral372029331, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_006d;
		}
	}
	{
		CompileResult_t4163790932 * L_15 = V_1;
		NullCheck((CompileResult_t4163790932 *)L_15);
		Il2CppObject * L_16 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		if (L_16)
		{
			goto IL_006d;
		}
	}
	{
		BinaryExpression_t2159924157 * L_17 = V_0;
		CompileResult_t4163790932 * L_18 = V_2;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		String_t* L_19 = ((  String_t* (*) (TableQuery_1_t4026329353 *, BinaryExpression_t2159924157 *, CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((TableQuery_1_t4026329353 *)__this, (BinaryExpression_t2159924157 *)L_17, (CompileResult_t4163790932 *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_3 = (String_t*)L_19;
		goto IL_00e3;
	}

IL_006d:
	{
		CompileResult_t4163790932 * L_20 = V_2;
		NullCheck((CompileResult_t4163790932 *)L_20);
		String_t* L_21 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_21, (String_t*)_stringLiteral372029331, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009b;
		}
	}
	{
		CompileResult_t4163790932 * L_23 = V_2;
		NullCheck((CompileResult_t4163790932 *)L_23);
		Il2CppObject * L_24 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_23, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		if (L_24)
		{
			goto IL_009b;
		}
	}
	{
		BinaryExpression_t2159924157 * L_25 = V_0;
		CompileResult_t4163790932 * L_26 = V_1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		String_t* L_27 = ((  String_t* (*) (TableQuery_1_t4026329353 *, BinaryExpression_t2159924157 *, CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((TableQuery_1_t4026329353 *)__this, (BinaryExpression_t2159924157 *)L_25, (CompileResult_t4163790932 *)L_26, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_3 = (String_t*)L_27;
		goto IL_00e3;
	}

IL_009b:
	{
		StringU5BU5D_t1642385972* L_28 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)7));
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, _stringLiteral372029318);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_29 = (StringU5BU5D_t1642385972*)L_28;
		CompileResult_t4163790932 * L_30 = V_1;
		NullCheck((CompileResult_t4163790932 *)L_30);
		String_t* L_31 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_30, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 1);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_31);
		StringU5BU5D_t1642385972* L_32 = (StringU5BU5D_t1642385972*)L_29;
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 2);
		ArrayElementTypeCheck (L_32, _stringLiteral372029310);
		(L_32)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029310);
		StringU5BU5D_t1642385972* L_33 = (StringU5BU5D_t1642385972*)L_32;
		BinaryExpression_t2159924157 * L_34 = V_0;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		String_t* L_35 = ((  String_t* (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 3);
		ArrayElementTypeCheck (L_33, L_35);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_35);
		StringU5BU5D_t1642385972* L_36 = (StringU5BU5D_t1642385972*)L_33;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 4);
		ArrayElementTypeCheck (L_36, _stringLiteral372029310);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029310);
		StringU5BU5D_t1642385972* L_37 = (StringU5BU5D_t1642385972*)L_36;
		CompileResult_t4163790932 * L_38 = V_2;
		NullCheck((CompileResult_t4163790932 *)L_38);
		String_t* L_39 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_38, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, 5);
		ArrayElementTypeCheck (L_37, L_39);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_39);
		StringU5BU5D_t1642385972* L_40 = (StringU5BU5D_t1642385972*)L_37;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 6);
		ArrayElementTypeCheck (L_40, _stringLiteral372029317);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_41 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_40, /*hidden argument*/NULL);
		V_3 = (String_t*)L_41;
	}

IL_00e3:
	{
		CompileResult_t4163790932 * L_42 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_42, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_42;
		CompileResult_t4163790932 * L_43 = V_24;
		String_t* L_44 = V_3;
		NullCheck((CompileResult_t4163790932 *)L_43);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_43, (String_t*)L_44, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_45 = V_24;
		return L_45;
	}

IL_00f5:
	{
		Expression_t114864668 * L_46 = ___expr0;
		NullCheck((Expression_t114864668 *)L_46);
		int32_t L_47 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_46, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_47) == ((uint32_t)6))))
		{
			goto IL_0519;
		}
	}
	{
		Expression_t114864668 * L_48 = ___expr0;
		V_4 = (MethodCallExpression_t3367820543 *)((MethodCallExpression_t3367820543 *)Castclass(L_48, MethodCallExpression_t3367820543_il2cpp_TypeInfo_var));
		MethodCallExpression_t3367820543 * L_49 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_49);
		ReadOnlyCollection_1_t300650360 * L_50 = MethodCallExpression_get_Arguments_m119024691((MethodCallExpression_t3367820543 *)L_49, /*hidden argument*/NULL);
		NullCheck((ReadOnlyCollection_1_t300650360 *)L_50);
		int32_t L_51 = ReadOnlyCollection_1_get_Count_m757747194((ReadOnlyCollection_1_t300650360 *)L_50, /*hidden argument*/ReadOnlyCollection_1_get_Count_m757747194_MethodInfo_var);
		V_5 = (CompileResultU5BU5D_t3217057757*)((CompileResultU5BU5D_t3217057757*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 21), (uint32_t)L_51));
		MethodCallExpression_t3367820543 * L_52 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_52);
		Expression_t114864668 * L_53 = MethodCallExpression_get_Object_m2833811513((MethodCallExpression_t3367820543 *)L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_013b;
		}
	}
	{
		MethodCallExpression_t3367820543 * L_54 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_54);
		Expression_t114864668 * L_55 = MethodCallExpression_get_Object_m2833811513((MethodCallExpression_t3367820543 *)L_54, /*hidden argument*/NULL);
		List_1_t2058570427 * L_56 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_57 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_55, (List_1_t2058570427 *)L_56, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		G_B15_0 = L_57;
		goto IL_013c;
	}

IL_013b:
	{
		G_B15_0 = ((CompileResult_t4163790932 *)(NULL));
	}

IL_013c:
	{
		V_6 = (CompileResult_t4163790932 *)G_B15_0;
		V_7 = (int32_t)0;
		goto IL_0166;
	}

IL_0146:
	{
		CompileResultU5BU5D_t3217057757* L_58 = V_5;
		int32_t L_59 = V_7;
		MethodCallExpression_t3367820543 * L_60 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_60);
		ReadOnlyCollection_1_t300650360 * L_61 = MethodCallExpression_get_Arguments_m119024691((MethodCallExpression_t3367820543 *)L_60, /*hidden argument*/NULL);
		int32_t L_62 = V_7;
		NullCheck((ReadOnlyCollection_1_t300650360 *)L_61);
		Expression_t114864668 * L_63 = ReadOnlyCollection_1_get_Item_m2979002709((ReadOnlyCollection_1_t300650360 *)L_61, (int32_t)L_62, /*hidden argument*/ReadOnlyCollection_1_get_Item_m2979002709_MethodInfo_var);
		List_1_t2058570427 * L_64 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_65 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_63, (List_1_t2058570427 *)L_64, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		ArrayElementTypeCheck (L_58, L_65);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(L_59), (CompileResult_t4163790932 *)L_65);
		int32_t L_66 = V_7;
		V_7 = (int32_t)((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_0166:
	{
		int32_t L_67 = V_7;
		CompileResultU5BU5D_t3217057757* L_68 = V_5;
		NullCheck(L_68);
		if ((((int32_t)L_67) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_68)->max_length)))))))
		{
			goto IL_0146;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_69 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_8 = (String_t*)L_69;
		MethodCallExpression_t3367820543 * L_70 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_70);
		MethodInfo_t * L_71 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_70, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_71);
		String_t* L_72 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_71);
		bool L_73 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_72, (String_t*)_stringLiteral3457520015, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_01df;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_74 = V_5;
		NullCheck(L_74);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_74)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_01df;
		}
	}
	{
		StringU5BU5D_t1642385972* L_75 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, 0);
		ArrayElementTypeCheck (L_75, _stringLiteral372029318);
		(L_75)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_76 = (StringU5BU5D_t1642385972*)L_75;
		CompileResultU5BU5D_t3217057757* L_77 = V_5;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 0);
		int32_t L_78 = 0;
		CompileResult_t4163790932 * L_79 = (L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		NullCheck((CompileResult_t4163790932 *)L_79);
		String_t* L_80 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_79, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 1);
		ArrayElementTypeCheck (L_76, L_80);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_80);
		StringU5BU5D_t1642385972* L_81 = (StringU5BU5D_t1642385972*)L_76;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, 2);
		ArrayElementTypeCheck (L_81, _stringLiteral3199644883);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3199644883);
		StringU5BU5D_t1642385972* L_82 = (StringU5BU5D_t1642385972*)L_81;
		CompileResultU5BU5D_t3217057757* L_83 = V_5;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, 1);
		int32_t L_84 = 1;
		CompileResult_t4163790932 * L_85 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		NullCheck((CompileResult_t4163790932 *)L_85);
		String_t* L_86 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_85, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 3);
		ArrayElementTypeCheck (L_82, L_86);
		(L_82)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_86);
		StringU5BU5D_t1642385972* L_87 = (StringU5BU5D_t1642385972*)L_82;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 4);
		ArrayElementTypeCheck (L_87, _stringLiteral372029317);
		(L_87)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_88 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_87, /*hidden argument*/NULL);
		V_8 = (String_t*)L_88;
		goto IL_0506;
	}

IL_01df:
	{
		MethodCallExpression_t3367820543 * L_89 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_89);
		MethodInfo_t * L_90 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_89, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_90);
		String_t* L_91 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_90);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_92 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_91, (String_t*)_stringLiteral1789950331, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0246;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_93 = V_5;
		NullCheck(L_93);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_93)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_0246;
		}
	}
	{
		StringU5BU5D_t1642385972* L_94 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 0);
		ArrayElementTypeCheck (L_94, _stringLiteral372029318);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_95 = (StringU5BU5D_t1642385972*)L_94;
		CompileResultU5BU5D_t3217057757* L_96 = V_5;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, 1);
		int32_t L_97 = 1;
		CompileResult_t4163790932 * L_98 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		NullCheck((CompileResult_t4163790932 *)L_98);
		String_t* L_99 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_98, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 1);
		ArrayElementTypeCheck (L_95, L_99);
		(L_95)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_99);
		StringU5BU5D_t1642385972* L_100 = (StringU5BU5D_t1642385972*)L_95;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, 2);
		ArrayElementTypeCheck (L_100, _stringLiteral1010958311);
		(L_100)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1010958311);
		StringU5BU5D_t1642385972* L_101 = (StringU5BU5D_t1642385972*)L_100;
		CompileResultU5BU5D_t3217057757* L_102 = V_5;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 0);
		int32_t L_103 = 0;
		CompileResult_t4163790932 * L_104 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		NullCheck((CompileResult_t4163790932 *)L_104);
		String_t* L_105 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_104, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_101);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_101, 3);
		ArrayElementTypeCheck (L_101, L_105);
		(L_101)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_105);
		StringU5BU5D_t1642385972* L_106 = (StringU5BU5D_t1642385972*)L_101;
		NullCheck(L_106);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_106, 4);
		ArrayElementTypeCheck (L_106, _stringLiteral372029317);
		(L_106)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_107 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_106, /*hidden argument*/NULL);
		V_8 = (String_t*)L_107;
		goto IL_0506;
	}

IL_0246:
	{
		MethodCallExpression_t3367820543 * L_108 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_108);
		MethodInfo_t * L_109 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_108, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_109);
		String_t* L_110 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_109);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_111 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_110, (String_t*)_stringLiteral1789950331, /*hidden argument*/NULL);
		if (!L_111)
		{
			goto IL_0312;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_112 = V_5;
		NullCheck(L_112);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_112)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0312;
		}
	}
	{
		MethodCallExpression_t3367820543 * L_113 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_113);
		Expression_t114864668 * L_114 = MethodCallExpression_get_Object_m2833811513((MethodCallExpression_t3367820543 *)L_113, /*hidden argument*/NULL);
		if (!L_114)
		{
			goto IL_02d2;
		}
	}
	{
		MethodCallExpression_t3367820543 * L_115 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_115);
		Expression_t114864668 * L_116 = MethodCallExpression_get_Object_m2833811513((MethodCallExpression_t3367820543 *)L_115, /*hidden argument*/NULL);
		NullCheck((Expression_t114864668 *)L_116);
		Type_t * L_117 = Expression_get_Type_m1554952480((Expression_t114864668 *)L_116, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_118 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_117) == ((Il2CppObject*)(Type_t *)L_118))))
		{
			goto IL_02d2;
		}
	}
	{
		StringU5BU5D_t1642385972* L_119 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_119);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_119, 0);
		ArrayElementTypeCheck (L_119, _stringLiteral372029318);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_120 = (StringU5BU5D_t1642385972*)L_119;
		CompileResult_t4163790932 * L_121 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_121);
		String_t* L_122 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_121, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_120);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_120, 1);
		ArrayElementTypeCheck (L_120, L_122);
		(L_120)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_122);
		StringU5BU5D_t1642385972* L_123 = (StringU5BU5D_t1642385972*)L_120;
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 2);
		ArrayElementTypeCheck (L_123, _stringLiteral913036430);
		(L_123)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral913036430);
		StringU5BU5D_t1642385972* L_124 = (StringU5BU5D_t1642385972*)L_123;
		CompileResultU5BU5D_t3217057757* L_125 = V_5;
		NullCheck(L_125);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_125, 0);
		int32_t L_126 = 0;
		CompileResult_t4163790932 * L_127 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		NullCheck((CompileResult_t4163790932 *)L_127);
		String_t* L_128 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_127, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_124);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_124, 3);
		ArrayElementTypeCheck (L_124, L_128);
		(L_124)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_128);
		StringU5BU5D_t1642385972* L_129 = (StringU5BU5D_t1642385972*)L_124;
		NullCheck(L_129);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_129, 4);
		ArrayElementTypeCheck (L_129, _stringLiteral3923541145);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3923541145);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_130 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_129, /*hidden argument*/NULL);
		V_8 = (String_t*)L_130;
		goto IL_030d;
	}

IL_02d2:
	{
		StringU5BU5D_t1642385972* L_131 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, 0);
		ArrayElementTypeCheck (L_131, _stringLiteral372029318);
		(L_131)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_132 = (StringU5BU5D_t1642385972*)L_131;
		CompileResultU5BU5D_t3217057757* L_133 = V_5;
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, 0);
		int32_t L_134 = 0;
		CompileResult_t4163790932 * L_135 = (L_133)->GetAt(static_cast<il2cpp_array_size_t>(L_134));
		NullCheck((CompileResult_t4163790932 *)L_135);
		String_t* L_136 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_135, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_132);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_132, 1);
		ArrayElementTypeCheck (L_132, L_136);
		(L_132)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_136);
		StringU5BU5D_t1642385972* L_137 = (StringU5BU5D_t1642385972*)L_132;
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 2);
		ArrayElementTypeCheck (L_137, _stringLiteral1010958311);
		(L_137)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1010958311);
		StringU5BU5D_t1642385972* L_138 = (StringU5BU5D_t1642385972*)L_137;
		CompileResult_t4163790932 * L_139 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_139);
		String_t* L_140 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_139, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, 3);
		ArrayElementTypeCheck (L_138, L_140);
		(L_138)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_140);
		StringU5BU5D_t1642385972* L_141 = (StringU5BU5D_t1642385972*)L_138;
		NullCheck(L_141);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_141, 4);
		ArrayElementTypeCheck (L_141, _stringLiteral372029317);
		(L_141)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_142 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_141, /*hidden argument*/NULL);
		V_8 = (String_t*)L_142;
	}

IL_030d:
	{
		goto IL_0506;
	}

IL_0312:
	{
		MethodCallExpression_t3367820543 * L_143 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_143);
		MethodInfo_t * L_144 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_143, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_144);
		String_t* L_145 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_144);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_146 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_145, (String_t*)_stringLiteral2969948949, /*hidden argument*/NULL);
		if (!L_146)
		{
			goto IL_0377;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_147 = V_5;
		NullCheck(L_147);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_147)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0377;
		}
	}
	{
		StringU5BU5D_t1642385972* L_148 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_148);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_148, 0);
		ArrayElementTypeCheck (L_148, _stringLiteral372029318);
		(L_148)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_149 = (StringU5BU5D_t1642385972*)L_148;
		CompileResult_t4163790932 * L_150 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_150);
		String_t* L_151 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_150, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, 1);
		ArrayElementTypeCheck (L_149, L_151);
		(L_149)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_151);
		StringU5BU5D_t1642385972* L_152 = (StringU5BU5D_t1642385972*)L_149;
		NullCheck(L_152);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_152, 2);
		ArrayElementTypeCheck (L_152, _stringLiteral796699147);
		(L_152)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral796699147);
		StringU5BU5D_t1642385972* L_153 = (StringU5BU5D_t1642385972*)L_152;
		CompileResultU5BU5D_t3217057757* L_154 = V_5;
		NullCheck(L_154);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_154, 0);
		int32_t L_155 = 0;
		CompileResult_t4163790932 * L_156 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_155));
		NullCheck((CompileResult_t4163790932 *)L_156);
		String_t* L_157 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_156, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_153);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_153, 3);
		ArrayElementTypeCheck (L_153, L_157);
		(L_153)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_157);
		StringU5BU5D_t1642385972* L_158 = (StringU5BU5D_t1642385972*)L_153;
		NullCheck(L_158);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_158, 4);
		ArrayElementTypeCheck (L_158, _stringLiteral3923541145);
		(L_158)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3923541145);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_159 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_158, /*hidden argument*/NULL);
		V_8 = (String_t*)L_159;
		goto IL_0506;
	}

IL_0377:
	{
		MethodCallExpression_t3367820543 * L_160 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_160);
		MethodInfo_t * L_161 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_160, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_161);
		String_t* L_162 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_161);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_163 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_162, (String_t*)_stringLiteral2605454916, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_03dc;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_164 = V_5;
		NullCheck(L_164);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_164)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_03dc;
		}
	}
	{
		StringU5BU5D_t1642385972* L_165 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_165);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_165, 0);
		ArrayElementTypeCheck (L_165, _stringLiteral372029318);
		(L_165)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_166 = (StringU5BU5D_t1642385972*)L_165;
		CompileResult_t4163790932 * L_167 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_167);
		String_t* L_168 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_167, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_166);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_166, 1);
		ArrayElementTypeCheck (L_166, L_168);
		(L_166)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_168);
		StringU5BU5D_t1642385972* L_169 = (StringU5BU5D_t1642385972*)L_166;
		NullCheck(L_169);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_169, 2);
		ArrayElementTypeCheck (L_169, _stringLiteral913036430);
		(L_169)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral913036430);
		StringU5BU5D_t1642385972* L_170 = (StringU5BU5D_t1642385972*)L_169;
		CompileResultU5BU5D_t3217057757* L_171 = V_5;
		NullCheck(L_171);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_171, 0);
		int32_t L_172 = 0;
		CompileResult_t4163790932 * L_173 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck((CompileResult_t4163790932 *)L_173);
		String_t* L_174 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_173, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_170);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_170, 3);
		ArrayElementTypeCheck (L_170, L_174);
		(L_170)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_174);
		StringU5BU5D_t1642385972* L_175 = (StringU5BU5D_t1642385972*)L_170;
		NullCheck(L_175);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_175, 4);
		ArrayElementTypeCheck (L_175, _stringLiteral3183958472);
		(L_175)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3183958472);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_176 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_175, /*hidden argument*/NULL);
		V_8 = (String_t*)L_176;
		goto IL_0506;
	}

IL_03dc:
	{
		MethodCallExpression_t3367820543 * L_177 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_177);
		MethodInfo_t * L_178 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_177, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_178);
		String_t* L_179 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_178);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_180 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_179, (String_t*)_stringLiteral1014339031, /*hidden argument*/NULL);
		if (!L_180)
		{
			goto IL_0441;
		}
	}
	{
		CompileResultU5BU5D_t3217057757* L_181 = V_5;
		NullCheck(L_181);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_181)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0441;
		}
	}
	{
		StringU5BU5D_t1642385972* L_182 = (StringU5BU5D_t1642385972*)((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_182);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_182, 0);
		ArrayElementTypeCheck (L_182, _stringLiteral372029318);
		(L_182)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral372029318);
		StringU5BU5D_t1642385972* L_183 = (StringU5BU5D_t1642385972*)L_182;
		CompileResult_t4163790932 * L_184 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_184);
		String_t* L_185 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_184, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_183);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_183, 1);
		ArrayElementTypeCheck (L_183, L_185);
		(L_183)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_185);
		StringU5BU5D_t1642385972* L_186 = (StringU5BU5D_t1642385972*)L_183;
		NullCheck(L_186);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_186, 2);
		ArrayElementTypeCheck (L_186, _stringLiteral2616673141);
		(L_186)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2616673141);
		StringU5BU5D_t1642385972* L_187 = (StringU5BU5D_t1642385972*)L_186;
		CompileResultU5BU5D_t3217057757* L_188 = V_5;
		NullCheck(L_188);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_188, 0);
		int32_t L_189 = 0;
		CompileResult_t4163790932 * L_190 = (L_188)->GetAt(static_cast<il2cpp_array_size_t>(L_189));
		NullCheck((CompileResult_t4163790932 *)L_190);
		String_t* L_191 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_190, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck(L_187);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_187, 3);
		ArrayElementTypeCheck (L_187, L_191);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_191);
		StringU5BU5D_t1642385972* L_192 = (StringU5BU5D_t1642385972*)L_187;
		NullCheck(L_192);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_192, 4);
		ArrayElementTypeCheck (L_192, _stringLiteral3183958472);
		(L_192)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3183958472);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_193 = String_Concat_m626692867(NULL /*static, unused*/, (StringU5BU5D_t1642385972*)L_192, /*hidden argument*/NULL);
		V_8 = (String_t*)L_193;
		goto IL_0506;
	}

IL_0441:
	{
		MethodCallExpression_t3367820543 * L_194 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_194);
		MethodInfo_t * L_195 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_194, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_195);
		String_t* L_196 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_195);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_197 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_196, (String_t*)_stringLiteral689406616, /*hidden argument*/NULL);
		if (!L_197)
		{
			goto IL_0479;
		}
	}
	{
		CompileResult_t4163790932 * L_198 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_198);
		String_t* L_199 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_198, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_200 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral884689995, (String_t*)L_199, (String_t*)_stringLiteral3183958472, /*hidden argument*/NULL);
		V_8 = (String_t*)L_200;
		goto IL_0506;
	}

IL_0479:
	{
		MethodCallExpression_t3367820543 * L_201 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_201);
		MethodInfo_t * L_202 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_201, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_202);
		String_t* L_203 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_202);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_204 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_203, (String_t*)_stringLiteral689641025, /*hidden argument*/NULL);
		if (!L_204)
		{
			goto IL_04b1;
		}
	}
	{
		CompileResult_t4163790932 * L_205 = V_6;
		NullCheck((CompileResult_t4163790932 *)L_205);
		String_t* L_206 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_205, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_207 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral639503754, (String_t*)L_206, (String_t*)_stringLiteral3183958472, /*hidden argument*/NULL);
		V_8 = (String_t*)L_207;
		goto IL_0506;
	}

IL_04b1:
	{
		MethodCallExpression_t3367820543 * L_208 = V_4;
		NullCheck((MethodCallExpression_t3367820543 *)L_208);
		MethodInfo_t * L_209 = MethodCallExpression_get_Method_m3076540940((MethodCallExpression_t3367820543 *)L_208, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_209);
		String_t* L_210 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_209);
		NullCheck((String_t*)L_210);
		String_t* L_211 = String_ToLower_m2994460523((String_t*)L_210, /*hidden argument*/NULL);
		CompileResultU5BU5D_t3217057757* L_212 = V_5;
		Func_2_t1767698742 * L_213 = ((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_U3CU3Ef__amU24cacheE_14();
		G_B45_0 = L_212;
		G_B45_1 = _stringLiteral372029314;
		G_B45_2 = _stringLiteral372029318;
		G_B45_3 = L_211;
		if (L_213)
		{
			G_B46_0 = L_212;
			G_B46_1 = _stringLiteral372029314;
			G_B46_2 = _stringLiteral372029318;
			G_B46_3 = L_211;
			goto IL_04e6;
		}
	}
	{
		IntPtr_t L_214;
		L_214.set_m_value_0((void*)(void*)IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 22));
		Func_2_t1767698742 * L_215 = (Func_2_t1767698742 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 23));
		((  void (*) (Func_2_t1767698742 *, Il2CppObject *, IntPtr_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24)->methodPointer)(L_215, (Il2CppObject *)NULL, (IntPtr_t)L_214, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 24));
		((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->set_U3CU3Ef__amU24cacheE_14(L_215);
		G_B46_0 = G_B45_0;
		G_B46_1 = G_B45_1;
		G_B46_2 = G_B45_2;
		G_B46_3 = G_B45_3;
	}

IL_04e6:
	{
		Func_2_t1767698742 * L_216 = ((TableQuery_1_t4026329353_StaticFields*)IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 13)->static_fields)->get_U3CU3Ef__amU24cacheE_14();
		Il2CppObject* L_217 = ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t1767698742 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)(Il2CppObject*)G_B46_0, (Func_2_t1767698742 *)L_216, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 25));
		StringU5BU5D_t1642385972* L_218 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, (Il2CppObject*)L_217, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_219 = String_Join_m1966872927(NULL /*static, unused*/, (String_t*)G_B46_1, (StringU5BU5D_t1642385972*)L_218, /*hidden argument*/NULL);
		String_t* L_220 = String_Concat_m1561703559(NULL /*static, unused*/, (String_t*)G_B46_3, (String_t*)G_B46_2, (String_t*)L_219, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		V_8 = (String_t*)L_220;
	}

IL_0506:
	{
		CompileResult_t4163790932 * L_221 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_221, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_221;
		CompileResult_t4163790932 * L_222 = V_24;
		String_t* L_223 = V_8;
		NullCheck((CompileResult_t4163790932 *)L_222);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_222, (String_t*)L_223, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_224 = V_24;
		return L_224;
	}

IL_0519:
	{
		Expression_t114864668 * L_225 = ___expr0;
		NullCheck((Expression_t114864668 *)L_225);
		int32_t L_226 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_225, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_226) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_055f;
		}
	}
	{
		Expression_t114864668 * L_227 = ___expr0;
		V_9 = (ConstantExpression_t305952364 *)((ConstantExpression_t305952364 *)Castclass(L_227, ConstantExpression_t305952364_il2cpp_TypeInfo_var));
		List_1_t2058570427 * L_228 = ___queryArgs1;
		ConstantExpression_t305952364 * L_229 = V_9;
		NullCheck((ConstantExpression_t305952364 *)L_229);
		Il2CppObject * L_230 = ConstantExpression_get_Value_m176732788((ConstantExpression_t305952364 *)L_229, /*hidden argument*/NULL);
		NullCheck((List_1_t2058570427 *)L_228);
		List_1_Add_m567051994((List_1_t2058570427 *)L_228, (Il2CppObject *)L_230, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
		CompileResult_t4163790932 * L_231 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_231, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_231;
		CompileResult_t4163790932 * L_232 = V_24;
		NullCheck((CompileResult_t4163790932 *)L_232);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_232, (String_t*)_stringLiteral372029331, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_233 = V_24;
		ConstantExpression_t305952364 * L_234 = V_9;
		NullCheck((ConstantExpression_t305952364 *)L_234);
		Il2CppObject * L_235 = ConstantExpression_get_Value_m176732788((ConstantExpression_t305952364 *)L_234, /*hidden argument*/NULL);
		NullCheck((CompileResult_t4163790932 *)L_233);
		((  void (*) (CompileResult_t4163790932 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((CompileResult_t4163790932 *)L_233, (Il2CppObject *)L_235, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		CompileResult_t4163790932 * L_236 = V_24;
		return L_236;
	}

IL_055f:
	{
		Expression_t114864668 * L_237 = ___expr0;
		NullCheck((Expression_t114864668 *)L_237);
		int32_t L_238 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_237, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_238) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_05cc;
		}
	}
	{
		Expression_t114864668 * L_239 = ___expr0;
		V_10 = (UnaryExpression_t4253534555 *)((UnaryExpression_t4253534555 *)Castclass(L_239, UnaryExpression_t4253534555_il2cpp_TypeInfo_var));
		UnaryExpression_t4253534555 * L_240 = V_10;
		NullCheck((Expression_t114864668 *)L_240);
		Type_t * L_241 = Expression_get_Type_m1554952480((Expression_t114864668 *)L_240, /*hidden argument*/NULL);
		V_11 = (Type_t *)L_241;
		UnaryExpression_t4253534555 * L_242 = V_10;
		NullCheck((UnaryExpression_t4253534555 *)L_242);
		Expression_t114864668 * L_243 = UnaryExpression_get_Operand_m2082944639((UnaryExpression_t4253534555 *)L_242, /*hidden argument*/NULL);
		List_1_t2058570427 * L_244 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_245 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_243, (List_1_t2058570427 *)L_244, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_12 = (CompileResult_t4163790932 *)L_245;
		CompileResult_t4163790932 * L_246 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_246, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_246;
		CompileResult_t4163790932 * L_247 = V_24;
		CompileResult_t4163790932 * L_248 = V_12;
		NullCheck((CompileResult_t4163790932 *)L_248);
		String_t* L_249 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_248, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		NullCheck((CompileResult_t4163790932 *)L_247);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_247, (String_t*)L_249, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_250 = V_24;
		CompileResult_t4163790932 * L_251 = V_12;
		NullCheck((CompileResult_t4163790932 *)L_251);
		Il2CppObject * L_252 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_251, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		G_B52_0 = L_250;
		if (!L_252)
		{
			G_B53_0 = L_250;
			goto IL_05c3;
		}
	}
	{
		CompileResult_t4163790932 * L_253 = V_12;
		NullCheck((CompileResult_t4163790932 *)L_253);
		Il2CppObject * L_254 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_253, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		Type_t * L_255 = V_11;
		Il2CppObject * L_256 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27)->methodPointer)(NULL /*static, unused*/, (Il2CppObject *)L_254, (Type_t *)L_255, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 27));
		G_B54_0 = L_256;
		G_B54_1 = G_B52_0;
		goto IL_05c4;
	}

IL_05c3:
	{
		G_B54_0 = NULL;
		G_B54_1 = G_B53_0;
	}

IL_05c4:
	{
		NullCheck((CompileResult_t4163790932 *)G_B54_1);
		((  void (*) (CompileResult_t4163790932 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((CompileResult_t4163790932 *)G_B54_1, (Il2CppObject *)G_B54_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		CompileResult_t4163790932 * L_257 = V_24;
		return L_257;
	}

IL_05cc:
	{
		Expression_t114864668 * L_258 = ___expr0;
		NullCheck((Expression_t114864668 *)L_258);
		int32_t L_259 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_258, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_259) == ((uint32_t)((int32_t)23)))))
		{
			goto IL_082d;
		}
	}
	{
		Expression_t114864668 * L_260 = ___expr0;
		V_13 = (MemberExpression_t1790982958 *)((MemberExpression_t1790982958 *)Castclass(L_260, MemberExpression_t1790982958_il2cpp_TypeInfo_var));
		MemberExpression_t1790982958 * L_261 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_261);
		Expression_t114864668 * L_262 = MemberExpression_get_Expression_m1047809307((MemberExpression_t1790982958 *)L_261, /*hidden argument*/NULL);
		if (!L_262)
		{
			goto IL_0640;
		}
	}
	{
		MemberExpression_t1790982958 * L_263 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_263);
		Expression_t114864668 * L_264 = MemberExpression_get_Expression_m1047809307((MemberExpression_t1790982958 *)L_263, /*hidden argument*/NULL);
		NullCheck((Expression_t114864668 *)L_264);
		int32_t L_265 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_264, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_265) == ((uint32_t)((int32_t)38)))))
		{
			goto IL_0640;
		}
	}
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableMapping_t3898710812 * L_266 = ((  TableMapping_t3898710812 * (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)((TableQuery_1_t4026329353 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		MemberExpression_t1790982958 * L_267 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_267);
		MemberInfo_t * L_268 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_267, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_268);
		String_t* L_269 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_268);
		NullCheck((TableMapping_t3898710812 *)L_266);
		Column_t441055761 * L_270 = TableMapping_FindColumnWithPropertyName_m3751516816((TableMapping_t3898710812 *)L_266, (String_t*)L_269, /*hidden argument*/NULL);
		NullCheck((Column_t441055761 *)L_270);
		String_t* L_271 = Column_get_Name_m2031943999((Column_t441055761 *)L_270, /*hidden argument*/NULL);
		V_14 = (String_t*)L_271;
		CompileResult_t4163790932 * L_272 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_272, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_272;
		CompileResult_t4163790932 * L_273 = V_24;
		String_t* L_274 = V_14;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_275 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029312, (String_t*)L_274, (String_t*)_stringLiteral372029312, /*hidden argument*/NULL);
		NullCheck((CompileResult_t4163790932 *)L_273);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_273, (String_t*)L_275, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_276 = V_24;
		return L_276;
	}

IL_0640:
	{
		V_15 = (Il2CppObject *)NULL;
		MemberExpression_t1790982958 * L_277 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_277);
		Expression_t114864668 * L_278 = MemberExpression_get_Expression_m1047809307((MemberExpression_t1790982958 *)L_277, /*hidden argument*/NULL);
		if (!L_278)
		{
			goto IL_06a3;
		}
	}
	{
		MemberExpression_t1790982958 * L_279 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_279);
		Expression_t114864668 * L_280 = MemberExpression_get_Expression_m1047809307((MemberExpression_t1790982958 *)L_279, /*hidden argument*/NULL);
		List_1_t2058570427 * L_281 = ___queryArgs1;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		CompileResult_t4163790932 * L_282 = ((  CompileResult_t4163790932 * (*) (TableQuery_1_t4026329353 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_t114864668 *)L_280, (List_1_t2058570427 *)L_281, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		V_16 = (CompileResult_t4163790932 *)L_282;
		CompileResult_t4163790932 * L_283 = V_16;
		NullCheck((CompileResult_t4163790932 *)L_283);
		Il2CppObject * L_284 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_283, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		if (L_284)
		{
			goto IL_0676;
		}
	}
	{
		NotSupportedException_t1793819818 * L_285 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_285, (String_t*)_stringLiteral2244287635, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_285);
	}

IL_0676:
	{
		CompileResult_t4163790932 * L_286 = V_16;
		NullCheck((CompileResult_t4163790932 *)L_286);
		String_t* L_287 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_286, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_288 = String_op_Equality_m1790663636(NULL /*static, unused*/, (String_t*)L_287, (String_t*)_stringLiteral372029331, /*hidden argument*/NULL);
		if (!L_288)
		{
			goto IL_069a;
		}
	}
	{
		List_1_t2058570427 * L_289 = ___queryArgs1;
		List_1_t2058570427 * L_290 = ___queryArgs1;
		NullCheck((List_1_t2058570427 *)L_290);
		int32_t L_291 = List_1_get_Count_m4253763168((List_1_t2058570427 *)L_290, /*hidden argument*/List_1_get_Count_m4253763168_MethodInfo_var);
		NullCheck((List_1_t2058570427 *)L_289);
		List_1_RemoveAt_m3615096820((List_1_t2058570427 *)L_289, (int32_t)((int32_t)((int32_t)L_291-(int32_t)1)), /*hidden argument*/List_1_RemoveAt_m3615096820_MethodInfo_var);
	}

IL_069a:
	{
		CompileResult_t4163790932 * L_292 = V_16;
		NullCheck((CompileResult_t4163790932 *)L_292);
		Il2CppObject * L_293 = ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15)->methodPointer)((CompileResult_t4163790932 *)L_292, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 15));
		V_15 = (Il2CppObject *)L_293;
	}

IL_06a3:
	{
		V_17 = (Il2CppObject *)NULL;
		MemberExpression_t1790982958 * L_294 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_294);
		MemberInfo_t * L_295 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_294, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_295);
		int32_t L_296 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, (MemberInfo_t *)L_295);
		if ((!(((uint32_t)L_296) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_06d8;
		}
	}
	{
		MemberExpression_t1790982958 * L_297 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_297);
		MemberInfo_t * L_298 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_297, /*hidden argument*/NULL);
		V_18 = (PropertyInfo_t *)((PropertyInfo_t *)Castclass(L_298, PropertyInfo_t_il2cpp_TypeInfo_var));
		PropertyInfo_t * L_299 = V_18;
		Il2CppObject * L_300 = V_15;
		NullCheck((PropertyInfo_t *)L_299);
		Il2CppObject * L_301 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t3614634134* >::Invoke(24 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, (PropertyInfo_t *)L_299, (Il2CppObject *)L_300, (ObjectU5BU5D_t3614634134*)(ObjectU5BU5D_t3614634134*)NULL);
		V_17 = (Il2CppObject *)L_301;
		goto IL_0729;
	}

IL_06d8:
	{
		MemberExpression_t1790982958 * L_302 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_302);
		MemberInfo_t * L_303 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_302, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_303);
		int32_t L_304 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, (MemberInfo_t *)L_303);
		if ((!(((uint32_t)L_304) == ((uint32_t)4))))
		{
			goto IL_0708;
		}
	}
	{
		MemberExpression_t1790982958 * L_305 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_305);
		MemberInfo_t * L_306 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_305, /*hidden argument*/NULL);
		V_19 = (FieldInfo_t *)((FieldInfo_t *)Castclass(L_306, FieldInfo_t_il2cpp_TypeInfo_var));
		FieldInfo_t * L_307 = V_19;
		Il2CppObject * L_308 = V_15;
		NullCheck((FieldInfo_t *)L_307);
		Il2CppObject * L_309 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_307, (Il2CppObject *)L_308);
		V_17 = (Il2CppObject *)L_309;
		goto IL_0729;
	}

IL_0708:
	{
		MemberExpression_t1790982958 * L_310 = V_13;
		NullCheck((MemberExpression_t1790982958 *)L_310);
		MemberInfo_t * L_311 = MemberExpression_get_Member_m3815419971((MemberExpression_t1790982958 *)L_310, /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_311);
		int32_t L_312 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Reflection.MemberTypes System.Reflection.MemberInfo::get_MemberType() */, (MemberInfo_t *)L_311);
		int32_t L_313 = L_312;
		Il2CppObject * L_314 = Box(MemberTypes_t3343038963_il2cpp_TypeInfo_var, &L_313);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_315 = String_Concat_m56707527(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral4206147057, (Il2CppObject *)L_314, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_316 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_316, (String_t*)L_315, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_316);
	}

IL_0729:
	{
		Il2CppObject * L_317 = V_17;
		if (!L_317)
		{
			goto IL_0806;
		}
	}
	{
		Il2CppObject * L_318 = V_17;
		if (!((Il2CppObject *)IsInst(L_318, IEnumerable_t2911409499_il2cpp_TypeInfo_var)))
		{
			goto IL_0806;
		}
	}
	{
		Il2CppObject * L_319 = V_17;
		if (((String_t*)IsInst(L_319, String_t_il2cpp_TypeInfo_var)))
		{
			goto IL_0806;
		}
	}
	{
		Il2CppObject * L_320 = V_17;
		if (((Il2CppObject*)IsInst(L_320, IEnumerable_1_t3975231481_il2cpp_TypeInfo_var)))
		{
			goto IL_0806;
		}
	}
	{
		StringBuilder_t1221177846 * L_321 = (StringBuilder_t1221177846 *)il2cpp_codegen_object_new(StringBuilder_t1221177846_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3946851802(L_321, /*hidden argument*/NULL);
		V_20 = (StringBuilder_t1221177846 *)L_321;
		StringBuilder_t1221177846 * L_322 = V_20;
		NullCheck((StringBuilder_t1221177846 *)L_322);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_322, (String_t*)_stringLiteral372029318, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_323 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_21 = (String_t*)L_323;
		Il2CppObject * L_324 = V_17;
		NullCheck((Il2CppObject *)((Il2CppObject *)Castclass(L_324, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
		Il2CppObject * L_325 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t2911409499_il2cpp_TypeInfo_var, (Il2CppObject *)((Il2CppObject *)Castclass(L_324, IEnumerable_t2911409499_il2cpp_TypeInfo_var)));
		V_23 = (Il2CppObject *)L_325;
	}

IL_077d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_07b1;
		}

IL_0782:
		{
			Il2CppObject * L_326 = V_23;
			NullCheck((Il2CppObject *)L_326);
			Il2CppObject * L_327 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_326);
			V_22 = (Il2CppObject *)L_327;
			List_1_t2058570427 * L_328 = ___queryArgs1;
			Il2CppObject * L_329 = V_22;
			NullCheck((List_1_t2058570427 *)L_328);
			List_1_Add_m567051994((List_1_t2058570427 *)L_328, (Il2CppObject *)L_329, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
			StringBuilder_t1221177846 * L_330 = V_20;
			String_t* L_331 = V_21;
			NullCheck((StringBuilder_t1221177846 *)L_330);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_330, (String_t*)L_331, /*hidden argument*/NULL);
			StringBuilder_t1221177846 * L_332 = V_20;
			NullCheck((StringBuilder_t1221177846 *)L_332);
			StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_332, (String_t*)_stringLiteral372029331, /*hidden argument*/NULL);
			V_21 = (String_t*)_stringLiteral372029314;
		}

IL_07b1:
		{
			Il2CppObject * L_333 = V_23;
			NullCheck((Il2CppObject *)L_333);
			bool L_334 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_333);
			if (L_334)
			{
				goto IL_0782;
			}
		}

IL_07bd:
		{
			IL2CPP_LEAVE(0x7D8, FINALLY_07c2);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_07c2;
	}

FINALLY_07c2:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_335 = V_23;
			V_25 = (Il2CppObject *)((Il2CppObject *)IsInst(L_335, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			Il2CppObject * L_336 = V_25;
			if (L_336)
			{
				goto IL_07d0;
			}
		}

IL_07cf:
		{
			IL2CPP_END_FINALLY(1986)
		}

IL_07d0:
		{
			Il2CppObject * L_337 = V_25;
			NullCheck((Il2CppObject *)L_337);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_337);
			IL2CPP_END_FINALLY(1986)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(1986)
	{
		IL2CPP_JUMP_TBL(0x7D8, IL_07d8)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_07d8:
	{
		StringBuilder_t1221177846 * L_338 = V_20;
		NullCheck((StringBuilder_t1221177846 *)L_338);
		StringBuilder_Append_m3636508479((StringBuilder_t1221177846 *)L_338, (String_t*)_stringLiteral372029317, /*hidden argument*/NULL);
		CompileResult_t4163790932 * L_339 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_339, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_339;
		CompileResult_t4163790932 * L_340 = V_24;
		StringBuilder_t1221177846 * L_341 = V_20;
		NullCheck((StringBuilder_t1221177846 *)L_341);
		String_t* L_342 = StringBuilder_ToString_m1507807375((StringBuilder_t1221177846 *)L_341, /*hidden argument*/NULL);
		NullCheck((CompileResult_t4163790932 *)L_340);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_340, (String_t*)L_342, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_343 = V_24;
		Il2CppObject * L_344 = V_17;
		NullCheck((CompileResult_t4163790932 *)L_343);
		((  void (*) (CompileResult_t4163790932 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((CompileResult_t4163790932 *)L_343, (Il2CppObject *)L_344, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		CompileResult_t4163790932 * L_345 = V_24;
		return L_345;
	}

IL_0806:
	{
		List_1_t2058570427 * L_346 = ___queryArgs1;
		Il2CppObject * L_347 = V_17;
		NullCheck((List_1_t2058570427 *)L_346);
		List_1_Add_m567051994((List_1_t2058570427 *)L_346, (Il2CppObject *)L_347, /*hidden argument*/List_1_Add_m567051994_MethodInfo_var);
		CompileResult_t4163790932 * L_348 = (CompileResult_t4163790932 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 18));
		((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)(L_348, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		V_24 = (CompileResult_t4163790932 *)L_348;
		CompileResult_t4163790932 * L_349 = V_24;
		NullCheck((CompileResult_t4163790932 *)L_349);
		((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20)->methodPointer)((CompileResult_t4163790932 *)L_349, (String_t*)_stringLiteral372029331, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 20));
		CompileResult_t4163790932 * L_350 = V_24;
		Il2CppObject * L_351 = V_17;
		NullCheck((CompileResult_t4163790932 *)L_350);
		((  void (*) (CompileResult_t4163790932 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26)->methodPointer)((CompileResult_t4163790932 *)L_350, (Il2CppObject *)L_351, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 26));
		CompileResult_t4163790932 * L_352 = V_24;
		return L_352;
	}

IL_082d:
	{
		Expression_t114864668 * L_353 = ___expr0;
		NullCheck((Expression_t114864668 *)L_353);
		int32_t L_354 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_353, /*hidden argument*/NULL);
		int32_t L_355 = L_354;
		Il2CppObject * L_356 = Box(ExpressionType_t1567188220_il2cpp_TypeInfo_var, &L_355);
		NullCheck((Enum_t2459695545 *)L_356);
		String_t* L_357 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2459695545 *)L_356);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_358 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral3516623994, (String_t*)L_357, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_359 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_359, (String_t*)L_358, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_359);
	}
}
// System.Object SQLite4Unity3d.TableQuery`1<System.Object>::ConvertTo(System.Object,System.Type)
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern const uint32_t TableQuery_1_ConvertTo_m2602095610_MetadataUsageId;
extern "C"  Il2CppObject * TableQuery_1_ConvertTo_m2602095610_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, Type_t * ___t1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_ConvertTo_m2602095610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Type_t * L_0 = ___t1;
		Type_t * L_1 = Nullable_GetUnderlyingType_m4057508352(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_1;
		Type_t * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Il2CppObject * L_3 = ___obj0;
		if (L_3)
		{
			goto IL_0015;
		}
	}
	{
		return NULL;
	}

IL_0015:
	{
		Il2CppObject * L_4 = ___obj0;
		Type_t * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_6 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_4, (Type_t *)L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_001d:
	{
		Il2CppObject * L_7 = ___obj0;
		Type_t * L_8 = ___t1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = Convert_ChangeType_m1630780412(NULL /*static, unused*/, (Il2CppObject *)L_7, (Type_t *)L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::CompileNullBinaryExpression(System.Linq.Expressions.BinaryExpression,SQLite4Unity3d.TableQuery`1/CompileResult<T>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ExpressionType_t1567188220_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029318;
extern Il2CppCodeGenString* _stringLiteral2349122018;
extern Il2CppCodeGenString* _stringLiteral3754511525;
extern Il2CppCodeGenString* _stringLiteral3911415209;
extern const uint32_t TableQuery_1_CompileNullBinaryExpression_m4251067313_MetadataUsageId;
extern "C"  String_t* TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared (TableQuery_1_t4026329353 * __this, BinaryExpression_t2159924157 * ___expression0, CompileResult_t4163790932 * ___parameter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_CompileNullBinaryExpression_m4251067313_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BinaryExpression_t2159924157 * L_0 = ___expression0;
		NullCheck((Expression_t114864668 *)L_0);
		int32_t L_1 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0023;
		}
	}
	{
		CompileResult_t4163790932 * L_2 = ___parameter1;
		NullCheck((CompileResult_t4163790932 *)L_2);
		String_t* L_3 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029318, (String_t*)L_3, (String_t*)_stringLiteral2349122018, /*hidden argument*/NULL);
		return L_4;
	}

IL_0023:
	{
		BinaryExpression_t2159924157 * L_5 = ___expression0;
		NullCheck((Expression_t114864668 *)L_5);
		int32_t L_6 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0046;
		}
	}
	{
		CompileResult_t4163790932 * L_7 = ___parameter1;
		NullCheck((CompileResult_t4163790932 *)L_7);
		String_t* L_8 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 12));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m612901809(NULL /*static, unused*/, (String_t*)_stringLiteral372029318, (String_t*)L_8, (String_t*)_stringLiteral3754511525, /*hidden argument*/NULL);
		return L_9;
	}

IL_0046:
	{
		BinaryExpression_t2159924157 * L_10 = ___expression0;
		NullCheck((Expression_t114864668 *)L_10);
		int32_t L_11 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_10, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(ExpressionType_t1567188220_il2cpp_TypeInfo_var, &L_12);
		NullCheck((Enum_t2459695545 *)L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2459695545 *)L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, (String_t*)_stringLiteral3911415209, (String_t*)L_14, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_16 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_16, (String_t*)L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}
}
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::GetSqlName(System.Linq.Expressions.Expression)
extern Il2CppClass* ExpressionType_t1567188220_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029332;
extern Il2CppCodeGenString* _stringLiteral502129299;
extern Il2CppCodeGenString* _stringLiteral372029330;
extern Il2CppCodeGenString* _stringLiteral502129297;
extern Il2CppCodeGenString* _stringLiteral372029308;
extern Il2CppCodeGenString* _stringLiteral3068682295;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern Il2CppCodeGenString* _stringLiteral381169821;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern Il2CppCodeGenString* _stringLiteral502129276;
extern Il2CppCodeGenString* _stringLiteral1905419108;
extern const uint32_t TableQuery_1_GetSqlName_m1128366698_MetadataUsageId;
extern "C"  String_t* TableQuery_1_GetSqlName_m1128366698_gshared (TableQuery_1_t4026329353 * __this, Expression_t114864668 * ___expr0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_GetSqlName_m1128366698_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Expression_t114864668 * L_0 = ___expr0;
		NullCheck((Expression_t114864668 *)L_0);
		int32_t L_1 = Expression_get_NodeType_m950578925((Expression_t114864668 *)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0015;
		}
	}
	{
		return _stringLiteral372029332;
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0023;
		}
	}
	{
		return _stringLiteral502129299;
	}

IL_0023:
	{
		int32_t L_4 = V_0;
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0031;
		}
	}
	{
		return _stringLiteral372029330;
	}

IL_0031:
	{
		int32_t L_5 = V_0;
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)21)))))
		{
			goto IL_003f;
		}
	}
	{
		return _stringLiteral502129297;
	}

IL_003f:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)2))))
		{
			goto IL_004c;
		}
	}
	{
		return _stringLiteral372029308;
	}

IL_004c:
	{
		int32_t L_7 = V_0;
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0059;
		}
	}
	{
		return _stringLiteral3068682295;
	}

IL_0059:
	{
		int32_t L_8 = V_0;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)36)))))
		{
			goto IL_0067;
		}
	}
	{
		return _stringLiteral372029394;
	}

IL_0067:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0075;
		}
	}
	{
		return _stringLiteral381169821;
	}

IL_0075:
	{
		int32_t L_10 = V_0;
		if ((!(((uint32_t)L_10) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0083;
		}
	}
	{
		return _stringLiteral372029329;
	}

IL_0083:
	{
		int32_t L_11 = V_0;
		if ((!(((uint32_t)L_11) == ((uint32_t)((int32_t)35)))))
		{
			goto IL_0091;
		}
	}
	{
		return _stringLiteral502129276;
	}

IL_0091:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(ExpressionType_t1567188220_il2cpp_TypeInfo_var, &L_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m56707527(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1905419108, (Il2CppObject *)L_14, /*hidden argument*/NULL);
		NotSupportedException_t1793819818 * L_16 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_16, (String_t*)L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}
}
// System.Int32 SQLite4Unity3d.TableQuery`1<System.Object>::Count()
extern const MethodInfo* SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4216650246;
extern const uint32_t TableQuery_1_Count_m3413050791_MetadataUsageId;
extern "C"  int32_t TableQuery_1_Count_m3413050791_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_Count_m3413050791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		SQLiteCommand_t2935685145 * L_0 = ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t4026329353 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((TableQuery_1_t4026329353 *)__this, (String_t*)_stringLiteral4216650246, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		NullCheck((SQLiteCommand_t2935685145 *)L_0);
		int32_t L_1 = SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099((SQLiteCommand_t2935685145 *)L_0, /*hidden argument*/SQLiteCommand_ExecuteScalar_TisInt32_t2071877448_m3011987099_MethodInfo_var);
		return L_1;
	}
}
// System.Int32 SQLite4Unity3d.TableQuery`1<System.Object>::Count(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
extern "C"  int32_t TableQuery_1_Count_m3405720028_gshared (TableQuery_1_t4026329353 * __this, Expression_1_t4002038744 * ___predExpr0, const MethodInfo* method)
{
	{
		Expression_1_t4002038744 * L_0 = ___predExpr0;
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_1 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, Expression_1_t4002038744 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29)->methodPointer)((TableQuery_1_t4026329353 *)__this, (Expression_1_t4002038744 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 29));
		NullCheck((TableQuery_1_t4026329353 *)L_1);
		int32_t L_2 = ((  int32_t (*) (TableQuery_1_t4026329353 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30)->methodPointer)((TableQuery_1_t4026329353 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 30));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.TableQuery`1<System.Object>::GetEnumerator()
extern Il2CppCodeGenString* _stringLiteral372029320;
extern const uint32_t TableQuery_1_GetEnumerator_m710809644_MetadataUsageId;
extern "C"  Il2CppObject* TableQuery_1_GetEnumerator_m710809644_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_GetEnumerator_m710809644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (bool)__this->get__deferred_10();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		SQLiteCommand_t2935685145 * L_1 = ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t4026329353 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((TableQuery_1_t4026329353 *)__this, (String_t*)_stringLiteral372029320, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		NullCheck((SQLiteCommand_t2935685145 *)L_1);
		List_1_t2058570427 * L_2 = ((  List_1_t2058570427 * (*) (SQLiteCommand_t2935685145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31)->methodPointer)((SQLiteCommand_t2935685145 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 31));
		NullCheck((List_1_t2058570427 *)L_2);
		Enumerator_t1593300101  L_3 = ((  Enumerator_t1593300101  (*) (List_1_t2058570427 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32)->methodPointer)((List_1_t2058570427 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 32));
		Enumerator_t1593300101  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 33), &L_4);
		return (Il2CppObject*)L_5;
	}

IL_0026:
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		SQLiteCommand_t2935685145 * L_6 = ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t4026329353 *, String_t*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28)->methodPointer)((TableQuery_1_t4026329353 *)__this, (String_t*)_stringLiteral372029320, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 28));
		NullCheck((SQLiteCommand_t2935685145 *)L_6);
		Il2CppObject* L_7 = ((  Il2CppObject* (*) (SQLiteCommand_t2935685145 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34)->methodPointer)((SQLiteCommand_t2935685145 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 34));
		NullCheck((Il2CppObject*)L_7);
		Il2CppObject* L_8 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 35), (Il2CppObject*)L_7);
		return L_8;
	}
}
// T SQLite4Unity3d.TableQuery`1<System.Object>::First()
extern "C"  Il2CppObject * TableQuery_1_First_m3018168943_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	TableQuery_1_t4026329353 * V_0 = NULL;
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_0 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((TableQuery_1_t4026329353 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (TableQuery_1_t4026329353 *)L_0;
		TableQuery_1_t4026329353 * L_1 = V_0;
		List_1_t2058570427 * L_2 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 37));
		return L_3;
	}
}
// T SQLite4Unity3d.TableQuery`1<System.Object>::FirstOrDefault()
extern "C"  Il2CppObject * TableQuery_1_FirstOrDefault_m3828705525_gshared (TableQuery_1_t4026329353 * __this, const MethodInfo* method)
{
	TableQuery_1_t4026329353 * V_0 = NULL;
	{
		NullCheck((TableQuery_1_t4026329353 *)__this);
		TableQuery_1_t4026329353 * L_0 = ((  TableQuery_1_t4026329353 * (*) (TableQuery_1_t4026329353 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)((TableQuery_1_t4026329353 *)__this, (int32_t)1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (TableQuery_1_t4026329353 *)L_0;
		TableQuery_1_t4026329353 * L_1 = V_0;
		List_1_t2058570427 * L_2 = ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 36));
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 38)->methodPointer)(NULL /*static, unused*/, (Il2CppObject*)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 38));
		return L_3;
	}
}
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::<GenerateCommand>m__11(SQLite4Unity3d.BaseTableQuery/Ordering)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029312;
extern Il2CppCodeGenString* _stringLiteral1903996605;
extern const uint32_t TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_MetadataUsageId;
extern "C"  String_t* TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared (Il2CppObject * __this /* static, unused */, Ordering_t1038862794 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B2_2 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	String_t* G_B3_3 = NULL;
	{
		Ordering_t1038862794 * L_0 = ___o0;
		NullCheck((Ordering_t1038862794 *)L_0);
		String_t* L_1 = Ordering_get_ColumnName_m1415693972((Ordering_t1038862794 *)L_0, /*hidden argument*/NULL);
		Ordering_t1038862794 * L_2 = ___o0;
		NullCheck((Ordering_t1038862794 *)L_2);
		bool L_3 = Ordering_get_Ascending_m2384915282((Ordering_t1038862794 *)L_2, /*hidden argument*/NULL);
		G_B1_0 = _stringLiteral372029312;
		G_B1_1 = L_1;
		G_B1_2 = _stringLiteral372029312;
		if (!L_3)
		{
			G_B2_0 = _stringLiteral372029312;
			G_B2_1 = L_1;
			G_B2_2 = _stringLiteral372029312;
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_002a;
	}

IL_0025:
	{
		G_B3_0 = _stringLiteral1903996605;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m1561703559(NULL /*static, unused*/, (String_t*)G_B3_3, (String_t*)G_B3_2, (String_t*)G_B3_1, (String_t*)G_B3_0, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.String SQLite4Unity3d.TableQuery`1<System.Object>::<CompileExpr>m__12(SQLite4Unity3d.TableQuery`1/CompileResult<T>)
extern "C"  String_t* TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared (Il2CppObject * __this /* static, unused */, CompileResult_t4163790932 * ___a0, const MethodInfo* method)
{
	{
		CompileResult_t4163790932 * L_0 = ___a0;
		NullCheck((CompileResult_t4163790932 *)L_0);
		String_t* L_1 = ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12)->methodPointer)((CompileResult_t4163790932 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 12));
		return L_1;
	}
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m782011149_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1684652980((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2249515781_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T)
extern "C"  void Action_1_Invoke_m2983653028_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2983653028((Action_1_t2755832024 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1319991882_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1319991882_gshared (Action_1_t2755832024 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1319991882_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1485676899_gshared (Action_1_t2755832024 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m2184330030_gshared (Action_1_t904151438 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C"  void Action_1_Invoke_m592407309_gshared (Action_1_t904151438 * __this, SmartTerrainInitializationInfo_t1102352056  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m592407309((Action_1_t904151438 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SmartTerrainInitializationInfo_t1102352056  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, SmartTerrainInitializationInfo_t1102352056  ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* SmartTerrainInitializationInfo_t1102352056_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m1999307955_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1999307955_gshared (Action_1_t904151438 * __this, SmartTerrainInitializationInfo_t1102352056  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m1999307955_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(SmartTerrainInitializationInfo_t1102352056_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m4199649012_gshared (Action_1_t904151438 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3262798437_gshared (Action_1_t1951195598 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::Invoke(T)
extern "C"  void Action_1_Invoke_m2649252155_gshared (Action_1_t1951195598 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m2649252155((Action_1_t1951195598 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<Vuforia.VuforiaUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* InitError_t2149396216_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m776527021_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m776527021_gshared (Action_1_t1951195598 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m776527021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitError_t2149396216_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<Vuforia.VuforiaUnity/InitError>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m609895098_gshared (Action_1_t1951195598 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1053279240_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m2365036873_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m2365036873((Action_2_t1907880187 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Int32>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1925898598_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1925898598_gshared (Action_2_t1907880187 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1925898598_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m343868110_gshared (Action_2_t1907880187 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3463170984_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3143687639_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3143687639((Action_2_t2790035381 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m410485158_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m410485158_gshared (Action_2_t2790035381 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m410485158_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(InitializationFailureReason_t2954032642_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.InitializationFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m99462074_gshared (Action_2_t2790035381 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m1121744629_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1462475090_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1462475090((Action_2_t1158962578 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, int32_t ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern Il2CppClass* PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m772046713_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m772046713_gshared (Action_2_t1158962578 * __this, bool ___arg10, int32_t ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m772046713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = Box(PurchaseFailureReason_t1322959839_il2cpp_TypeInfo_var, &___arg21);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,UnityEngine.Purchasing.PurchaseFailureReason>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m430603547_gshared (Action_2_t1158962578 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1501152969((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<UnityEngine.Vector2,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m497263298_gshared (Action_2_t3938509041 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<UnityEngine.Vector2,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m961527555_gshared (Action_2_t3938509041 * __this, Vector2_t2243707579  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m961527555((Action_2_t3938509041 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<UnityEngine.Vector2,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m377899404_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m377899404_gshared (Action_2_t3938509041 * __this, Vector2_t2243707579  ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m377899404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<UnityEngine.Vector2,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3255379472_gshared (Action_2_t3938509041 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<UnityEngine.Vector3,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m4071786849_gshared (Action_2_t129729484 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<UnityEngine.Vector3,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m12962276_gshared (Action_2_t129729484 * __this, Vector3_t2243707580  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m12962276((Action_2_t129729484 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<UnityEngine.Vector3,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m1370092077_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1370092077_gshared (Action_2_t129729484 * __this, Vector3_t2243707580  ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m1370092077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<UnityEngine.Vector3,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2096582895_gshared (Action_2_t129729484 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m841787854_gshared (InternalEnumerator_1_t2867586724 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m841787854_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	InternalEnumerator_1__ctor_m841787854(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4224085210_gshared (InternalEnumerator_1_t2867586724 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4224085210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4224085210(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4239912252_gshared (InternalEnumerator_1_t2867586724 * __this, const MethodInfo* method)
{
	{
		ArrayMetadata_t2008834462  L_0 = InternalEnumerator_1_get_Current_m2241461473((InternalEnumerator_1_t2867586724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ArrayMetadata_t2008834462  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4239912252_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4239912252(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1721578713_gshared (InternalEnumerator_1_t2867586724 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1721578713_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1721578713(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m726201614_gshared (InternalEnumerator_1_t2867586724 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m726201614_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m726201614(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.ArrayMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2241461473_MetadataUsageId;
extern "C"  ArrayMetadata_t2008834462  InternalEnumerator_1_get_Current_m2241461473_gshared (InternalEnumerator_1_t2867586724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2241461473_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ArrayMetadata_t2008834462  L_8 = ((  ArrayMetadata_t2008834462  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ArrayMetadata_t2008834462  InternalEnumerator_1_get_Current_m2241461473_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2867586724 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2867586724 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2241461473(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3281303050_gshared (InternalEnumerator_1_t559707364 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3281303050_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	InternalEnumerator_1__ctor_m3281303050(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2401797550_gshared (InternalEnumerator_1_t559707364 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2401797550_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2401797550(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4095783582_gshared (InternalEnumerator_1_t559707364 * __this, const MethodInfo* method)
{
	{
		ObjectMetadata_t3995922398  L_0 = InternalEnumerator_1_get_Current_m3491825305((InternalEnumerator_1_t559707364 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ObjectMetadata_t3995922398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4095783582_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4095783582(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1687284417_gshared (InternalEnumerator_1_t559707364 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1687284417_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1687284417(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2379468810_gshared (InternalEnumerator_1_t559707364 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2379468810_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2379468810(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.ObjectMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3491825305_MetadataUsageId;
extern "C"  ObjectMetadata_t3995922398  InternalEnumerator_1_get_Current_m3491825305_gshared (InternalEnumerator_1_t559707364 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3491825305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ObjectMetadata_t3995922398  L_8 = ((  ObjectMetadata_t3995922398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ObjectMetadata_t3995922398  InternalEnumerator_1_get_Current_m3491825305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t559707364 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t559707364 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3491825305(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4186198824_gshared (InternalEnumerator_1_t257611102 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4186198824_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	InternalEnumerator_1__ctor_m4186198824(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1247144512_gshared (InternalEnumerator_1_t257611102 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1247144512_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1247144512(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1337173388_gshared (InternalEnumerator_1_t257611102 * __this, const MethodInfo* method)
{
	{
		PropertyMetadata_t3693826136  L_0 = InternalEnumerator_1_get_Current_m2661172007((InternalEnumerator_1_t257611102 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		PropertyMetadata_t3693826136  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1337173388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1337173388(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3725867263_gshared (InternalEnumerator_1_t257611102 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3725867263_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3725867263(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3375724832_gshared (InternalEnumerator_1_t257611102 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3375724832_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3375724832(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<LitJson.PropertyMetadata>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2661172007_MetadataUsageId;
extern "C"  PropertyMetadata_t3693826136  InternalEnumerator_1_get_Current_m2661172007_gshared (InternalEnumerator_1_t257611102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2661172007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		PropertyMetadata_t3693826136  L_8 = ((  PropertyMetadata_t3693826136  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  PropertyMetadata_t3693826136  InternalEnumerator_1_get_Current_m2661172007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t257611102 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t257611102 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2661172007(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3483542273_gshared (InternalEnumerator_1_t1532912250 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3483542273_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	InternalEnumerator_1__ctor_m3483542273(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3599613109(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method)
{
	{
		IndexedColumn_t674159988  L_0 = InternalEnumerator_1_get_Current_m3065343396((InternalEnumerator_1_t1532912250 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IndexedColumn_t674159988  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3253925289(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m121797580_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m121797580_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	InternalEnumerator_1_Dispose_m121797580(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2090093981_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2090093981_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2090093981(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3065343396_MetadataUsageId;
extern "C"  IndexedColumn_t674159988  InternalEnumerator_1_get_Current_m3065343396_gshared (InternalEnumerator_1_t1532912250 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3065343396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IndexedColumn_t674159988  L_8 = ((  IndexedColumn_t674159988  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IndexedColumn_t674159988  InternalEnumerator_1_get_Current_m3065343396_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1532912250 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1532912250 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3065343396(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1550456612_gshared (InternalEnumerator_1_t1706787027 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1550456612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	InternalEnumerator_1__ctor_m1550456612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method)
{
	{
		IndexInfo_t848034765  L_0 = InternalEnumerator_1_get_Current_m204930273((InternalEnumerator_1_t1706787027 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IndexInfo_t848034765  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m397456233_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m397456233_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	InternalEnumerator_1_Dispose_m397456233(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1246711028_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1246711028_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1246711028(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m204930273_MetadataUsageId;
extern "C"  IndexInfo_t848034765  InternalEnumerator_1_get_Current_m204930273_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m204930273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IndexInfo_t848034765  L_8 = ((  IndexInfo_t848034765  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IndexInfo_t848034765  InternalEnumerator_1_get_Current_m204930273_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1706787027 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1706787027 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m204930273(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2688327768_gshared (InternalEnumerator_1_t1723885533 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2688327768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1__ctor_m2688327768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		Link_t865133271  L_0 = InternalEnumerator_1_get_Current_m1855333455((InternalEnumerator_1_t1723885533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t865133271  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1064404287(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3585886944(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId;
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t865133271  L_8 = ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1855333455(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3441346029_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1__ctor_m3441346029(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = InternalEnumerator_1_get_Current_m3582710858((InternalEnumerator_1_t313372414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m718416578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m718416578(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1791963761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId;
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3749587448  L_8 = ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582710858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m49278889_gshared (InternalEnumerator_1_t3866418389 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m49278889_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	InternalEnumerator_1__ctor_m49278889(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2516123773(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3007666127  L_0 = InternalEnumerator_1_get_Current_m2132140880((InternalEnumerator_1_t3866418389 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3007666127  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m696238645(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3100772778_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3100772778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3100772778(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m507230133_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m507230133_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m507230133(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2132140880_MetadataUsageId;
extern "C"  KeyValuePair_2_t3007666127  InternalEnumerator_1_get_Current_m2132140880_gshared (InternalEnumerator_1_t3866418389 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2132140880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3007666127  L_8 = ((  KeyValuePair_2_t3007666127  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3007666127  InternalEnumerator_1_get_Current_m2132140880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3866418389 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3866418389 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2132140880(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3694461094_gshared (InternalEnumerator_1_t3142776066 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3694461094_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	InternalEnumerator_1__ctor_m3694461094(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3941294486_gshared (InternalEnumerator_1_t3142776066 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3941294486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3941294486(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m353244778_gshared (InternalEnumerator_1_t3142776066 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2284023804  L_0 = InternalEnumerator_1_get_Current_m3582725487((InternalEnumerator_1_t3142776066 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2284023804  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m353244778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m353244778(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m699162871_gshared (InternalEnumerator_1_t3142776066 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m699162871_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	InternalEnumerator_1_Dispose_m699162871(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2586136510_gshared (InternalEnumerator_1_t3142776066 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2586136510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2586136510(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3582725487_MetadataUsageId;
extern "C"  KeyValuePair_2_t2284023804  InternalEnumerator_1_get_Current_m3582725487_gshared (InternalEnumerator_1_t3142776066 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582725487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2284023804  L_8 = ((  KeyValuePair_2_t2284023804  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2284023804  InternalEnumerator_1_get_Current_m3582725487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3142776066 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3142776066 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582725487(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1701403783_gshared (InternalEnumerator_1_t216992074 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1701403783_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	InternalEnumerator_1__ctor_m1701403783(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m111113895_gshared (InternalEnumerator_1_t216992074 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m111113895_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m111113895(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3963271227_gshared (InternalEnumerator_1_t216992074 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3653207108  L_0 = InternalEnumerator_1_get_Current_m146527680((InternalEnumerator_1_t216992074 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3653207108  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3963271227_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3963271227(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3768784518_gshared (InternalEnumerator_1_t216992074 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3768784518_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3768784518(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3862353347_gshared (InternalEnumerator_1_t216992074 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3862353347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3862353347(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ArrayMetadata>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m146527680_MetadataUsageId;
extern "C"  KeyValuePair_2_t3653207108  InternalEnumerator_1_get_Current_m146527680_gshared (InternalEnumerator_1_t216992074 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m146527680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3653207108  L_8 = ((  KeyValuePair_2_t3653207108  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3653207108  InternalEnumerator_1_get_Current_m146527680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t216992074 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t216992074 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m146527680(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1121415019_gshared (InternalEnumerator_1_t2204080010 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1121415019_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	InternalEnumerator_1__ctor_m1121415019(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2925758851_gshared (InternalEnumerator_1_t2204080010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2925758851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2925758851(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2461204595_gshared (InternalEnumerator_1_t2204080010 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1345327748  L_0 = InternalEnumerator_1_get_Current_m1552431004((InternalEnumerator_1_t2204080010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1345327748  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2461204595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2461204595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1263364952_gshared (InternalEnumerator_1_t2204080010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1263364952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1263364952(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3045930583_gshared (InternalEnumerator_1_t2204080010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3045930583_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3045930583(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.ObjectMetadata>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1552431004_MetadataUsageId;
extern "C"  KeyValuePair_2_t1345327748  InternalEnumerator_1_get_Current_m1552431004_gshared (InternalEnumerator_1_t2204080010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1552431004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1345327748  L_8 = ((  KeyValuePair_2_t1345327748  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1345327748  InternalEnumerator_1_get_Current_m1552431004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2204080010 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2204080010 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1552431004(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1989011005_gshared (InternalEnumerator_1_t1901983748 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1989011005_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	InternalEnumerator_1__ctor_m1989011005(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2051077857_gshared (InternalEnumerator_1_t1901983748 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2051077857_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2051077857(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1588952245_gshared (InternalEnumerator_1_t1901983748 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1043231486  L_0 = InternalEnumerator_1_get_Current_m1082833050((InternalEnumerator_1_t1901983748 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1043231486  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1588952245_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1588952245(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m923524778_gshared (InternalEnumerator_1_t1901983748 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m923524778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	InternalEnumerator_1_Dispose_m923524778(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4083156753_gshared (InternalEnumerator_1_t1901983748 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4083156753_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4083156753(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,LitJson.PropertyMetadata>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1082833050_MetadataUsageId;
extern "C"  KeyValuePair_2_t1043231486  InternalEnumerator_1_get_Current_m1082833050_gshared (InternalEnumerator_1_t1901983748 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1082833050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1043231486  L_8 = ((  KeyValuePair_2_t1043231486  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1043231486  InternalEnumerator_1_get_Current_m1082833050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1901983748 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1901983748 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1082833050(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2906363851_gshared (InternalEnumerator_1_t3351159673 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2906363851_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	InternalEnumerator_1__ctor_m2906363851(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2492407411  L_0 = InternalEnumerator_1_get_Current_m3628801954((InternalEnumerator_1_t3351159673 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2492407411  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1817796636_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1817796636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1817796636(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3504749663_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3504749663_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3504749663(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3628801954_MetadataUsageId;
extern "C"  KeyValuePair_2_t2492407411  InternalEnumerator_1_get_Current_m3628801954_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3628801954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2492407411  L_8 = ((  KeyValuePair_2_t2492407411  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2492407411  InternalEnumerator_1_get_Current_m3628801954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3351159673 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3351159673 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3628801954(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m967618647_gshared (InternalEnumerator_1_t2033732330 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m967618647_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1__ctor_m967618647(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = InternalEnumerator_1_get_Current_m3900993294((InternalEnumerator_1_t2033732330 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318835130_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m318835130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_Dispose_m318835130(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4294226955(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId;
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1174980068  L_8 = ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3900993294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2544656173_gshared (InternalEnumerator_1_t2541979553 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2544656173_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	InternalEnumerator_1__ctor_m2544656173(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1564890497_gshared (InternalEnumerator_1_t2541979553 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1564890497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1564890497(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2175290877_gshared (InternalEnumerator_1_t2541979553 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1683227291  L_0 = InternalEnumerator_1_get_Current_m429379826((InternalEnumerator_1_t2541979553 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1683227291  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2175290877_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2175290877(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m778760474_gshared (InternalEnumerator_1_t2541979553 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m778760474_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	InternalEnumerator_1_Dispose_m778760474(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2486639217_gshared (InternalEnumerator_1_t2541979553 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2486639217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2486639217(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m429379826_MetadataUsageId;
extern "C"  KeyValuePair_2_t1683227291  InternalEnumerator_1_get_Current_m429379826_gshared (InternalEnumerator_1_t2541979553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m429379826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1683227291  L_8 = ((  KeyValuePair_2_t1683227291  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1683227291  InternalEnumerator_1_get_Current_m429379826_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2541979553 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2541979553 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m429379826(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3362782841_gshared (InternalEnumerator_1_t280035060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3362782841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1__ctor_m3362782841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = InternalEnumerator_1_get_Current_m2882946014((InternalEnumerator_1_t280035060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1748410190(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3486952605(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId;
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3716250094  L_8 = ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2882946014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3587374424_gshared (InternalEnumerator_1_t897606907 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3587374424_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1__ctor_m3587374424(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = InternalEnumerator_1_get_Current_m2345377791((InternalEnumerator_1_t897606907 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2413981551(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1667794624(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t38854645  L_8 = ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2345377791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2339127852_gshared (InternalEnumerator_1_t3490007519 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2339127852_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	InternalEnumerator_1__ctor_m2339127852(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172058988_gshared (InternalEnumerator_1_t3490007519 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172058988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m172058988(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m652788160_gshared (InternalEnumerator_1_t3490007519 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2631255257  L_0 = InternalEnumerator_1_get_Current_m2101567003((InternalEnumerator_1_t3490007519 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2631255257  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m652788160_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m652788160(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3323073915_gshared (InternalEnumerator_1_t3490007519 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3323073915_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3323073915(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2768190308_gshared (InternalEnumerator_1_t3490007519 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2768190308_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2768190308(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2101567003_MetadataUsageId;
extern "C"  KeyValuePair_2_t2631255257  InternalEnumerator_1_get_Current_m2101567003_gshared (InternalEnumerator_1_t3490007519 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2101567003_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2631255257  L_8 = ((  KeyValuePair_2_t2631255257  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2631255257  InternalEnumerator_1_get_Current_m2101567003_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3490007519 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3490007519 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2101567003(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2577374626_gshared (InternalEnumerator_1_t4227791396 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2577374626_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	InternalEnumerator_1__ctor_m2577374626(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2496185450_gshared (InternalEnumerator_1_t4227791396 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2496185450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2496185450(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4254632950_gshared (InternalEnumerator_1_t4227791396 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3369039134  L_0 = InternalEnumerator_1_get_Current_m19786651((InternalEnumerator_1_t4227791396 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3369039134  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4254632950_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4254632950(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m980531531_gshared (InternalEnumerator_1_t4227791396 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m980531531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	InternalEnumerator_1_Dispose_m980531531(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1747837434_gshared (InternalEnumerator_1_t4227791396 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1747837434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1747837434(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m19786651_MetadataUsageId;
extern "C"  KeyValuePair_2_t3369039134  InternalEnumerator_1_get_Current_m19786651_gshared (InternalEnumerator_1_t4227791396 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m19786651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3369039134  L_8 = ((  KeyValuePair_2_t3369039134  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3369039134  InternalEnumerator_1_get_Current_m19786651_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4227791396 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4227791396 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m19786651(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3330651452_gshared (InternalEnumerator_1_t2999800314 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3330651452_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	InternalEnumerator_1__ctor_m3330651452(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1522375228_gshared (InternalEnumerator_1_t2999800314 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1522375228_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1522375228(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1964426498_gshared (InternalEnumerator_1_t2999800314 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2141048052  L_0 = InternalEnumerator_1_get_Current_m2144566283((InternalEnumerator_1_t2999800314 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2141048052  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1964426498_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1964426498(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1782726327_gshared (InternalEnumerator_1_t2999800314 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1782726327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1782726327(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3349422356_gshared (InternalEnumerator_1_t2999800314 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3349422356_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3349422356(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2144566283_MetadataUsageId;
extern "C"  KeyValuePair_2_t2141048052  InternalEnumerator_1_get_Current_m2144566283_gshared (InternalEnumerator_1_t2999800314 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2144566283_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2141048052  L_8 = ((  KeyValuePair_2_t2141048052  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t2141048052  InternalEnumerator_1_get_Current_m2144566283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2999800314 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2999800314 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2144566283(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m439810834_gshared (InternalEnumerator_1_t3582009740 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m439810834_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1__ctor_m439810834(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		Link_t2723257478  L_0 = InternalEnumerator_1_get_Current_m3444791149((InternalEnumerator_1_t3582009740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2723257478  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m296683029_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m296683029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_Dispose_m296683029(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1994485778(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId;
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2723257478  L_8 = ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3444791149(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m488579894_gshared (InternalEnumerator_1_t2881283523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m488579894_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1__ctor_m488579894(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		Slot_t2022531261  L_0 = InternalEnumerator_1_get_Current_m198513457((InternalEnumerator_1_t2881283523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2022531261  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m802528953_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m802528953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_Dispose_m802528953(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3278167302(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m198513457_MetadataUsageId;
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m198513457_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2022531261  L_8 = ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m198513457(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1405610577_gshared (InternalEnumerator_1_t3126312864 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1405610577_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1__ctor_m1405610577(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3237341717(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		Slot_t2267560602  L_0 = InternalEnumerator_1_get_Current_m4193726352((InternalEnumerator_1_t3126312864 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2267560602  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3600601141(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2337194690_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2337194690_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2337194690(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3476348493_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3476348493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3476348493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4193726352_MetadataUsageId;
extern "C"  Slot_t2267560602  InternalEnumerator_1_get_Current_m4193726352_gshared (InternalEnumerator_1_t3126312864 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4193726352_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2267560602  L_8 = ((  Slot_t2267560602  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2267560602  InternalEnumerator_1_get_Current_m4193726352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3126312864 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3126312864 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4193726352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m245588437_gshared (InternalEnumerator_1_t1551957931 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m245588437_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1__ctor_m245588437(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2174159777(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		DateTime_t693205669  L_0 = InternalEnumerator_1_get_Current_m4279678504((InternalEnumerator_1_t1551957931 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t693205669  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3315293493(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3383574608_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3383574608(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3300932033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3300932033(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId;
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_gshared (InternalEnumerator_1_t1551957931 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4279678504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t693205669  L_8 = ((  DateTime_t693205669  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DateTime_t693205669  InternalEnumerator_1_get_Current_m4279678504_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1551957931 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1551957931 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4279678504(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4150855019_gshared (InternalEnumerator_1_t1583453339 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4150855019_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1__ctor_m4150855019(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1963130955(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		Decimal_t724701077  L_0 = InternalEnumerator_1_get_Current_m245025210((InternalEnumerator_1_t1583453339 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t724701077  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1025729343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3407567388_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3407567388(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4134231455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4134231455(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m245025210_MetadataUsageId;
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_gshared (InternalEnumerator_1_t1583453339 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m245025210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t724701077  L_8 = ((  Decimal_t724701077  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Decimal_t724701077  InternalEnumerator_1_get_Current_m245025210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1583453339 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1583453339 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m245025210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3589241961_gshared (InternalEnumerator_1_t641800647 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3589241961_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1__ctor_m3589241961(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3194282029(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		double L_0 = InternalEnumerator_1_get_Current_m1389169756((InternalEnumerator_1_t641800647 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2842514953(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3578333724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3578333724(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m83303365_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m83303365(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_gshared (InternalEnumerator_1_t641800647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1389169756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  double InternalEnumerator_1_get_Current_m1389169756_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t641800647 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t641800647 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1389169756(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m557239862_gshared (InternalEnumerator_1_t605030880 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m557239862_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1__ctor_m557239862(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m487832594(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = InternalEnumerator_1_get_Current_m3259181373((InternalEnumerator_1_t605030880 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2068723842(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2743309309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2743309309(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4274987126_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4274987126(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_gshared (InternalEnumerator_1_t605030880 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3259181373_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int16_t InternalEnumerator_1_get_Current_m3259181373_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t605030880 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t605030880 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3259181373(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m504913220_gshared (InternalEnumerator_1_t2930629710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m504913220_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1__ctor_m504913220(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2726857860(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m10285187((InternalEnumerator_1_t2930629710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1527025224(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3393096515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3393096515(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3679487948_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3679487948(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m10285187_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_gshared (InternalEnumerator_1_t2930629710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m10285187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m10285187_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2930629710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2930629710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m10285187(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2597133905_gshared (InternalEnumerator_1_t1767830299 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2597133905_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1__ctor_m2597133905(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2144409197(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = InternalEnumerator_1_get_Current_m2415979394((InternalEnumerator_1_t1767830299 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2545039741(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m307741520_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m307741520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	InternalEnumerator_1_Dispose_m307741520(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1683120485_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1683120485(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_gshared (InternalEnumerator_1_t1767830299 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2415979394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int64_t InternalEnumerator_1_get_Current_m2415979394_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1767830299 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1767830299 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2415979394(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1648185761_gshared (InternalEnumerator_1_t3362812871 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1648185761_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1__ctor_m1648185761(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1809507733(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = InternalEnumerator_1_get_Current_m1706492988((InternalEnumerator_1_t3362812871 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m127456009(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3933737284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3933737284(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2720582493_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2720582493(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_gshared (InternalEnumerator_1_t3362812871 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1706492988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m1706492988_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3362812871 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3362812871 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1706492988(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m853313801_gshared (InternalEnumerator_1_t3548201557 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m853313801_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1__ctor_m853313801(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3080260213(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = InternalEnumerator_1_get_Current_m3206960238((InternalEnumerator_1_t3548201557 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m94051553(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1636767846_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1636767846(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1047150157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1047150157(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_gshared (InternalEnumerator_1_t3548201557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3206960238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m3206960238_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3548201557 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3548201557 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3206960238(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m492779768_gshared (InternalEnumerator_1_t952909805 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m492779768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1__ctor_m492779768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2494446096(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = InternalEnumerator_1_get_Current_m1089848479((InternalEnumerator_1_t952909805 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1322273508(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m238246335_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m238246335_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	InternalEnumerator_1_Dispose_m238246335(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1548080384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1548080384(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_gshared (InternalEnumerator_1_t952909805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1089848479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t94157543  L_8 = ((  CustomAttributeNamedArgument_t94157543  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeNamedArgument_t94157543  InternalEnumerator_1_get_Current_m1089848479_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t952909805 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t952909805 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1089848479(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m821424641_gshared (InternalEnumerator_1_t2356950176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m821424641_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1__ctor_m821424641(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2624612805(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = InternalEnumerator_1_get_Current_m1047712960((InternalEnumerator_1_t2356950176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2315179333(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4038440306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4038440306(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2904932349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2904932349(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_gshared (InternalEnumerator_1_t2356950176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1047712960_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t1498197914  L_8 = ((  CustomAttributeTypedArgument_t1498197914  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  CustomAttributeTypedArgument_t1498197914  InternalEnumerator_1_get_Current_m1047712960_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2356950176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2356950176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1047712960(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3323962057_gshared (InternalEnumerator_1_t275897710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3323962057_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1__ctor_m3323962057(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2589050037(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		LabelData_t3712112744  L_0 = InternalEnumerator_1_get_Current_m3922357178((InternalEnumerator_1_t275897710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t3712112744  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4242639349(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m549215360_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m549215360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	InternalEnumerator_1_Dispose_m549215360(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3389738333_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3389738333(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId;
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_gshared (InternalEnumerator_1_t275897710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3922357178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t3712112744  L_8 = ((  LabelData_t3712112744  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelData_t3712112744  InternalEnumerator_1_get_Current_m3922357178_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t275897710 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t275897710 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3922357178(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3228997263_gshared (InternalEnumerator_1_t654694480 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3228997263_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1__ctor_m3228997263(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3279821511(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t4090909514  L_0 = InternalEnumerator_1_get_Current_m2468740214((InternalEnumerator_1_t654694480 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t4090909514  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1597849391(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3927915442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3927915442(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4292005299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4292005299(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId;
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_gshared (InternalEnumerator_1_t654694480 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2468740214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t4090909514  L_8 = ((  LabelFixup_t4090909514  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  LabelFixup_t4090909514  InternalEnumerator_1_get_Current_m2468740214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t654694480 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t654694480 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2468740214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3387972470_gshared (InternalEnumerator_1_t1008311600 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3387972470_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1__ctor_m3387972470(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m651165750(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t149559338  L_0 = InternalEnumerator_1_get_Current_m3296972783((InternalEnumerator_1_t1008311600 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t149559338  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3239681450(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2056889175_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2056889175(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1590907854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1590907854(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId;
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_gshared (InternalEnumerator_1_t1008311600 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3296972783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t149559338  L_8 = ((  ILTokenInfo_t149559338  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ILTokenInfo_t149559338  InternalEnumerator_1_get_Current_m3296972783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1008311600 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1008311600 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3296972783(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2890018883_gshared (InternalEnumerator_1_t2679387182 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2890018883_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1__ctor_m2890018883(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107040235(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t1820634920  L_0 = InternalEnumerator_1_get_Current_m4083613828((InternalEnumerator_1_t2679387182 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t1820634920  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2851415307(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3952699776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3952699776(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1594563423_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1594563423(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId;
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_gshared (InternalEnumerator_1_t2679387182 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4083613828_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t1820634920  L_8 = ((  ParameterModifier_t1820634920  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ParameterModifier_t1820634920  InternalEnumerator_1_get_Current_m4083613828_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2679387182 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2679387182 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4083613828(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1182539814_gshared (InternalEnumerator_1_t1191988411 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1182539814_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1__ctor_m1182539814(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2821513122(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t333236149  L_0 = InternalEnumerator_1_get_Current_m789289033((InternalEnumerator_1_t1191988411 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t333236149  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1049770044(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4175113225_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4175113225_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4175113225(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2302237510_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2302237510_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2302237510(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m789289033_MetadataUsageId;
extern "C"  ResourceCacheItem_t333236149  InternalEnumerator_1_get_Current_m789289033_gshared (InternalEnumerator_1_t1191988411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m789289033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t333236149  L_8 = ((  ResourceCacheItem_t333236149  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceCacheItem_t333236149  InternalEnumerator_1_get_Current_m789289033_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1191988411 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1191988411 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m789289033(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1336720787_gshared (InternalEnumerator_1_t496834202 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1336720787_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1__ctor_m1336720787(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2116079299(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t3933049236  L_0 = InternalEnumerator_1_get_Current_m4154059426((InternalEnumerator_1_t496834202 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t3933049236  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4023948615(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1794459540_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1794459540_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1794459540(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2576139351_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2576139351_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2576139351(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154059426_MetadataUsageId;
extern "C"  ResourceInfo_t3933049236  InternalEnumerator_1_get_Current_m4154059426_gshared (InternalEnumerator_1_t496834202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154059426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t3933049236  L_8 = ((  ResourceInfo_t3933049236  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ResourceInfo_t3933049236  InternalEnumerator_1_get_Current_m4154059426_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t496834202 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t496834202 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154059426(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4063293236_gshared (InternalEnumerator_1_t999961858 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4063293236_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1__ctor_m4063293236(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1561424184(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m2286118957((InternalEnumerator_1_t999961858 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4088899688(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1020222893_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1020222893_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1020222893(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1686633972_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1686633972_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1686633972(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2286118957_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2286118957_gshared (InternalEnumerator_1_t999961858 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2286118957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2286118957_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t999961858 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t999961858 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2286118957(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2108401677_gshared (InternalEnumerator_1_t1313169811 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2108401677_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1__ctor_m2108401677(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4085710193(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = InternalEnumerator_1_get_Current_m314017974((InternalEnumerator_1_t1313169811 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2607490481(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1676985532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1676985532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3984801393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3984801393(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m314017974_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_gshared (InternalEnumerator_1_t1313169811 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m314017974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int8_t InternalEnumerator_1_get_Current_m314017974_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1313169811 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1313169811 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m314017974(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m655778553_gshared (InternalEnumerator_1_t842163687 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m655778553_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1__ctor_m655778553(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2198960685(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t4278378721  L_0 = InternalEnumerator_1_get_Current_m1550231132((InternalEnumerator_1_t842163687 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t4278378721  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576641073(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3671580532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3671580532(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1869236997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1869236997(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId;
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_gshared (InternalEnumerator_1_t842163687 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1550231132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t4278378721  L_8 = ((  X509ChainStatus_t4278378721  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  X509ChainStatus_t4278378721  InternalEnumerator_1_get_Current_m1550231132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t842163687 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t842163687 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1550231132(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2314640734_gshared (InternalEnumerator_1_t2935262194 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2314640734_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1__ctor_m2314640734(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m214315662(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		float L_0 = InternalEnumerator_1_get_Current_m727737343((InternalEnumerator_1_t2935262194 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1231402888(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2195973811_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2195973811(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m580128774_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m580128774(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m727737343_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m727737343_gshared (InternalEnumerator_1_t2935262194 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m727737343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  float InternalEnumerator_1_get_Current_m727737343_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2935262194 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2935262194 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m727737343(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1240086835_gshared (InternalEnumerator_1_t3583626735 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1240086835_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1__ctor_m1240086835(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3826378355(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		Mark_t2724874473  L_0 = InternalEnumerator_1_get_Current_m575280506((InternalEnumerator_1_t3583626735 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t2724874473  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2035754659(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3744916110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3744916110(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1741571735_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1741571735(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m575280506_MetadataUsageId;
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_gshared (InternalEnumerator_1_t3583626735 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m575280506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t2724874473  L_8 = ((  Mark_t2724874473  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Mark_t2724874473  InternalEnumerator_1_get_Current_m575280506_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3583626735 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3583626735 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m575280506(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2189699457_gshared (InternalEnumerator_1_t4289011211 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2189699457_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1__ctor_m2189699457(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3249248421(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t3430258949  L_0 = InternalEnumerator_1_get_Current_m3411759116((InternalEnumerator_1_t4289011211 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t3430258949  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m439366097(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3838127340_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3838127340(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1674480765_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1674480765(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId;
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_gshared (InternalEnumerator_1_t4289011211 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3411759116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t3430258949  L_8 = ((  TimeSpan_t3430258949  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TimeSpan_t3430258949  InternalEnumerator_1_get_Current_m3411759116_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4289011211 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4289011211 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3411759116(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2981879621_gshared (InternalEnumerator_1_t1845634873 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2981879621_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1__ctor_m2981879621(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2571770313(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m3179981210((InternalEnumerator_1_t1845634873 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1658267053(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1824402698_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1824402698(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2809569305_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2809569305(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_gshared (InternalEnumerator_1_t1845634873 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3179981210_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3179981210_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1845634873 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1845634873 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3179981210(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m691972083_gshared (InternalEnumerator_1_t3008434283 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m691972083_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1__ctor_m691972083(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3107741851(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = InternalEnumerator_1_get_Current_m2198364332((InternalEnumerator_1_t3008434283 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2458630467(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2620838688_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2620838688(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m470170271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m470170271(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_gshared (InternalEnumerator_1_t3008434283 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2198364332_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint32_t InternalEnumerator_1_get_Current_m2198364332_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3008434283 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3008434283 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2198364332(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3084132532_gshared (InternalEnumerator_1_t3767949176 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3084132532_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1__ctor_m3084132532(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m187060888(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = InternalEnumerator_1_get_Current_m35328337((InternalEnumerator_1_t3767949176 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m771161214(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3642485841_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3642485841(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2954283444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2954283444(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m35328337_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_gshared (InternalEnumerator_1_t3767949176 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m35328337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint64_t InternalEnumerator_1_get_Current_m35328337_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3767949176 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3767949176 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m35328337(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3052252268_gshared (InternalEnumerator_1_t2735343205 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3052252268_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1__ctor_m3052252268(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3606709516(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		UriScheme_t1876590943  L_0 = InternalEnumerator_1_get_Current_m1830023619((InternalEnumerator_1_t2735343205 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t1876590943  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3065287496(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1770651099_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1770651099_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1770651099(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3629145604_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3629145604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3629145604(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1830023619_MetadataUsageId;
extern "C"  UriScheme_t1876590943  InternalEnumerator_1_get_Current_m1830023619_gshared (InternalEnumerator_1_t2735343205 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1830023619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t1876590943  L_8 = ((  UriScheme_t1876590943  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  UriScheme_t1876590943  InternalEnumerator_1_get_Current_m1830023619_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2735343205 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2735343205 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1830023619(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
