﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractB1486411203.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.KeepAliveBehaviour
struct  KeepAliveBehaviour_t1532923751  : public KeepAliveAbstractBehaviour_t1486411203
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
