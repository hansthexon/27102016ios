﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3129598331MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2447662476(__this, ___host0, method) ((  void (*) (Enumerator_t1157350169 *, Dictionary_2_t2762814027 *, const MethodInfo*))Enumerator__ctor_m4292427602_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2440038865(__this, method) ((  Il2CppObject * (*) (Enumerator_t1157350169 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1496472837_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1103872305(__this, method) ((  void (*) (Enumerator_t1157350169 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m387949317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
#define Enumerator_Dispose_m475526612(__this, method) ((  void (*) (Enumerator_t1157350169 *, const MethodInfo*))Enumerator_Dispose_m2018226074_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
#define Enumerator_MoveNext_m2486777284(__this, method) ((  bool (*) (Enumerator_t1157350169 *, const MethodInfo*))Enumerator_MoveNext_m2380055421_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
#define Enumerator_get_Current_m139617662(__this, method) ((  String_t* (*) (Enumerator_t1157350169 *, const MethodInfo*))Enumerator_get_Current_m4000415415_gshared)(__this, method)
