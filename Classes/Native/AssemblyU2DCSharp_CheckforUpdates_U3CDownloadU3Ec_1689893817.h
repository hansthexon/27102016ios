﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// CodeTest[]
struct CodeTestU5BU5D_t2090683080;
// CodeTest
struct CodeTest_t2096217477;
// System.Collections.Generic.IEnumerator`1<Code>
struct IEnumerator_1_t2456658634;
// Code
struct Code_t686167511;
// System.Object
struct Il2CppObject;
// CheckforUpdates
struct CheckforUpdates_t3659555713;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CheckforUpdates/<Download>c__Iterator8
struct  U3CDownloadU3Ec__Iterator8_t1689893817  : public Il2CppObject
{
public:
	// UnityEngine.WWW CheckforUpdates/<Download>c__Iterator8::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// CodeTest[] CheckforUpdates/<Download>c__Iterator8::<vr>__1
	CodeTestU5BU5D_t2090683080* ___U3CvrU3E__1_1;
	// CodeTest[] CheckforUpdates/<Download>c__Iterator8::<$s_25>__2
	CodeTestU5BU5D_t2090683080* ___U3CU24s_25U3E__2_2;
	// System.Int32 CheckforUpdates/<Download>c__Iterator8::<$s_26>__3
	int32_t ___U3CU24s_26U3E__3_3;
	// CodeTest CheckforUpdates/<Download>c__Iterator8::<v>__4
	CodeTest_t2096217477 * ___U3CvU3E__4_4;
	// System.Collections.Generic.IEnumerator`1<Code> CheckforUpdates/<Download>c__Iterator8::<$s_27>__5
	Il2CppObject* ___U3CU24s_27U3E__5_5;
	// Code CheckforUpdates/<Download>c__Iterator8::<c>__6
	Code_t686167511 * ___U3CcU3E__6_6;
	// System.Int32 CheckforUpdates/<Download>c__Iterator8::$PC
	int32_t ___U24PC_7;
	// System.Object CheckforUpdates/<Download>c__Iterator8::$current
	Il2CppObject * ___U24current_8;
	// CheckforUpdates CheckforUpdates/<Download>c__Iterator8::<>f__this
	CheckforUpdates_t3659555713 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CvrU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CvrU3E__1_1)); }
	inline CodeTestU5BU5D_t2090683080* get_U3CvrU3E__1_1() const { return ___U3CvrU3E__1_1; }
	inline CodeTestU5BU5D_t2090683080** get_address_of_U3CvrU3E__1_1() { return &___U3CvrU3E__1_1; }
	inline void set_U3CvrU3E__1_1(CodeTestU5BU5D_t2090683080* value)
	{
		___U3CvrU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvrU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_25U3E__2_2() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CU24s_25U3E__2_2)); }
	inline CodeTestU5BU5D_t2090683080* get_U3CU24s_25U3E__2_2() const { return ___U3CU24s_25U3E__2_2; }
	inline CodeTestU5BU5D_t2090683080** get_address_of_U3CU24s_25U3E__2_2() { return &___U3CU24s_25U3E__2_2; }
	inline void set_U3CU24s_25U3E__2_2(CodeTestU5BU5D_t2090683080* value)
	{
		___U3CU24s_25U3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_25U3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_26U3E__3_3() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CU24s_26U3E__3_3)); }
	inline int32_t get_U3CU24s_26U3E__3_3() const { return ___U3CU24s_26U3E__3_3; }
	inline int32_t* get_address_of_U3CU24s_26U3E__3_3() { return &___U3CU24s_26U3E__3_3; }
	inline void set_U3CU24s_26U3E__3_3(int32_t value)
	{
		___U3CU24s_26U3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CvU3E__4_4() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CvU3E__4_4)); }
	inline CodeTest_t2096217477 * get_U3CvU3E__4_4() const { return ___U3CvU3E__4_4; }
	inline CodeTest_t2096217477 ** get_address_of_U3CvU3E__4_4() { return &___U3CvU3E__4_4; }
	inline void set_U3CvU3E__4_4(CodeTest_t2096217477 * value)
	{
		___U3CvU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CU24s_27U3E__5_5() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CU24s_27U3E__5_5)); }
	inline Il2CppObject* get_U3CU24s_27U3E__5_5() const { return ___U3CU24s_27U3E__5_5; }
	inline Il2CppObject** get_address_of_U3CU24s_27U3E__5_5() { return &___U3CU24s_27U3E__5_5; }
	inline void set_U3CU24s_27U3E__5_5(Il2CppObject* value)
	{
		___U3CU24s_27U3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_27U3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CcU3E__6_6() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CcU3E__6_6)); }
	inline Code_t686167511 * get_U3CcU3E__6_6() const { return ___U3CcU3E__6_6; }
	inline Code_t686167511 ** get_address_of_U3CcU3E__6_6() { return &___U3CcU3E__6_6; }
	inline void set_U3CcU3E__6_6(Code_t686167511 * value)
	{
		___U3CcU3E__6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3E__6_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator8_t1689893817, ___U3CU3Ef__this_9)); }
	inline CheckforUpdates_t3659555713 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline CheckforUpdates_t3659555713 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(CheckforUpdates_t3659555713 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
