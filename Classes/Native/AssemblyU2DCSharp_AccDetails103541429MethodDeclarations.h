﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccDetails
struct AccDetails_t103541429;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AccDetails::.ctor()
extern "C"  void AccDetails__ctor_m3990643284 (AccDetails_t103541429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AccDetails::get_Name()
extern "C"  String_t* AccDetails_get_Name_m3395764523 (AccDetails_t103541429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccDetails::set_Name(System.String)
extern "C"  void AccDetails_set_Name_m3593025494 (AccDetails_t103541429 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AccDetails::get_Surname()
extern "C"  String_t* AccDetails_get_Surname_m3303478739 (AccDetails_t103541429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccDetails::set_Surname(System.String)
extern "C"  void AccDetails_set_Surname_m4280285916 (AccDetails_t103541429 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 AccDetails::get_Age()
extern "C"  int32_t AccDetails_get_Age_m53171650 (AccDetails_t103541429 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccDetails::set_Age(System.Int32)
extern "C"  void AccDetails_set_Age_m3015730623 (AccDetails_t103541429 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
