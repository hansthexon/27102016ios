﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveFileANdPlay/<Download>c__Iterator10
struct U3CDownloadU3Ec__Iterator10_t4266829119;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveFileANdPlay/<Download>c__Iterator10::.ctor()
extern "C"  void U3CDownloadU3Ec__Iterator10__ctor_m2788428662 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SaveFileANdPlay/<Download>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4249721424 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SaveFileANdPlay/<Download>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m1116252344 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SaveFileANdPlay/<Download>c__Iterator10::MoveNext()
extern "C"  bool U3CDownloadU3Ec__Iterator10_MoveNext_m3339498070 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay/<Download>c__Iterator10::Dispose()
extern "C"  void U3CDownloadU3Ec__Iterator10_Dispose_m3766312281 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay/<Download>c__Iterator10::Reset()
extern "C"  void U3CDownloadU3Ec__Iterator10_Reset_m2487458731 (U3CDownloadU3Ec__Iterator10_t4266829119 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
