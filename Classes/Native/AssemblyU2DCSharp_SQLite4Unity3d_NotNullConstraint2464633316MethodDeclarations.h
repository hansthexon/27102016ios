﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey13
struct U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_t2464633316;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"

// System.Void SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey13::.ctor()
extern "C"  void U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13__ctor_m1493515983 (U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_t2464633316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.NotNullConstraintViolationException/<NotNullConstraintViolationException>c__AnonStorey13::<>m__1(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_U3CU3Em__1_m118514312 (U3CNotNullConstraintViolationExceptionU3Ec__AnonStorey13_t2464633316 * __this, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
