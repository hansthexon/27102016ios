﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct ValueCollection_t3438122032;
// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct IEnumerator_1_t2618525888;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SQLite4Unity3d.SQLiteConnection/IndexInfo[]
struct IndexInfoU5BU5D_t625615456;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2126627657.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m2980779627_gshared (ValueCollection_t3438122032 * __this, Dictionary_2_t440094893 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m2980779627(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3438122032 *, Dictionary_2_t440094893 *, const MethodInfo*))ValueCollection__ctor_m2980779627_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3467596809_gshared (ValueCollection_t3438122032 * __this, IndexInfo_t848034765  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3467596809(__this, ___item0, method) ((  void (*) (ValueCollection_t3438122032 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3467596809_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1073638090_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1073638090(__this, method) ((  void (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1073638090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1440625111_gshared (ValueCollection_t3438122032 * __this, IndexInfo_t848034765  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1440625111(__this, ___item0, method) ((  bool (*) (ValueCollection_t3438122032 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1440625111_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m100409112_gshared (ValueCollection_t3438122032 * __this, IndexInfo_t848034765  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m100409112(__this, ___item0, method) ((  bool (*) (ValueCollection_t3438122032 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m100409112_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m359754090_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m359754090(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m359754090_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m1375316058_gshared (ValueCollection_t3438122032 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m1375316058(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3438122032 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1375316058_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1800558157_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1800558157(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1800558157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3008407260_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3008407260(__this, method) ((  bool (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3008407260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2828968226_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2828968226(__this, method) ((  bool (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2828968226_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m969375434_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m969375434(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m969375434_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m55448994_gshared (ValueCollection_t3438122032 * __this, IndexInfoU5BU5D_t625615456* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m55448994(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3438122032 *, IndexInfoU5BU5D_t625615456*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m55448994_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetEnumerator()
extern "C"  Enumerator_t2126627657  ValueCollection_GetEnumerator_m2504057031_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2504057031(__this, method) ((  Enumerator_t2126627657  (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_GetEnumerator_m2504057031_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m1235472514_gshared (ValueCollection_t3438122032 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m1235472514(__this, method) ((  int32_t (*) (ValueCollection_t3438122032 *, const MethodInfo*))ValueCollection_get_Count_m1235472514_gshared)(__this, method)
