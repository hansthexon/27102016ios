﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_Generic_EqualityCompar3542762555.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct  DefaultComparer_t2296090149  : public EqualityComparer_1_t3542762555
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
