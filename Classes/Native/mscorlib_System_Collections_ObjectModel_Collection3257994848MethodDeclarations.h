﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Collection_1_t3257994848;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2270685851;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t1191773921;
// System.Collections.Generic.IList`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IList_1_t4257190695;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void Collection_1__ctor_m2957397231_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1__ctor_m2957397231(__this, method) ((  void (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1__ctor_m2957397231_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629489514_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629489514(__this, method) ((  bool (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m629489514_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m2994407427_gshared (Collection_1_t3257994848 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m2994407427(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3257994848 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m2994407427_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373965454_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373965454(__this, method) ((  Il2CppObject * (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2373965454_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1604187507_gshared (Collection_1_t3257994848 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1604187507(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3257994848 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1604187507_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m736828683_gshared (Collection_1_t3257994848 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m736828683(__this, ___value0, method) ((  bool (*) (Collection_1_t3257994848 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m736828683_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3313233605_gshared (Collection_1_t3257994848 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3313233605(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3257994848 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3313233605_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1654279356_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1654279356(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1654279356_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m4198360090_gshared (Collection_1_t3257994848 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m4198360090(__this, ___value0, method) ((  void (*) (Collection_1_t3257994848 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m4198360090_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m119100907_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m119100907(__this, method) ((  bool (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m119100907_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1179834595_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1179834595(__this, method) ((  Il2CppObject * (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1179834595_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1784704736_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1784704736(__this, method) ((  bool (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1784704736_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m158144903_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m158144903(__this, method) ((  bool (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m158144903_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m225128332_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m225128332(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3257994848 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m225128332_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2140059461_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2140059461(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2140059461_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Add(T)
extern "C"  void Collection_1_Add_m1787264192_gshared (Collection_1_t3257994848 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1787264192(__this, ___item0, method) ((  void (*) (Collection_1_t3257994848 *, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_Add_m1787264192_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Clear()
extern "C"  void Collection_1_Clear_m637084204_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_Clear_m637084204(__this, method) ((  void (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_Clear_m637084204_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4190262494_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4190262494(__this, method) ((  void (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_ClearItems_m4190262494_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Contains(T)
extern "C"  bool Collection_1_Contains_m170562226_gshared (Collection_1_t3257994848 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m170562226(__this, ___item0, method) ((  bool (*) (Collection_1_t3257994848 *, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_Contains_m170562226_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2215212900_gshared (Collection_1_t3257994848 * __this, KeyValuePair_2U5BU5D_t2270685851* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2215212900(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3257994848 *, KeyValuePair_2U5BU5D_t2270685851*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2215212900_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1527806603_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1527806603(__this, method) ((  Il2CppObject* (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_GetEnumerator_m1527806603_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3003122346_gshared (Collection_1_t3257994848 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3003122346(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3257994848 *, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_IndexOf_m3003122346_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m2087806665_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m2087806665(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_Insert_m2087806665_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2313910862_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2313910862(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_InsertItem_m2313910862_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Remove(T)
extern "C"  bool Collection_1_Remove_m3661360561_gshared (Collection_1_t3257994848 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3661360561(__this, ___item0, method) ((  bool (*) (Collection_1_t3257994848 *, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_Remove_m3661360561_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1343423493_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1343423493(__this, ___index0, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1343423493_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1834357153_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1834357153(__this, ___index0, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1834357153_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m1454809363_gshared (Collection_1_t3257994848 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m1454809363(__this, method) ((  int32_t (*) (Collection_1_t3257994848 *, const MethodInfo*))Collection_1_get_Count_m1454809363_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  Collection_1_get_Item_m2408428953_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2408428953(__this, ___index0, method) ((  KeyValuePair_2_t3716250094  (*) (Collection_1_t3257994848 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2408428953_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3272422502_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3272422502(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_set_Item_m3272422502_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3412905109_gshared (Collection_1_t3257994848 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3412905109(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3257994848 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))Collection_1_SetItem_m3412905109_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m4073855012_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m4073855012(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m4073855012_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::ConvertItem(System.Object)
extern "C"  KeyValuePair_2_t3716250094  Collection_1_ConvertItem_m2559985310_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2559985310(__this /* static, unused */, ___item0, method) ((  KeyValuePair_2_t3716250094  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2559985310_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1208319296_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1208319296(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1208319296_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1695216446_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1695216446(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1695216446_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1594774609_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1594774609(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1594774609_gshared)(__this /* static, unused */, ___list0, method)
