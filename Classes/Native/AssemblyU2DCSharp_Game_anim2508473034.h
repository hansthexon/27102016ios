﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Game_anim
struct  Game_anim_t2508473034  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Game_anim::gameButton
	GameObject_t1756533147 * ___gameButton_2;
	// UnityEngine.Animator Game_anim::gameAnim
	Animator_t69676727 * ___gameAnim_3;

public:
	inline static int32_t get_offset_of_gameButton_2() { return static_cast<int32_t>(offsetof(Game_anim_t2508473034, ___gameButton_2)); }
	inline GameObject_t1756533147 * get_gameButton_2() const { return ___gameButton_2; }
	inline GameObject_t1756533147 ** get_address_of_gameButton_2() { return &___gameButton_2; }
	inline void set_gameButton_2(GameObject_t1756533147 * value)
	{
		___gameButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameButton_2, value);
	}

	inline static int32_t get_offset_of_gameAnim_3() { return static_cast<int32_t>(offsetof(Game_anim_t2508473034, ___gameAnim_3)); }
	inline Animator_t69676727 * get_gameAnim_3() const { return ___gameAnim_3; }
	inline Animator_t69676727 ** get_address_of_gameAnim_3() { return &___gameAnim_3; }
	inline void set_gameAnim_3(Animator_t69676727 * value)
	{
		___gameAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
