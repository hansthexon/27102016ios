﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct KeyCollection_t2923592664;
// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object[]
struct ObjectU5BU5D_t3614634134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3129598331.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m2113853019_gshared (KeyCollection_t2923592664 * __this, Dictionary_2_t440094893 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m2113853019(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2923592664 *, Dictionary_2_t440094893 *, const MethodInfo*))KeyCollection__ctor_m2113853019_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1005923985_gshared (KeyCollection_t2923592664 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1005923985(__this, ___item0, method) ((  void (*) (KeyCollection_t2923592664 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1005923985_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2085351498_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2085351498(__this, method) ((  void (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2085351498_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3945669279_gshared (KeyCollection_t2923592664 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3945669279(__this, ___item0, method) ((  bool (*) (KeyCollection_t2923592664 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3945669279_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3143167270_gshared (KeyCollection_t2923592664 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3143167270(__this, ___item0, method) ((  bool (*) (KeyCollection_t2923592664 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3143167270_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1394097920_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1394097920(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1394097920_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1923914272_gshared (KeyCollection_t2923592664 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1923914272(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2923592664 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1923914272_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2219900805_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2219900805(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2219900805_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3175780828_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3175780828(__this, method) ((  bool (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3175780828_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2965775648_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2965775648(__this, method) ((  bool (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2965775648_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2849362144_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2849362144(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2849362144_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2449658018_gshared (KeyCollection_t2923592664 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2449658018(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2923592664 *, ObjectU5BU5D_t3614634134*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2449658018_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetEnumerator()
extern "C"  Enumerator_t3129598331  KeyCollection_GetEnumerator_m2543932487_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2543932487(__this, method) ((  Enumerator_t3129598331  (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_GetEnumerator_m2543932487_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1053061128_gshared (KeyCollection_t2923592664 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1053061128(__this, method) ((  int32_t (*) (KeyCollection_t2923592664 *, const MethodInfo*))KeyCollection_get_Count_m1053061128_gshared)(__this, method)
